public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Read the size of the array and validate it
        System.out.print("Enter an integer number representing the size of the array: ");
        int arraySize = scanner.nextInt();
        
        while (arraySize <= 0) {
            System.out.println("The size of the array should be a positive integer number!");
            System.out.print("Try again: ");
            arraySize = scanner.nextInt();
        }
        
        // Read the elements of the original array
        int[] originalArray = new int[arraySize];
        for (int i = 0; i < arraySize; i++) {
            System.out.format("arr[%d] = ", i);
            originalArray[i] = scanner.nextInt();
        }
        
        // Display the original array
        System.out.println(Arrays.toString(originalArray));
        
        // Create a copy of the original array
        int[] sortedArray = Arrays.copyOf(originalArray, originalArray.length);
        // Sort the copy
        Arrays.sort(sortedArray);
        
        // Display the sorted copy array
        System.out.println(Arrays.toString(sortedArray));
        
        // Read the lowerLimit and upperLimit values
        System.out.print("Enter a lower limit value: ");
        int lowerLimit = scanner.nextInt();
        
        System.out.print("Enter an upper limit value: ");
        int upperLimit = scanner.nextInt();
        
        // Exchange the values if lowerLimit > upperLimit
        if (lowerLimit > upperLimit) {
            lowerLimit ^= upperLimit;
            upperLimit ^= lowerLimit;
            lowerLimit ^= upperLimit;
        }
        
        // Read a fill value from the keyboard
        System.out.print("Enter a fill value: ");
        int fillValue = scanner.nextInt();
        
        // Determine the indices of the lowerLimit and upperLimit values inside the sorted array using the binary search algorithm
        int lowerLimitIndex = Arrays.binarySearch(sortedArray, lowerLimit);
        int upperLimitIndex = Arrays.binarySearch(sortedArray, upperLimit);
        
        // If the values were not found, determine the default values
        lowerLimitIndex = lowerLimitIndex == -1 ? 0 : lowerLimitIndex;
        upperLimitIndex = upperLimitIndex == -1 ? sortedArray.length : upperLimitIndex;
        
        // Fill the array between the found boundaries
        int[] filledArray = Arrays.copyOf(sortedArray, sortedArray.length);
        Arrays.fill(filledArray, lowerLimitIndex, upperLimitIndex, fillValue);
        
        // Check if the array did change with respect to the original sorted array
        if (Arrays.equals(sortedArray, filledArray)) {
            System.out.println("The sorted array is equal to the array obtained after the replacement!");
        } else {
            System.out.println("The sorted array is not equal to the array obtained after the replacement!");
        }
        
        // Display the filled array
        System.out.println(Arrays.toString(filledArray));
        
        scanner.close();
    }
}