import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		// Read the number of persons and validate
		System.out.print("Enter the number of persons from the group: ");
		int personCount = scanner.nextInt();
		
		while (personCount <= 0) {
			System.out.print("The number of persons from the group must be a positive integer number! Please try again: ");
			personCount = scanner.nextInt();
		}
		// Get rid of the ENTER pressed when reading the number of persons
		scanner.nextLine();
		
		// Dynamic allocation for the array of objects
		Person[] persons = new Person[personCount];
		
		for (int i = 0; i < personCount; i++) {
			// Read the name of the person from the keyboard
			System.out.format("Enter the name of person number %d from the group: ", i + 1);
			String name = scanner.nextLine();

			// Read the age of the person from the keyboard and validate it
			System.out.format("Enter the age of person number %d from the group: ", i + 1);
			Integer age = scanner.nextInt();
			if (age < 0 || age > 100) {
				System.out.print("The age of the person must be between 0 and 100! Please try again: ");
				age = scanner.nextInt();
			}
			// Again, get rid of the ENTER from the end of the previous reading
			scanner.nextLine();

			// Read the gender of the person from the keyboard and validate it
			System.out.format("Enter the gender of person number %d from the group: ", i + 1);
			String gender = scanner.nextLine();
			while (!"M".equalsIgnoreCase(gender) && !"F".equalsIgnoreCase(gender)) {
				System.out.print("Accepted values for gender are 'M' or 'F'! Please try again: ");
				gender = scanner.nextLine();
			}

			Person person = new Person(name, gender, age);
			persons[i] = person;
		}
		
		// Enter your name and receive greetings from everyone from the group
		System.out.print("Enter your name: ");
		String name = scanner.nextLine();
		for (Person person : persons) {
			person.hello(name);
		}
		
		// Initialization of the values that are to be computed
		int averageAge = 0;
		int menCount = 0;
		int womenCount = 0;
		int countBetween0And17 = 0;
		int countBetween18And35 = 0;
		int countBetween36And50 = 0;
		int count51OrAbove = 0;
		
		// Traverse the array of persons in order to compute the required parameters
		for (Person person : persons) {
			averageAge = person.getAge();
			
			if ("M".equalsIgnoreCase(person.gender)) {
				menCount++;
			} else if ("F".equalsIgnoreCase(person.gender)) {
				womenCount++;
			}
			
			if (person.getAge() >= 0 && person.getAge() <= 17) {
				countBetween0And17++;
			} else if (person.getAge() >= 18 && person.getAge() <= 35) {
				countBetween18And35++;
			} else if (person.getAge() >= 36 && person.getAge() <= 50) {
				countBetween36And50++;
			} else if (person.getAge() >= 51) {
				count51OrAbove++;
			}
		}
		
		// Finalize computation of the average
		averageAge /= Person.count;
		
		// Display the computed values
		System.out.format("The average age of the group is %d\n", averageAge);
		System.out.format("There are %d men and %d women in the group\n", menCount, womenCount);
		System.out.format("The number of persons with ages between 0 and 17 is %d\n", countBetween0And17);
		System.out.format("The number of persons with ages between 18 and 35 is %d\n", countBetween18And35);
		System.out.format("The number of persons with ages between 36 and 50 is %d\n", countBetween36And50);
		System.out.format("The number of persons with ages equal to or above 51 is %d\n", count51OrAbove);
		
		// Close the Scanner object
		scanner.close();
	}
}
