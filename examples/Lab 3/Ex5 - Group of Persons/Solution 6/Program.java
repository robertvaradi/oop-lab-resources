import java.util.Scanner;

public class Program {

	public static Integer readInteger(Scanner scanner, String prompt) {
		System.out.print(prompt);
		return scanner.nextInt();
	}

	public static String readString(Scanner scanner, String prompt) {
		System.out.print(prompt);
		return scanner.nextLine();
	}
	
	public static Integer readPersonCount(Scanner scanner) {
		// Read the number of persons and validate
		int personCount = readInteger(scanner, "Enter the number of persons from the group: ");

		while (personCount <= 0) {
			personCount = readInteger(scanner, "The number of persons from the group must be a positive integer number! Please try again: ");
		}
		// Get rid of the ENTER pressed when reading the number of persons
		scanner.nextLine();
		
		return personCount;
	}
	
	public static Integer readAge(Scanner scanner, int index) {
		// Read the age of the person from the keyboard and validate it
		Integer age = readInteger(scanner, String.format("Enter the age of person number %d from the group: ", index + 1));
		
		if (age < 0 || age > 100) {
			age = readInteger(scanner, "The age of the person must be between 0 and 100! Please try again: ");
		}
		// Again, get rid of the ENTER from the end of the previous reading
		scanner.nextLine();
		
		return age;
	}
	
	public static String readGender(Scanner scanner, int index) {
		// Read the gender of the person from the keyboard and validate it
		String gender = readString(scanner, String.format("Enter the gender of person number %d from the group: ", index + 1));
		while (!"M".equalsIgnoreCase(gender) && !"F".equalsIgnoreCase(gender)) {
			gender = readString(scanner, "Accepted values for gender are 'M' or 'F'! Please try again: ");
		}
		return gender;
	}
	
	public static Person readPerson(Scanner scanner, int index) {
		// Read the name of the person from the keyboard
		String name = readString(scanner, String.format("Enter the name of person number %d from the group: ", index + 1));
		Integer age = readAge(scanner, index);
		String gender = readGender(scanner, index);
		Person person = new Person(name, gender, age);
		return person;
	}
	
	public static Person[] readPersons(Scanner scanner) {
		Integer personCount = readPersonCount(scanner);

		// Dynamic allocation for the array of objects
		Person[] persons = new Person[personCount];

		for (int i = 0; i < personCount; i++) {
			persons[i] = readPerson(scanner, i);
		}
		
		return persons;
	}
	
	public static Integer getAverageAge(Person[] persons) {
		int averageAge = 0;
		for (Person person : persons) {
			averageAge += person.getAge();
		}
		return averageAge / Person.count;
	}
	
	public static Integer[] getGenderDistribution(Person[] persons) {
		Integer[] counts = new Integer[] {0, 0};
		for (Person person : persons) {
			if ("M".equalsIgnoreCase(person.gender)) {
				counts[0]++;
			} else if ("F".equalsIgnoreCase(person.gender)) {
				counts[1]++;
			}
		}
		return counts;
	}
	
	public static Integer getCountInAgeInterval(Integer lowerLimit, Integer upperLimit, Person[] persons) {
		Integer count = 0;
		for (Person person : persons) {
			if (person.getAge() >= lowerLimit && person.getAge() <= upperLimit) {
				count++;
			}
		}
		return count;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		Person[] persons = readPersons(scanner);

		// Enter your name and receive greetings from everyone from the group
		String name = readString(scanner, "Enter your name: ");
		
		for (Person person : persons) {
			person.hello(name);
		}

		// Initialization of the values that are to be computed
		Integer averageAge = getAverageAge(persons);
		Integer[] genderCounts = getGenderDistribution(persons);
		Integer countBetween0And17 = getCountInAgeInterval(0, 17, persons);
		Integer countBetween18And35 = getCountInAgeInterval(18, 35, persons);
		Integer countBetween36And50 = getCountInAgeInterval(36, 50, persons);
		Integer count51OrAbove = getCountInAgeInterval(51, 100, persons);

		// Display the computed values
		System.out.format("The average age of the group is %d\n", averageAge);
		System.out.format("There are %d men and %d women in the group\n", genderCounts[0], genderCounts[1]);
		System.out.format("The number of persons with ages between 0 and 17 is %d\n", countBetween0And17);
		System.out.format("The number of persons with ages between 18 and 35 is %d\n", countBetween18And35);
		System.out.format("The number of persons with ages between 36 and 50 is %d\n", countBetween36And50);
		System.out.format("The number of persons with ages equal to or above 51 is %d\n", count51OrAbove);

		// Close the Scanner object
		scanner.close();
	}
}
