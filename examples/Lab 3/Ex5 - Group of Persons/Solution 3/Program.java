import java.util.Scanner;

public class Program {

	public static Integer readInteger(Scanner scanner, String prompt) {
		System.out.print(prompt);
		return scanner.nextInt();
	}

	public static String readString(Scanner scanner, String prompt) {
		System.out.print(prompt);
		return scanner.nextLine();
	}
	
	public static Integer readPersonCount(Scanner scanner) {
		// Read the number of persons and validate
		int personCount = readInteger(scanner, "Enter the number of persons from the group: ");

		while (personCount <= 0) {
			personCount = readInteger(scanner, "The number of persons from the group must be a positive integer number! Please try again: ");
		}
		// Get rid of the ENTER pressed when reading the number of persons
		scanner.nextLine();
		
		return personCount;
	}
	
	public static Integer readAge(Scanner scanner, int index) {
		// Read the age of the person from the keyboard and validate it
		Integer age = readInteger(scanner, String.format("Enter the age of person number %d from the group: ", index + 1));
		
		if (age < 0 || age > 100) {
			age = readInteger(scanner, "The age of the person must be between 0 and 100! Please try again: ");
		}
		// Again, get rid of the ENTER from the end of the previous reading
		scanner.nextLine();
		
		return age;
	}
	
	public static String readGender(Scanner scanner, int index) {
		// Read the gender of the person from the keyboard and validate it
		String gender = readString(scanner, String.format("Enter the gender of person number %d from the group: ", index + 1));
		while (!"M".equalsIgnoreCase(gender) && !"F".equalsIgnoreCase(gender)) {
			gender = readString(scanner, "Accepted values for gender are 'M' or 'F'! Please try again: ");
		}
		return gender;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		Integer personCount = readPersonCount(scanner);

		// Dynamic allocation for the array of objects
		Person[] persons = new Person[personCount];

		for (int i = 0; i < personCount; i++) {
			// Read the name of the person from the keyboard
			String name = readString(scanner, String.format("Enter the name of person number %d from the group: ", i + 1));
			Integer age = readAge(scanner, i);
			String gender = readGender(scanner, i);
			Person person = new Person(name, gender, age);
			persons[i] = person;
		}

		// Enter your name and receive greetings from everyone from the group
		String name = readString(scanner, "Enter your name: ");
		
		for (Person person : persons) {
			person.hello(name);
		}

		// Initialization of the values that are to be computed
		int averageAge = 0;
		int menCount = 0;
		int womenCount = 0;
		int countBetween0And17 = 0;
		int countBetween18And35 = 0;
		int countBetween36And50 = 0;
		int count51OrAbove = 0;

		// Traverse the array of persons in order to compute the required parameters
		for (Person person : persons) {
			averageAge = person.getAge();

			if ("M".equalsIgnoreCase(person.gender)) {
				menCount++;
			} else if ("F".equalsIgnoreCase(person.gender)) {
				womenCount++;
			}

			if (person.getAge() >= 0 && person.getAge() <= 17) {
				countBetween0And17++;
			} else if (person.getAge() >= 18 && person.getAge() <= 35) {
				countBetween18And35++;
			} else if (person.getAge() >= 36 && person.getAge() <= 50) {
				countBetween36And50++;
			} else if (person.getAge() >= 51) {
				count51OrAbove++;
			}
		}

		// Finalize computation of the average
		averageAge /= Person.count;

		// Display the computed values
		System.out.format("The average age of the group is %d\n", averageAge);
		System.out.format("There are %d men and %d women in the group\n", menCount, womenCount);
		System.out.format("The number of persons with ages between 0 and 17 is %d\n", countBetween0And17);
		System.out.format("The number of persons with ages between 18 and 35 is %d\n", countBetween18And35);
		System.out.format("The number of persons with ages between 36 and 50 is %d\n", countBetween36And50);
		System.out.format("The number of persons with ages equal to or above 51 is %d\n", count51OrAbove);

		// Close the Scanner object
		scanner.close();
	}
}
