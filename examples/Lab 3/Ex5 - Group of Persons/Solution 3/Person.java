public class Person {
	static Integer count;
	
	protected String name;
	
	public String gender;
	
	private Integer age;
	
	public Person(String name, String gender, Integer age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		count++;
	}
	
	public void hello(String name) {
		System.out.format("Hello, %s, my name is %s and I am %d years old! Nice to meet you!", name, this.name, age);
	}
	
	public Integer getAge() {
		return age;
	}
}
