
public class Program {
	public static void main(String[] args) {
		String[] weekDays = new String[] { WeekDay.MONDAY, WeekDay.TUESDAY, WeekDay.WEDNESDAY, WeekDay.THURSDAY,
				WeekDay.FRIDAY, WeekDay.SATURDAY, WeekDay.SUNDAY };
		for (String day : weekDays) {
			System.out.println(day);
		}
	}
}
