
public class WeekDay {
	private String name;
	
	public static final WeekDay MONDAY = new WeekDay("Monday");
	public static final WeekDay TUESDAY = new WeekDay("Tuesday");
	public static final WeekDay WEDNESDAY = new WeekDay("Wednesday");
	public static final WeekDay THURSDAY = new WeekDay("Thursday");
	public static final WeekDay FRIDAY = new WeekDay("Friday");
	public static final WeekDay SATURDAY = new WeekDay("Saturday");
	public static final WeekDay SUNDAY = new WeekDay("Sunday");
	
	private WeekDay(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
