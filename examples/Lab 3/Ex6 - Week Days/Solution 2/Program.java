public class Program {
	public static void main(String[] args) {
		WeekDay[] weekDays = new WeekDay[] { WeekDay.MONDAY, WeekDay.TUESDAY, WeekDay.WEDNESDAY, WeekDay.THURSDAY,
				WeekDay.FRIDAY, WeekDay.SATURDAY, WeekDay.SUNDAY };
		for (WeekDay day : weekDays) {
			System.out.println(day.getName());
		}
	}
}
