public class Program {
	public static void main(String[] args) {
		for (WeekDay day : WeekDay.values()) {
			System.out.println(day.name());
		}
		boolean valid = true;
		for (WeekDay wd : WeekDay.values()) {
			if (wd != WeekDay.valueOf(wd.name())) {
				valid = false;
				break;
			}
		}
		if (valid) {
			System.out.println("OK!");
		} else {
			System.out.println("Something is very wrong!");
		}
	}
}
