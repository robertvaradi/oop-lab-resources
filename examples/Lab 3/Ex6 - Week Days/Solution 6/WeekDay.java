
public enum WeekDay {
	MONDAY("Monday", 1),
	TUESDAY("Tuesday", 2),
	WEDNESDAY("Wednesday", 3),
	THURSDAY("Thursday", 4),
	FRIDAY("Friday", 5),
	SATURDAY("Saturday", 6),
	SUNDAY("Sunday", 7);
	
	private String name;
	
	private Integer orderNumber;
	
	private WeekDay(String name, Integer orderNumber) {
		this.name = name;
		this.orderNumber = orderNumber;
	}
	
	public String getName() {
		return name;
	}
	
	public Integer getOrderNumber() {
		return orderNumber;
	}
	
	public static WeekDay getDayOfWeek(String name) {
		for (WeekDay wd : WeekDay.values()) {
			if (wd.getName().equals(name)) {
				return wd;
			}
		}
		return null;
	}
}
