public class Program {
	public static void main(String[] args) {
		for (WeekDay day : WeekDay.values()) {
			System.out.format("Name: %s\nOrder number: %d\n", day.getName(), day.getOrderNumber());
		}
		boolean valid = true;
		for (WeekDay wd : WeekDay.values()) {
			if (wd != WeekDay.valueOf(wd.name())) {
				valid = false;
				break;
			}
			if (wd != WeekDay.getDayOfWeek(wd.getName())) {
				valid = false;
				break;
			}
		}
		if (valid) {
			System.out.println("OK!");
		} else {
			System.out.println("Something is very wrong!");
		}
	}
}
