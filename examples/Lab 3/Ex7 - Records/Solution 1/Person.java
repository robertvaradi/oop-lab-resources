import java.util.Objects;

public class Person {
	private final String name;
	private final Integer age;
	private final String gender;

	public Person(String name, Integer age, String gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public Integer getAge() {
		return age;
	}

	public String getGender() {
		return gender;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (!(obj instanceof Person)) {
			return false;
		} else {
			Person other = (Person) obj;
			return Objects.equals(name, other.name) && Objects.equals(age, other.age)
					&& Objects.equals(gender, other.gender);
		}
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + "]";
	}
}
