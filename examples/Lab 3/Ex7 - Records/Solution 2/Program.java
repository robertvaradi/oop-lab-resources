
public class Program {
	public static void main(String[] args) {
		Person person = new Person("John", 25, "M");
		Person person1 = new Person("John", 25, "M");
		Person person2 = new Person("Jane", 25, "F");
		System.out.println(person);
		System.out.println(person1);
		System.out.println(person2);
		if (person.equals(person1)) {
			System.out.format("%s and %s are equal\n", person.toString(), person1.toString());
		} else {
			System.out.format("%s and %s are not equal\n", person.toString(), person1.toString());
		}
		if (person.equals(person2)) {
			System.out.format("%s and %s are equal\n", person.toString(), person2.toString());
		} else {
			System.out.format("%s and %s are not equal\n", person.toString(), person2.toString());
		}
	}
}
