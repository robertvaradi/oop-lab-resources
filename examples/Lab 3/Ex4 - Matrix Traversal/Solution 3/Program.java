import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Read and validate the number of rows in the matrix
        System.out.print("Enter the number of rows of the matrix: ");
        int rowCount = scanner.nextInt();
        
        while (rowCount <= 0) {
            System.out.println("The number of rows must be a positive integer number!");
            System.out.print("Try again: ");
            rowCount = scanner.nextInt();
        }
        
        // Read and validate the number of columns in the matrix
        System.out.print("Enter the number of columns of the matrix: ");
        int columnCount = scanner.nextInt();
        
        while (columnCount <= 0) {
            System.out.println("The number of columns must be a positive integer number!");
            System.out.print("Try again: ");
            columnCount = scanner.nextInt();
        }
        
        // Dynamically allocate memory for the array of row references of the matrix
        int[][] matrix = new int[rowCount][];
        
        for (int i = 0; i < rowCount; i++) {
            // Dynamically allocate memory for the elements on the matrix's row
            matrix[i] = new int[columnCount];
            // Read the matrix element-by-element
            for (int j = 0; j < columnCount; j++) {
                System.out.format("matrix[%d][%d] = ", i, j);
                matrix[i][j] = scanner.nextInt();
            }
        }
        
        // Display the entries of the matrix
        System.out.print("[");
        for (int[] row : matrix) {
            System.out.format("\n    %s", Arrays.toString(row));
        }
        System.out.format("\n]");
        
        scanner.close();
    }
}