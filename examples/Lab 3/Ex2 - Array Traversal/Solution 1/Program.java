public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter the size of the array: ");
        int arraySize = scanner.nextInt();
        
        while (arraySize <= 0) {
            System.out.println("The size of the array must be a positive integer number!");
            System.out.print("Try again: ");
            arraySize = scanner.nextInt();
        }
        
        int[] arr = new int[arraySize];
        for (int i = 0; i < arraySize; i++) {
            System.out.format("arr[%d] = ", i);
            arr[i] = scanner.nextInt();
        }
        
        for (int i = 0; i < arr.length; i++) {
            System.out.format("arr[%d] = %d\n", i, arr[i]);
        }
        
        scanner.close();
    }
}