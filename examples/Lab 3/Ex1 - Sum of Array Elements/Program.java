import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Start by reading the size of the array from the keyboard
        System.out.print("Enter the size of the array: ");
        int arraySize = scanner.nextInt();
        
        // If the size of the array is not a valid number, display an error message and try again
        while (arraySize <= 0) {
            System.out.println("The size of the array must be a positive integer number!");
            System.out.print("Try again: ");
            arraySize = scanner.nextInt();
        }
        
        // Dynamic allocation of the array elements
        int[] arr = new int[arraySize];

        // Reading the elements one by one
        for (int i = 0; i < arraySize; i++) {
            System.out.format("arr[%d] = ", i);
            arr[i] = scanner.nextInt();
        }
        
        // Compute the sum of the elements from the array
        int sum = 0;
        for (int i = 0; i < arraySize; i++) {
            sum += arr[i];
        }
        
        // Display the result
        System.out.format("The sum of the elements of the array is %d.", sum);
        
        scanner.close();
    }
}