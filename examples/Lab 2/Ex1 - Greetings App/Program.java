import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
		// We need a Scanner object in order to read the information from the keyboard
        Scanner scanner = new Scanner(System.in);
		
		// Read all the required information from the keyboard
        System.out.print("Enter your name from the keyboard: ");
        String name = scanner.nextLine();

        System.out.print("Enter your age from the keyaboard: ");
        int age = scanner.nextInt();
		// This needed in order to get rid of the ENTER that was pressed after entering the number
        scanner.nextLine();

        System.out.print("Enter your phone number from the keyboard: ");
        String phoneNumber = scanner.nextLine();

        System.out.print("Enter your e-mail address from the keyboard: ");
        String email = scanner.nextLine();
		
		// Display the message
        System.out.format(
                "Hello, my name is %s and I am %d years old, my phone number is %s and my e-mail address is %s! Feel free to contact me anytime.",
                name, age, phoneNumber, email);

		// Close the scanner
        scanner.close();
    }
}