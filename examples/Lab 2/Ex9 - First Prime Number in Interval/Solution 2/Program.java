public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter the lower limit: ");
        int lowerLimit = scanner.nextInt();
        
        System.out.print("Enter the upper limit: ");
        int upperLimit = scanner.nextInt();
        
        if (lowerLimit > upperLimit) {
            lowerLimit ^= upperLimit;
            upperLimit ^= lowerLimit;
            lowerLimit ^= upperLimit;
        }
        
        int smallestPrime = lowerLimit;
        
        outerLoop:
        for (int currentNumber = lowerLimit; currentNumber <= upperLimit; currentNumber++) {
            System.out.format("Executing loop for %d ... \n", currentNumber);
            
            boolean isPrime = switch (currentNumber) {
                case 0 -> false;
                case 1 -> false;
                case 2 -> true;
                default -> true;
            };
            
            for (int divisor = 2; divisor <= currentNumber && isPrime; divisor++) {
                if (currentNumber % 2 == 0) {
                    System.out.format("Even numbers cannot be prime. Skipping %d ...\n", currentNumber);
                    continue outerLoop;
                }
                
                if (currentNumber % divisor == 0) {
                    isPrime = false;
                }
                
                if (divisor >= currentNumber / 2 && isPrime) {
                    System.out.format("Found smallest prime %d!\n", currentNumber);
                    smallestPrime = currentNumber;
                    break outerLoop;
                }
            }
            System.out.format("Finished loop for number %d.\n", currentNumber);
        }
        
        System.out.format("The first prime number in the [%d, %d] interval is %d.", lowerLimit, upperLimit, smallestPrime);
        scanner.close();
    }
}