public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter the lower limit: ");
        int lowerLimit = scanner.nextInt();
        
        System.out.print("Enter the upper limit: ");
        int upperLimit = scanner.nextInt();
        
        if (lowerLimit > upperLimit) {
            lowerLimit ^= upperLimit;
            upperLimit ^= lowerLimit;
            lowerLimit ^= upperLimit;
        }
        
        // Find the first prime number in the [0, number] interval
        int currentNumber;
        outerLoop:
        for (currentNumber = 0; currentNumber < number; currentNumber++) {
            System.out.format("Executing loop for %d ... \n", currentNumber);
            
            boolean isPrime = switch (currentNumber) {
                case 0 -> false;
                case 1 -> false;
                case 2 -> true;
                default -> true;
            };
            
            if (isPrime) {
                for (int divisor = 2; divisor <= currentNumber; divisor++) {
                    if (currentNumber % divisor == 0) {
                        isPrime = false;
                    }
                    
                    // If we found the first prime number, our work is done
                    // so there is no point in continuing the search
                    if (divisor >= currentNumber / 2 && isPrime) {
                        System.out.format("Break loop for number %d\n", currentNumber);
                        // The next outer loop will not be executed after this break
                        break outerLoop;
                    }
                }
            }
        }

        System.out.format("The first prime number in the [%d, %d] interval is %d.", 0, number, currentNumber);
        scanner.close();
    }
}