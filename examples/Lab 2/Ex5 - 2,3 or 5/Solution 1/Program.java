import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter an integer number from the keyboard: ");
        int number = scanner.nextInt();

        if (number % 2 == 0) {
            System.out.format("%d is divisible by 2\n", number);
        } else {
            if (number % 3 == 0) {
                System.out.format("%d is divisible by 3\n", number);
            } else {
                if (number % 5 == 0) {
                    System.out.format("%d is divisible by 5\n", number);
                } else {
                    System.out.format("%d is not divisible by 2, 3 or 5\n", number);
                }
            }
        }

        scanner.close();
    }
}