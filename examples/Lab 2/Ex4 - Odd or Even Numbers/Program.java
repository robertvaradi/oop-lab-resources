import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter an integer number from the keyboard: ");
        int number = scanner.nextInt();

        if (number % 2 == 1) {
            System.out.format("%s is an odd number\n", number);
        } else {
            System.out.format("%s is an even number\n", number);
        }

        scanner.close();
    }
}