import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        // Declaration of the values we will be working with
        int a, b;

        // We will need a Scanner object in order to read data from the keyboard
        Scanner scanner = new Scanner(System.in);

        // Prompt for reading the first number
        System.out.print("Enter the first integer number from the keyboard: ");
        a = scanner.nextInt();

        // Prompt for reading the second number
        System.out.print("Enter the second integer number from the keyboard: ");
        b = scanner.nextInt();

        // Compute the sum, difference and product
        int sum = a + b;
        int difference = a - b;
        int product = a * b;

        // Display the corresponding messages
        System.out.format("%d + %d = %d\n", a, b, sum);
        System.out.format("%d - %d = %d\n", a, b, difference);
        System.out.format("%d * %d = %d\n", a, b, product);

        // Close the scanner object
        scanner.close();
    }

}