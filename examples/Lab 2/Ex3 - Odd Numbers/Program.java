import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number from the keyboard: ");
        int number = scanner.nextInt();
        
        if (number % 2 == 1) {
            System.out.format("%d is an odd number\n", number);
        }

        scanner.close();
    }
}