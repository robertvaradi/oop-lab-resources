import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter an integer number from the keyboard: ");
        int number = scanner.nextInt();
        
        int remainder = number % 3;

        // IS THIS CORRECT?
        switch (remainder) {
            case 0: number *= 2;
            case 1: number = number * number;
            case 2: number--;
        }

        System.out.format("The result is %s\n", number);

        scanner.close();
    }
}