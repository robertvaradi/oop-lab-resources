import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number from the keyboard: ");
        int number = scanner.nextInt();

        number = switch(number % 3) {
            case 0 -> number * 2;
            case 1 -> number * number;
            case 2 -> number - 1;
        };

        System.out.format("The result is %d\n", number);

        scanner.close();
    }
}