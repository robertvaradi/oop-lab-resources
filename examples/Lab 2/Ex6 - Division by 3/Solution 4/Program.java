import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter an integer number from the keyboard: ");
        int number = scanner.nextInt();
        
        int remainder = number % 3;

        switch (remainder) {
            case 0: number *= 2; break;
            case 1: number = number * number; break;
            default: number--;
        }

        System.out.format("The result is %s\n", number);

        scanner.close();
    }
}