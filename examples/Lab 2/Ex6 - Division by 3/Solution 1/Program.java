import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter an integer number from the keyboard: ");
        int number = scanner.nextInt();

        int remainder = number % 3;
        
        if (remainder == 0) {
            number *= 2;
        } else if (remainder == 1) {
            number = number * number;
        } else if (remainder == 2) {
            number--;
        }

        System.out.format("The result is %d\n", number);
        scanner.close();
    }
}