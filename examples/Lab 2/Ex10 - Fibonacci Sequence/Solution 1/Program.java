public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter an integer number from the keyboard: ");
		int n = scanner.nextInt();
		
        // Validation
		while (n < 0) {
			System.out.print("You must enter a positive integer number. Try again: ");
			n = scanner.nextInt();
		}
		
        // Use the direct formula
		double sqrt5 = Math.sqrt(5.0);
		double fibonacciDirect = (Math.pow((1 + sqrt5) / 2.0, n) - Math.pow((1 - sqrt5) / 2.0, n)) / sqrt5;
		
        // Use the recursive formula
		int fibonacciRecursive = 0;
		
		if (n == 0) {
			fibonacciRecursive = 0;
		} else if (n == 1) {
			fibonacciRecursive = 1;
		} else {
			int f0 = 0;
			int f1 = 1;
			for (int i = 2; i <= n; i++) {
				fibonacciRecursive = f0 + f1;
				f0 = f1;
				f1 = fibonacciRecursive;
			}
		}
		
        // SHOULD THE TWO VALUES BE EQUAL?
        // WILL THE TWO VALUES BE EQUAL?
        // WHY?
        // IS IT CORRECT?
        // IF NOT, HOW CAN IT BE CORRECTED?
		if (fibonacciDirect == (double) fibonacciRecursive) {
			System.out.format("%f is equal to %d\n", fibonacciDirect, fibonacciRecursive);
		} else {
			System.out.format("%f is not equal to %d\n", fibonacciDirect, fibonacciRecursive);
		}
		
		scanner.close();
    }
}