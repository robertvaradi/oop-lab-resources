public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter a number from the keyboard: ");
        int number = scanner.nextInt();
        
        int copy;
        int reversed;
        
        // Everything could theoretically be done without needing a body for the
        // for statement. It is correct, however, in most of the times, not recommended
        for (reversed = 0, copy = number; 
            copy != 0; 
            reversed = reversed * 10 + copy % 10, copy /= 10);
        
        System.out.format("By reversing %d we get %d.", number, reversed);
        
        scanner.close();
    }
}