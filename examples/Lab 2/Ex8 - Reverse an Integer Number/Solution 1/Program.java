public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter a number from the keyboard: ");
        int number = scanner.nextInt();
        
        // Save a copy of the number, which can be changed
        int copy = number;
        int reversed;
        
        for (reversed = 0; copy != 0; copy /= 10) {
            reversed = reversed * 10 + copy % 10;
        }
        
        System.out.format("By reversing %d we get %d.", number, reversed);
        
        scanner.close();
    }
}