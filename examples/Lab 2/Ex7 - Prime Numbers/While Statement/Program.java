import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter an integer number from the keyboard: ");
        int number = scanner.nextInt();

        // Initialize the isPrime value, depending on the corner cases
        boolean isPrime = switch (number) {
            // 0 and 1 are not prime
            case 0 -> false;
            case 1 -> false;
            // 2 is prime
            case 2 -> true;
            // Assume about anything else that it is prime
            default -> true;
        };

        // If the initial assumption is that the number is prime
        // and the current divisor is smaller than the number / 2
        // continue checking the divisibility and stop when the number is for sure non-prime
        int divisor = 2;
        while (isPrime && divisor <= number / 2) {
            if (number % divisor == 0) {
                isPrime = false;
            }
            divisor++;
        }

        if (isPrime) {
            System.out.format("Number %d is a prime number\n", number);
        } else {
            System.out.format("Number %d is not a prime number\n", number);
        }

        scanner.close();
    }
}