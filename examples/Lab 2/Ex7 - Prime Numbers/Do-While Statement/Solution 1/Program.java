import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter an integer number from the keyboard: ");
        int number = scanner.nextInt();

        // Initialize the isPrime value, depending on the corner cases
        boolean isPrime = switch (number) {
            // 0 and 1 are not prime
            case 0 -> false;
            case 1 -> false;
            // 2 is prime
            case 2 -> true;
            // Assume about anything else that it is prime
            default -> true;
        };

        // Divide the number to the current divisor and check it is divisible
        // if so, the number cannot be prime. Continue while the assumption
        // is still that the number is prime and the divisor did not reach the half of the number
        // IS THIS CORRECT?
        // TRY WITH 2
        // WHAT IS MISSING?
        int divisor = 2;
        do {
            if (number % divisor == 0) {
                isPrime = false;
            }
        } while (isPrime && divisor <= number / 2);

        if (isPrime) {
            System.out.format("Number %d is a prime number\n", number);
        } else {
            System.out.format("Number %d is not a prime number\n", number);
        }

        scanner.close();
    }
}