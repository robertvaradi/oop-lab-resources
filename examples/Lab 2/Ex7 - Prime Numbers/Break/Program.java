public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number from the keyboard: ");
        int number = scanner.nextInt();
        
        // Initialization of the isPrime variable
        boolean isPrime = switch (number) {
            case 0 -> false;
            case 1 -> false;
            case 2 -> true;
            default -> true;
        };
        
        for (int divisor = 2; divisor <= number / 2; divisor++) {
            System.out.format("Checking primality with current divisor %d ...\n", divisor);
            // If we found a divisor, there is no point in continuing to check if the number is prime
            if (number % divisor == 0) {
                System.out.format("%d divides %d. Break the loop\n", divisor, number);
                isPrime = false;
                // The next iteration will not be executed
                break;
            }
        }
        
        if (isPrime) {
            System.out.format("Number %d is a prime number.", number);
        } else {
            System.out.format("Number %d is not a prime number.", number);
        }

        scanner.close();
    }
}