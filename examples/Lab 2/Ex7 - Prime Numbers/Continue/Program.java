public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number from the keyboard: ");
        int number = scanner.nextInt();
        
        // Initialization of the isPrime variable
        boolean isPrime = switch (number) {
            case 0 -> false;
            case 1 -> false;
            case 2 -> true;
            default -> true;
        };
        
        for (int divisor = 2; divisor <= number / 2 && isPrime; divisor++) {
            // If the number is odd and the divisor is even, there is no chance
            // the number will be divisible with the current divisor, so we can
            // go and try the next value
            if (number % 2 == 1 && divisor % 2 == 0) {
                System.out.format("Skipping for number %d ...\n", divisor);
                // It will jump to the next iteration
                continue;
            }
            if (number % divisor == 0) {
                System.out.format("Found divisor %d for number %d. Cannot be a prime number.\n", divisor, number);
                isPrime = false;
            }
        }
        
        if (isPrime) {
            System.out.format("Number %d is a prime number.", number);
        } else {
            System.out.format("Number %d is not a prime number.", number);
        }
        
        scanner.close();
    }
}