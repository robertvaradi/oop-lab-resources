package lab6.jcf.iterable;

import lab6.jcf.iterable.ArrayImpl.ArrayElement;

public class Program {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Array<Integer> array = new ArrayImpl<Integer>(20, 1.5);
		for (int i = 0; i < 100; i++) {
			array.insert(i, i);
		}
		System.out.print("[");
		for (ArrayElement<Integer> element : (Iterable<ArrayElement<Integer>>) array) {
			if (element.getIndex() < array.size() - 1) {
				System.out.print(String.format("%s, ", element.getElement().toString()));
			} else {
				System.out.print(element.getElement().toString());
			}
		}
		System.out.println("]");
		
		((Iterable<ArrayElement<Integer>>) array).forEach(e -> {
			System.out.println(String.format("%d -> %s", e.getIndex(), e.getElement().toString()));
		});
	}
}
