package lab6.jcf.iterable;

import java.util.Iterator;

public class ArrayImpl<T> implements Array<T>, Iterable<ArrayImpl.ArrayElement<T>> {
	private Integer size;
	private Integer capacity;
	private Object[] elements;
	private Double resizeFactor;
	
	public static class ArrayElement<T> {
		private Integer index;
		private T element;
		
		public ArrayElement(Integer index, T element) {
			this.index = index;
			this.element = element;
		}
		
		public Integer getIndex() {
			return index;
		}
		
		public T getElement() {
			return element;
		}
	}
	
	public static class ArrayIterator<T> implements Iterator<ArrayElement<T>> {
		private Array<T> subject;
		
		private Integer currentIndex;
		
		public ArrayIterator(Array<T> subject) {
			this.subject = subject;
			this.currentIndex = 0;
		}
		
		@Override
		public boolean hasNext() {
			return currentIndex < subject.size();
		}

		@Override
		public ArrayElement<T> next() {
			if (currentIndex < subject.size()) {
				return new ArrayElement<T>(currentIndex, subject.getElement(currentIndex++));
			}
			return null;
		}
		
	}
	
	public ArrayImpl(Integer capacity, Double resizeFactor) {
		this.size = 0;
		this.capacity = capacity;
		this.resizeFactor = resizeFactor;
		this.elements = new Object[capacity];
	}
	
	
	@Override
	public Iterator<ArrayElement<T>> iterator() {
		return new ArrayIterator<T>(this);
	}

	@Override
	public Integer size() {
		return size;
	}

	@Override
	public void insert(int position, T element) {
		if (size == capacity) {
			Integer newCapacity = (int) (capacity * resizeFactor);
			Object[] newElements = new Object[newCapacity];
			for (int i = 0; i < capacity; i++) {
				newElements[i] = elements[i];
			}
			elements = newElements;
			capacity = newCapacity;
		}
		for (int i = size; i > position; i--) {
			elements[i] = elements[i - 1];
		}
		elements[position] = element;
		size++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T remove(int position) {
		T element = (T) elements[position];
		for (int i = position; i < size - 1; i++) {
			elements[i] = elements[i + 1];
		}
		elements[size - 1] = null;
		size--;
		return element;
	}


	@SuppressWarnings("unchecked")
	@Override
	public T getElement(int position) {
		return (T) elements[position];
	}

}
