package lab6.jcf.iterable;

public interface Array<T> {
	public Integer size();
	public void insert(int position, T element);
	public T remove(int position);
	public T getElement(int position);
}
