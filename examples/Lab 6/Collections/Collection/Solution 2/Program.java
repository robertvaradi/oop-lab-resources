package lab6.jcf.collection;

import java.util.Collection;

public class Program {
	public static void main(String[] args) {
		Collection<Integer> array = new ArrayImpl<Integer>(20, 1.5);
		for (int i = 0; i < 100; i++) {
			array.add(i);
		}
		for (Integer element : array) {
			System.out.println(element);
		}
	}
}
