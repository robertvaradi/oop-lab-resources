package lab6.jcf.collection;

import java.util.AbstractCollection;
import java.util.Iterator;

public class ArrayImpl<T> extends AbstractCollection<T> {
	
	private Integer size;
	
	private Integer capacity;
	
	private Double resizeFactor;
	
	private Object[] elements;

	public static class ArrayIterator<T> implements Iterator<T> {

		private Integer currentIndex;
		
		private ArrayImpl<T> subject;
		
		public ArrayIterator(ArrayImpl<T> subject) {
			this.subject = subject;
			this.currentIndex = 0;
		}
		
		@Override
		public boolean hasNext() {
			return currentIndex < subject.size();
		}

		@Override
		public T next() {
			return currentIndex < subject.size() ? subject.getElement(currentIndex++) : null;
		}
		
	}
	
	public ArrayImpl(Integer capacity, Double resizeFactor) {
		this.capacity = capacity;
		this.resizeFactor = resizeFactor;
		this.size = 0;
		this.elements = new Object[capacity];
	}
	
	@Override
	public boolean add(T e) {
		if (size == capacity) {
			Integer newCapacity = (int) (capacity * resizeFactor);
			Object[] newElements = new Object[newCapacity];
			for (int i = 0; i < size; i++) {
				newElements[i] = elements[i];
			}
			elements = newElements;
			capacity = newCapacity;
		}
		elements[size++] = e;
		return true;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new ArrayIterator<T>(this);
	}

	@Override
	public int size() {
		return size;
	}
	
	@SuppressWarnings("unchecked")
	public T getElement(Integer index) {
		return (T) elements[index];
	}
	
}
