package lab6.jcf.collection;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ArrayImpl<T> implements Collection<T> {
	
	private Integer size;
	
	private Integer capacity;
	
	private Double resizeFactor;
	
	private Object[] elements;
	
	ArrayList<T> elems;
	
	public static class ArrayIterator<T> implements Iterator<T> {
		private ArrayImpl<T> array;
		
		private Integer currentIndex; 
		
		public ArrayIterator(ArrayImpl<T> array) {
			this.array = array;
			this.currentIndex = 0;
		}
		
		@Override
		public boolean hasNext() {
			return currentIndex < array.size();
		}

		@Override
		public T next() {
			return currentIndex < array.size() ? array.get(currentIndex++) : null;
		}
		
	}
	
	public ArrayImpl(Integer capacity, Double resizeFactor) {
		this.capacity = capacity;
		this.resizeFactor = resizeFactor;
		this.elements = new Object[capacity];
		this.size = 0;
	}
	
	
	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(Object o) {
		for (T element : this) {
			if (element.equals(o)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Iterator<T> iterator() {
		return new ArrayImpl.ArrayIterator<T>(this);
	}

	@Override
	public Object[] toArray() {
		Object[] ret = new Object[capacity];
		for (int i = 0; i < size(); i++) {
			ret[i] = elements[i];
		}
		return ret;
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	@Override
	public <T> T[] toArray(T[] a) {
		T[] ret = (T[]) Array.newInstance(a.getClass().arrayType().getComponentType(), size());
		for (int i = 0; i < size(); i++) {
			ret[i] = (T) elements[i];
		}
		return ret;
	}

	@Override
	public boolean add(T e) {
		if (size == capacity) {
			Integer newCapacity = (int) (capacity * resizeFactor);
			Object[] newElements = new Object[newCapacity];
			for (int i = 0; i < size; i++) {
				newElements[i] = elements[i];
			}
			elements = newElements;
			capacity = newCapacity;
		}
		elements[size++] = e;
		return true;
	}

	@Override
	public boolean remove(Object o) {
		if (isEmpty()) {
			return false;
		}
		
		int position = -1;
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(o)) {
				position = i;
				break;
			}
		}
		
		if (position != -1) {
			for (int i = position; i < size - 1; i++) {
				elements[i] = elements[i + 1];
			}
			elements[size - 1] = null;
			size--;
			return true;
		}
		
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		for (Object elem : c) {
			if (!contains(elem)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		for (T e : c) {
			if (!add(e)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		for (Object e : c) {
			if (!remove(e)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		int index = 0;
		while (index < size()) {
			if (!c.contains(get(index))) {
				if (!remove(get(index))) {
					return false; 
				}
			} else {
				index++;
			}
		}
		return true;
	}

	@Override
	public void clear() {
		while (!isEmpty()) {
			remove(get(0));
		}
	}
	
	@SuppressWarnings("unchecked")
	public T get(Integer index) {
		return (T) elements[index];
	}
}
