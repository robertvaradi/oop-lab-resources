package lab6.generics;

public class SortedDynamicArray<T extends Comparable<T>> extends DynamicArray<T> {
	private SortingMode sortingMode;

	public static enum SortingMode {
		ASC, DESC
	};

	public SortedDynamicArray(Integer capacity, Double resizeFactor, SortingMode sortingMode) {
		super(capacity, resizeFactor);
		this.sortingMode = sortingMode;
	}

	public SortingMode getSortingMode() {
		return sortingMode;
	}

	@Override
	public void insert(Integer position, T value) {
		// No implementation as it does not make sense for a sorted array
	}

	@Override
	public void insertFirst(T value) {
		// No implementation as it does not make sense for a sorted array
	}

	@Override
	public void insertLast(T value) {
		// No implementation as it does not make sense for a sorted array
	}

	@SuppressWarnings("unchecked")
	private Integer find(T value) {
		int low = 0;
		int high = getSize() - 1;
		int middle = (low + high) / 2;
		while (low <= high) {
			Comparable<Object> middleElement = (Comparable<Object>) getElement(middle);
			int comparisonValue = middleElement.compareTo(value);
			if ((comparisonValue < 0 && SortingMode.ASC.equals(sortingMode))
					|| (comparisonValue > 0 && SortingMode.DESC.equals(sortingMode))) {
				low = middle + 1;
			} else if ((comparisonValue > 0 && SortingMode.ASC.equals(sortingMode))
					|| (comparisonValue < 0 && SortingMode.DESC.equals(sortingMode))) {
				high = middle - 1;
			} else {
				return middle;
			}
			middle = (low + high) / 2;
		}
		return middle;
	}

	private Integer getSortedInsertPosition(T value) {
		Integer size = getSize();
		if (size == 0) {
			return 0;
		}
		T first = getElement(0);
		T last = getElement(getSize() - 1);
		int comparisonWithFirst = value.compareTo(first);
		int comparisonWithLast = value.compareTo(last);
		if ((comparisonWithFirst < 0 && SortingMode.ASC.equals(sortingMode))
				|| (comparisonWithFirst > 0 && SortingMode.DESC.equals(sortingMode))) {
			return 0;
		} else if ((comparisonWithLast > 0 && SortingMode.ASC.equals(sortingMode))
				|| (comparisonWithLast < 0 && SortingMode.DESC.equals(sortingMode))) {
			return size;
		} else {
			Integer initialPosition = find(value);
			T element = getElement(initialPosition);
			int comparisonResult = element.compareTo(value);
			if (comparisonResult == 0) {
				return initialPosition + 1;
			} else if ((comparisonResult < 0 && SortingMode.ASC.equals(sortingMode))
					|| (comparisonResult > 0 && SortingMode.DESC.equals(sortingMode))) {
				return initialPosition + 1;
			} else if ((comparisonResult > 0 && SortingMode.ASC.equals(sortingMode))
					|| (comparisonResult < 0 && SortingMode.DESC.equals(sortingMode))) {
				return initialPosition > 0 ? initialPosition - 1 : 0;
			}
		}
		return -1;
	}

	private Boolean isSorted() {
		Integer size = getSize();
		T first = getElement(0, false);
		T second = getElement(1, false);
		for (int i = 0; i < size - 1; i++) {
			if ((first.compareTo(second) > 0 && SortingMode.ASC.equals(sortingMode))
					|| (second.compareTo(first) > 0 && SortingMode.DESC.equals(sortingMode))) {
				return false;
			}
			first = second;
			if (i < size - 2) {
				second = getElement(i + 2, false);
			}
		}
		return true;
	}

	@Override
	public Boolean validate() {
		return super.validate() && isSorted();
	}

	public void insertOrdered(T value) {
		if (validate()) {
			Integer position = getSortedInsertPosition(value);
			super.insert(position, value);
		}
	}

	@Override
	public Integer search(T value) {
		Integer position = find(value);
		T element = getElement(position);
		return element == value ? position : -1;
	}

	@Override
	public void setElement(Integer position, T value) {
		// No implementation as it does not make sense for a sorted array
	}
}
