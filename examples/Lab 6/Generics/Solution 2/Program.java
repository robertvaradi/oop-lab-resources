package lab6.generics;

import lab6.generics.SortedDynamicArray.SortingMode;

public class Program {
	private static void testStaticArray() {
		StaticArray<Integer> array = new StaticArray<Integer>(20);
		System.out.println("array.getCapacity() = " + array.getCapacity());
		System.out.println("array.getSize() = " + array.getSize());
		System.out.println("array.isEmpty() = " + array.isEmpty());
		System.out.println("array.isFull() = " + array.isFull());
		for (int i = 0; i < 10; i++) {
			array.insert(i, 3 * i);
		}
		for (int i = 0; i < 10; i++) {
			array.insertFirst(3 * i + 1);
		}
		for (int i = 0; i < 10; i++) {
			array.insertLast(3 * i + 2);
		}
		array.display();
		System.out.println("array.getCapacity() = " + array.getCapacity());
		System.out.println("array.getSize() = " + array.getSize());
		System.out.println("array.isEmpty() = " + array.isEmpty());
		System.out.println("array.isFull() = " + array.isFull());
		System.out.println("array.getElement(5) = " + array.getElement(5));
		array.setElement(5, 100);
		System.out.println("array.getElement(5) = " + array.getElement(5));
		System.out.println("array.remove(5) = " + array.remove(5));
		System.out.println("array.getElement(5) = " + array.getElement(5));
		System.out.println("array.removeFirst() = " + array.removeFirst());
		System.out.println("array.removeLast() = " + array.removeLast());
		System.out.println("array.search(7) = " + array.search(7));
		System.out.println("array.search(100) = " + array.search(100));
		System.out.println("array.exists(7) = " + array.exists(7));
		System.out.println("array.exists(100) = " + array.exists(100));
		array.display();
	}
	
	private static void testDynamicArray() {
		System.out.println("Testing DynamicArray ...");
		DynamicArray<Integer> array = new DynamicArray<Integer>(20, 1.5);
		System.out.println("array.getCapacity() = " + array.getCapacity());
		System.out.println("array.getSize() = " + array.getSize());
		System.out.println("array.isEmpty() = " + array.isEmpty());
		System.out.println("array.isFull() = " + array.isFull());
		for (int i = 0; i < 10; i++) {
			array.insert(i, 3 * i);
		}
		for (int i = 0; i < 10; i++) {
			array.insertFirst(3 * i + 1);
		}
		for (int i = 0; i < 10; i++) {
			array.insertLast(3 * i + 2);
		}
		array.display();
		System.out.println("array.getCapacity() = " + array.getCapacity());
		System.out.println("array.getSize() = " + array.getSize());
		System.out.println("array.isEmpty() = " + array.isEmpty());
		System.out.println("array.isFull() = " + array.isFull());
		System.out.println("array.getElement(5) = " + array.getElement(5));
		array.setElement(5, 100);
		System.out.println("array.getElement(5) = " + array.getElement(5));
		System.out.println("array.remove(5) = " + array.remove(5));
		System.out.println("array.getElement(5) = " + array.getElement(5));
		System.out.println("array.removeFirst() = " + array.removeFirst());
		System.out.println("array.removeLast() = " + array.removeLast());
		System.out.println("array.search(7) = " + array.search(7));
		System.out.println("array.search(100) = " + array.search(100));
		System.out.println("array.exists(7) = " + array.exists(7));
		System.out.println("array.exists(100) = " + array.exists(100));
		array.display();
	}
	
	private static void testSortedDynamicArray() {
		System.out.println("Testing SortedDynamicArray ...");
		SortedDynamicArray<Integer> array = new SortedDynamicArray<Integer>(20, 1.5, SortingMode.ASC);
		System.out.println("array.getCapacity() = " + array.getCapacity());
		System.out.println("array.getSize() = " + array.getSize());
		System.out.println("array.isEmpty() = " + array.isEmpty());
		System.out.println("array.isFull() = " + array.isFull());
		for (int i = 0; i < 10; i++) {
			array.insert(i, 3 * i);
		}
		for (int i = 0; i < 10; i++) {
			array.insertFirst(3 * i + 1);
		}
		for (int i = 0; i < 10; i++) {
			array.insertLast(3 * i + 2);
		}
		array.display();
		System.out.println("array.getCapacity() = " + array.getCapacity());
		System.out.println("array.getSize() = " + array.getSize());
		System.out.println("array.isEmpty() = " + array.isEmpty());
		System.out.println("array.isFull() = " + array.isFull());
		System.out.println("array.getElement(5) = " + array.getElement(5));
		for (int i = 0; i < 10; i++) {
			array.insertOrdered(3 * i);
		}
		for (int i = 0; i < 10; i++) {
			array.insertOrdered(3 * i + 1);
		}
		for (int i = 0; i < 10; i++) {
			array.insertOrdered(3 * i + 2);
		}
		System.out.println("array.getCapacity() = " + array.getCapacity());
		System.out.println("array.getSize() = " + array.getSize());
		System.out.println("array.isEmpty() = " + array.isEmpty());
		System.out.println("array.isFull() = " + array.isFull());
		System.out.println("array.getElement(5) = " + array.getElement(5));
		array.setElement(5, 100);
		System.out.println("array.getElement(5) = " + array.getElement(5));
		System.out.println("array.remove(5) = " + array.remove(5));
		System.out.println("array.getElement(5) = " + array.getElement(5));
		System.out.println("array.removeFirst() = " + array.removeFirst());
		System.out.println("array.removeLast() = " + array.removeLast());
		System.out.println("array.search(7) = " + array.search(7));
		System.out.println("array.search(100) = " + array.search(100));
		System.out.println("array.exists(7) = " + array.exists(7));
		System.out.println("array.exists(100) = " + array.exists(100));
		array.display();
	}
	
	public static void main(String[] args) {
		testStaticArray();
		testDynamicArray();
		testSortedDynamicArray();
	}
}
