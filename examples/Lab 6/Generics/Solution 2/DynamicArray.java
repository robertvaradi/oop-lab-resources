package lab6.generics;

import java.lang.reflect.Array;

public class DynamicArray<T> extends StaticArray<T> {
	private Double resizeFactor;
	
	public DynamicArray(Integer capacity, Double resizeFactor) {
		super(capacity);
		this.resizeFactor = resizeFactor;
	}
	
	public Double getResizeFactor() {
		return resizeFactor;
	}

	@Override
	protected Boolean resizeAllowed() {
		return true;
	}

	@Override
	protected Integer newCapacity(Integer capacity) {
		return (int) (capacity * resizeFactor);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T[] resize(Integer oldCapacity, Integer newCapacity, T[] elements) {
		T[] ret = (T[]) Array.newInstance(elements.getClass().getComponentType(), newCapacity);
		for (int i = 0; i < oldCapacity; i++) {
			ret[i] = elements[i];
		}
		return ret;
	}

	@Override
	public Boolean validate() {
		return super.validate() && newCapacity(getCapacity()) > getCapacity();
	}
}
