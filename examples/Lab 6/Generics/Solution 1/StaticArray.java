package lab6.generics;

public class StaticArray {
	private Integer size;

	private Object[] elements;

	public static enum ArrayOp {
		GET, SET, INSERT, REMOVE
	}

	public StaticArray(int capacity) {
		this.size = 0;
		this.elements = new Object[capacity];
	}

	public Integer getCapacity() {
		return elements.length;
	}

	public Integer getSize() {
		return size;
	}

	public Boolean validate() {
		return elements != null && getCapacity() > 0 && size <= getCapacity();
	}

	public Boolean validatePosition(Integer position, ArrayOp op) {
		return position >= 0
				&& ((ArrayOp.INSERT.equals(op) && position <= size) || (!ArrayOp.INSERT.equals(op) && position < size));
	}
	
	public Object getElement(Integer position) {
		return getElement(position, true);
	}
	
	public Object getFirst() {
		return getElement(0);
	}
	
	public Object getLast() {
		return getElement(size - 1);
	}
	
	protected Object getElement(Integer position, Boolean doValidate) {
		if (!doValidate || (validate() && validatePosition(position, ArrayOp.GET))) {
			return elements[position];
		}
		return null;
	}

	public void setElement(Integer position, Object value) {
		if (validate() && validatePosition(position, ArrayOp.SET)) {
			elements[position] = value;
		}
	}

	public Boolean isEmpty() {
		return size == 0;
	}

	public Boolean isFull() {
		return size == getCapacity();
	}

	public void insert(Integer position, Object value) {
		if (validate() && validatePosition(position, ArrayOp.INSERT)) {
			if (isFull() && resizeAllowed()) {
				Integer newCapacity = newCapacity(getCapacity());
				elements = resize(getCapacity(), newCapacity, elements);
			}
			
			if (!isFull()) {
				for (int i = size; i > position; i--) {
					elements[i] = elements[i - 1];
				}
				elements[position] = value;
				size++;
			}
		}
	}

	public void insertFirst(Object value) {
		insert(0, value);
	}

	public void insertLast(Object value) {
		insert(size, value);
	}

	public Object remove(Integer position) {
		Object ret = null;
		if (validate() && validatePosition(position, ArrayOp.REMOVE)) {
			if (!isEmpty()) {
				ret = elements[position];
				for (int i = position; i < size - 1; i++) {
					elements[i] = elements[i + 1];
				}
				elements[size - 1] = 0;
				size--;
			}
		}
		return ret;
	}

	public Object removeFirst() {
		return remove(0);
	}

	public Object removeLast() {
		return remove(size - 1);
	}

	public void display() {
		if (validate()) {
			for (int i = 0; i < size; i++) {
				System.out.format("%s%s", elements[i].toString(), (i == size - 1) ? "\n" : ", ");
			}
		}
	}

	public Integer search(Object value) {
		if (validate()) {
			for (int i = 0; i < size; i++) {
				if (elements[i].equals(value)) {
					return i;
				}
			}
		}
		return -1;
	}

	public Boolean exists(int value) {
		return search(value) != -1;
	}
	
	protected Boolean resizeAllowed() {
		return false;
	}
	
	protected Integer newCapacity(Integer capacity) {
		return capacity;
	}
	
	protected Object[] resize(Integer oldCapacity, Integer newCapacity, Object[] elements) {
		return elements;
	}
}