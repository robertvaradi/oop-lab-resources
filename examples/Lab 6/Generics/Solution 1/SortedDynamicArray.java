package lab6.generics;

public class SortedDynamicArray extends DynamicArray {
	private SortingMode sortingMode;

	public static enum SortingMode {
		ASC, DESC
	};

	public SortedDynamicArray(Integer capacity, Double resizeFactor, SortingMode sortingMode) {
		super(capacity, resizeFactor);
		this.sortingMode = sortingMode;
	}

	public SortingMode getSortingMode() {
		return sortingMode;
	}

	@Override
	public void insert(Integer position, Object value) {
		// No implementation as it does not make sense for a sorted array
	}

	@Override
	public void insertFirst(Object value) {
		// No implementation as it does not make sense for a sorted array
	}

	@Override
	public void insertLast(Object value) {
		// No implementation as it does not make sense for a sorted array
	}

	@SuppressWarnings("unchecked")
	private Integer find(Comparable<Object> value) {
		int low = 0;
		int high = getSize() - 1;
		int middle = (low + high) / 2;
		while (low <= high) {
			Comparable<Object> middleElement = (Comparable<Object>) getElement(middle);
			int comparisonValue = middleElement.compareTo(value);
			if ((comparisonValue < 0 && SortingMode.ASC.equals(sortingMode))
					|| (comparisonValue > 0 && SortingMode.DESC.equals(sortingMode))) {
				low = middle + 1;
			} else if ((comparisonValue > 0 && SortingMode.ASC.equals(sortingMode))
					|| (comparisonValue < 0 && SortingMode.DESC.equals(sortingMode))) {
				high = middle - 1;
			} else {
				return middle;
			}
			middle = (low + high) / 2;
		}
		return middle;
	}

	@SuppressWarnings("unchecked")
	private Integer getSortedInsertPosition(Comparable<Object> value) {
		Integer size = getSize();
		if (size == 0) {
			return 0;
		}
		Comparable<Object> first = (Comparable<Object>) getElement(0);
		Comparable<Object> last = (Comparable<Object>) getElement(getSize() - 1);
		int comparisonWithFirst = value.compareTo(first);
		int comparisonWithLast = value.compareTo(last);
		if ((comparisonWithFirst < 0 && SortingMode.ASC.equals(sortingMode))
				|| (comparisonWithFirst > 0 && SortingMode.DESC.equals(sortingMode))) {
			return 0;
		} else if ((comparisonWithLast > 0 && SortingMode.ASC.equals(sortingMode))
				|| (comparisonWithLast < 0 && SortingMode.DESC.equals(sortingMode))) {
			return size;
		} else {
			Integer initialPosition = find(value);
			Comparable<Object> element = (Comparable<Object>) getElement(initialPosition);
			int comparisonResult = element.compareTo(value);
			if (comparisonResult == 0) {
				return initialPosition + 1;
			} else if ((comparisonResult < 0 && SortingMode.ASC.equals(sortingMode))
					|| (comparisonResult > 0 && SortingMode.DESC.equals(sortingMode))) {
				return initialPosition + 1;
			} else if ((comparisonResult > 0 && SortingMode.ASC.equals(sortingMode))
					|| (comparisonResult < 0 && SortingMode.DESC.equals(sortingMode))) {
				return initialPosition > 0 ? initialPosition - 1 : 0;
			}
		}
		return -1;
	}

	@SuppressWarnings("unchecked")
	private Boolean isSorted() {
		Integer size = getSize();
		Comparable<Object> first = (Comparable<Object>) getElement(0, false);
		Comparable<Object> second = (Comparable<Object>) getElement(1, false);
		for (int i = 0; i < size - 1; i++) {
			if ((first.compareTo(second) > 0 && SortingMode.ASC.equals(sortingMode))
					|| (second.compareTo(first) > 0 && SortingMode.DESC.equals(sortingMode))) {
				return false;
			}
			first = second;
			if (i < size - 2) {
				second = (Comparable<Object>) getElement(i + 2, false);
			}
		}
		return true;
	}

	@Override
	public Boolean validate() {
		return super.validate() && isSorted();
	}

	@SuppressWarnings("unchecked")
	public void insertOrdered(Object value) {
		if (validate()) {
			Integer position = getSortedInsertPosition((Comparable<Object>) value);
			super.insert(position, value);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer search(Object value) {
		Integer position = find((Comparable<Object>) value);
		Comparable<Object> element = (Comparable<Object>) getElement(position);
		return element == value ? position : -1;
	}

	@Override
	public void setElement(Integer position, Object value) {
		// No implementation as it does not make sense for a sorted array
	}
}
