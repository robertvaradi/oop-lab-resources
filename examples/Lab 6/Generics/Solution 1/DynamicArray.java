package lab6.generics;

public class DynamicArray extends StaticArray {
	private Double resizeFactor;
	
	public DynamicArray(Integer capacity, Double resizeFactor) {
		super(capacity);
		this.resizeFactor = resizeFactor;
	}
	
	public Double getResizeFactor() {
		return resizeFactor;
	}

	@Override
	protected Boolean resizeAllowed() {
		return true;
	}

	@Override
	protected Integer newCapacity(Integer capacity) {
		return (int) (capacity * resizeFactor);
	}

	@Override
	protected Object[] resize(Integer oldCapacity, Integer newCapacity, Object[] elements) {
		Object[] ret = new Object[newCapacity];
		for (int i = 0; i < oldCapacity; i++) {
			ret[i] = elements[i];
		}
		return ret;
	}

	@Override
	public Boolean validate() {
		return super.validate() && newCapacity(getCapacity()) > getCapacity();
	}
}
