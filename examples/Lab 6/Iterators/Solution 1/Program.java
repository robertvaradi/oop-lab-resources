package lab6.iterators;

import java.util.Iterator;

import lab6.iterators.SortedDynamicArray.SortingMode;

public class Program {
	private static void testIterator(StaticArray<Integer> arr) {
		Iterator<Integer> iter = new ArrayIterator<Integer>(arr);
		while(iter.hasNext()) {
			System.out.println(iter.next());
		}
		System.out.println(iter.next());
	}
	
	private static StaticArray<Integer> makeStaticArray(Integer capacity) {
		StaticArray<Integer> arr = new StaticArray<Integer>(capacity);
		for (int i = 0; i < capacity; i++) {
			arr.insertLast(i + 1);
		}
		return arr;
	}
	
	private static DynamicArray<Integer> makeDynamicArray(Integer capacity, Double resizeFactor) {
		DynamicArray<Integer> arr = new DynamicArray<Integer>(capacity, resizeFactor);
		for (int i = 0; i < capacity; i++) {
			arr.insertLast(i + 1);
		}
		return arr;
	}
	
	private static SortedDynamicArray<Integer> makeSortedDynamicArray(Integer capacity, Double resizeFactor, SortingMode mode) {
		SortedDynamicArray<Integer> arr = new SortedDynamicArray<Integer>(capacity, resizeFactor, mode);
		for (int i = 0; i < capacity; i++) {
			arr.insertOrdered(i + 1);
		}
		return arr;
	}
	
	public static void main(String[] args) {
		testIterator(makeStaticArray(20));
		testIterator(makeDynamicArray(20, 1.5));
		testIterator(makeSortedDynamicArray(20, 1.5, SortingMode.DESC));	
	}
}
