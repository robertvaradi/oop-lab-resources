package lab6.iterators;

import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T> {

	private StaticArray<T> array;
	
	private Integer currentIndex;
	
	public ArrayIterator(StaticArray<T> array) {
		this.array = array;
		this.currentIndex = 0;
	}
	
	@Override
	public boolean hasNext() {
		return currentIndex < array.getSize();
	}

	@Override
	public T next() {
		if (currentIndex >= array.getSize()) {
			return null;
		}
		return array.getElement(currentIndex++);
	}

}
