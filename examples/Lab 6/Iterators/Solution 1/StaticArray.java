package lab6.iterators;

public class StaticArray<T> {
	private Integer size;

	private Object[] elements;

	public static enum ArrayOp {
		GET, SET, INSERT, REMOVE
	}

	public StaticArray(Integer capacity) {
		this.size = 0;
		this.elements = new Object[capacity];
	}

	public Integer getCapacity() {
		return elements.length;
	}

	public Integer getSize() {
		return size;
	}

	public Boolean validate() {
		return elements != null && getCapacity() > 0 && size <= getCapacity();
	}

	public Boolean validatePosition(Integer position, ArrayOp op) {
		return position >= 0
				&& ((ArrayOp.INSERT.equals(op) && position <= size) || (!ArrayOp.INSERT.equals(op) && position < size));
	}
	
	public T getElement(Integer position) {
		return getElement(position, true);
	}
	
	public T getFirst() {
		return getElement(0);
	}
	
	public T getLast() {
		return getElement(size - 1);
	}
	
	@SuppressWarnings("unchecked")
	protected T getElement(Integer position, Boolean doValidate) {
		if (!doValidate || (validate() && validatePosition(position, ArrayOp.GET))) {
			return (T) elements[position];
		}
		return null;
	}

	public void setElement(Integer position, T value) {
		if (validate() && validatePosition(position, ArrayOp.SET)) {
			elements[position] = value;
		}
	}

	public Boolean isEmpty() {
		return size == 0;
	}

	public Boolean isFull() {
		return size == getCapacity();
	}

	@SuppressWarnings("unchecked")
	public void insert(Integer position, T value) {
		if (validate() && validatePosition(position, ArrayOp.INSERT)) {
			if (isFull() && resizeAllowed()) {
				Integer newCapacity = newCapacity(getCapacity());
				elements = resize(getCapacity(), newCapacity, (T[]) elements);
			}
			
			if (!isFull()) {
				for (int i = size; i > position; i--) {
					elements[i] = elements[i - 1];
				}
				elements[position] = value;
				size++;
			}
		}
	}

	public void insertFirst(T value) {
		insert(0, value);
	}

	public void insertLast(T value) {
		insert(size, value);
	}

	@SuppressWarnings("unchecked")
	public T remove(Integer position) {
		T ret = null;
		if (validate() && validatePosition(position, ArrayOp.REMOVE)) {
			if (!isEmpty()) {
				ret = (T) elements[position];
				for (int i = position; i < size - 1; i++) {
					elements[i] = elements[i + 1];
				}
				elements[size - 1] = null;
				size--;
			}
		}
		return ret;
	}

	public T removeFirst() {
		return remove(0);
	}

	public T removeLast() {
		return remove(size - 1);
	}

	public void display() {
		if (validate()) {
			for (int i = 0; i < size; i++) {
				System.out.format("%s%s", elements[i].toString(), (i == size - 1) ? "\n" : ", ");
			}
		}
	}

	public Integer search(T value) {
		if (validate()) {
			for (int i = 0; i < size; i++) {
				if (elements[i].equals(value)) {
					return i;
				}
			}
		}
		return -1;
	}

	public Boolean exists(T value) {
		return search(value) != -1;
	}
	
	protected Boolean resizeAllowed() {
		return false;
	}
	
	protected Integer newCapacity(Integer capacity) {
		return capacity;
	}
	
	protected T[] resize(Integer oldCapacity, Integer newCapacity, T[] elements) {
		return elements;
	}
}