package lab6.iterators;

public class ArrayElement<T> {
	private T element;
	
	private Integer index;
	
	public ArrayElement(Integer index, T element) {
		this.index = index;
		this.element = element;
	}
	
	public T getElement() {
		return element;
	}
	
	public Integer getIndex() {
		return index;
	}
}
