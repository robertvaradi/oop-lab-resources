#pragma once

#include "st_array.h"

typedef struct
{
	st_array_t* wrapped;
	float resize_factor;
} dyn_array_t;

dyn_array_t* dyn_array_new(unsigned int capacity, float resize_factor);
unsigned int dyn_array_size(dyn_array_t* arr);
unsigned int dyn_array_capacity(dyn_array_t* arr);
float dyn_array_resize_factor(dyn_array_t* arr);
bool dyn_array_is_empty(dyn_array_t* arr);
bool dyn_array_is_full(dyn_array_t* arr);
int dyn_array_get_element(dyn_array_t* arr, unsigned int position);
dyn_array_t* dyn_array_set_element(dyn_array_t* arr, unsigned int position, int element);
dyn_array_t* dyn_array_insert(dyn_array_t* arr, unsigned int position, int element);
dyn_array_t* dyn_array_insert_first(dyn_array_t* arr, int element);
dyn_array_t* dyn_array_insert_last(dyn_array_t* arr, int element);
int dyn_array_remove(dyn_array_t* arr, unsigned int position);
int dyn_array_remove_first(dyn_array_t* arr);
int dyn_array_remove_last(dyn_array_t* arr);
void dyn_array_destroy(dyn_array_t** arr);
void dyn_array_display(dyn_array_t* arr);
bool dyn_array_validate(dyn_array_t* arr);
bool dyn_array_validate_position(dyn_array_t* arr, unsigned int position, array_op_t op);
int dyn_array_search(dyn_array_t* arr, int value);
bool dyn_array_element_exists(dyn_array_t* arr, int value);
void test_dyn_array();
void dyn_array_display_info(dyn_array_t* arr);