#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "array.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

static array_t* _array_make_generic(array_type_t type, unsigned int capacity, float resize_factor, sorting_mode_t mode);

static void _test_static_array();
static void _test_dynamic_array();
static void _test_sorted_array();

array_t* array_make_static(unsigned int capacity)
{
	return _array_make_generic(array_type_static, capacity, 0.0f, sorting_mode_invalid);
}

array_t* array_make_dynamic(unsigned int capacity, float resize_factor)
{
	return _array_make_generic(array_type_dynamic, capacity, resize_factor, sorting_mode_invalid);
}

array_t* array_make_sorted(unsigned int capacity, float resize_factor, sorting_mode_t mode)
{
	return _array_make_generic(array_type_sorted, capacity, resize_factor, mode);
}

unsigned int array_size(array_t* arr)
{
	if (array_validate(arr))
	{
		if (arr->type == array_type_static)
		{
			return st_array_size(arr->array_data.static_array);
		}
		else if (arr->type == array_type_dynamic)
		{
			return dyn_array_size(arr->array_data.dynamic_array);
		}
		else if (arr->type == array_type_sorted)
		{
			return srt_dyn_array_size(arr->array_data.sorted_array);
		}
	}
	return (unsigned int)-1;
}

unsigned int array_capacity(array_t* arr)
{
	if (array_validate(arr))
	{
		if (arr->type == array_type_static)
		{
			return st_array_capacity(arr->array_data.static_array);
		}
		else if (arr->type == array_type_dynamic)
		{
			return dyn_array_capacity(arr->array_data.dynamic_array);
		}
		else if (arr->type == array_type_sorted)
		{
			return srt_dyn_array_capacity(arr->array_data.sorted_array);
		}
	}
	return (unsigned int)-1;
}

array_type_t array_type(array_t* arr)
{
	if (array_validate(arr))
	{
		return arr->type;
	}
	return array_type_invalid;
}

float array_resize_factor(array_t* arr)
{
	if (array_validate(arr))
	{
		assert(arr->type != array_type_static);
		if (arr->type == array_type_dynamic)
		{
			return dyn_array_resize_factor(arr->array_data.dynamic_array);
		}
		else if (arr->type == array_type_sorted)
		{
			return srt_dyn_array_resize_factor(arr->array_data.sorted_array);
		}
	}
	return 0.0f;
}

sorting_mode_t array_sorting_mode(array_t* arr)
{
	if (array_validate(arr))
	{
		assert(arr->type = array_type_sorted);
		return srt_dyn_array_sorting_mode(arr->array_data.sorted_array);
	}
	return sorting_mode_invalid;
}

array_t* array_insert(array_t* arr, unsigned int position, int element)
{
	if (array_validate(arr) && array_validate_position(arr, position, array_op_insert))
	{
		assert(arr->type != array_type_sorted);
		if (arr->type == array_type_static)
		{
			arr->array_data.static_array = st_array_insert(arr->array_data.static_array, position, element);
		}
		else if (arr->type == array_type_dynamic)
		{
			arr->array_data.dynamic_array = dyn_array_insert(arr->array_data.dynamic_array, position, element);
		}
	}
	return arr;
}

array_t* array_insert_first(array_t* arr, int element)
{
	return array_insert(arr, 0, element);
}

array_t* array_insert_last(array_t* arr, int element)
{
	return array_insert(arr, array_size(arr), element);
}

array_t* array_insert_ordered(array_t* arr, int element)
{
	if (array_validate(arr))
	{
		assert(arr->type == array_type_sorted);
		arr->array_data.sorted_array = srt_dyn_array_insert_ordered(arr->array_data.sorted_array, element);
	}
	return arr;
}

int array_remove(array_t* arr, unsigned int position)
{
	int result;
	result = -1;
	if (array_validate(arr) && array_validate_position(arr, position, array_op_remove))
	{
		if (arr->type == array_type_static)
		{
			result = st_array_remove(arr->array_data.static_array, position);
		}
		else if (arr->type == array_type_dynamic)
		{
			result = dyn_array_remove(arr->array_data.dynamic_array, position);
		}
		else if (arr->type == array_type_sorted)
		{
			result = srt_dyn_array_remove(arr->array_data.sorted_array, position);
		}
	}
	return result;
}

int array_remove_first(array_t* arr)
{
	return array_remove(arr, 0);
}

int array_remove_last(array_t* arr)
{
	return array_remove(arr, array_size(arr) - 1);
}

void array_display(array_t* arr)
{
	if (array_validate(arr))
	{
		if (arr->type == array_type_static)
		{
			st_array_display(arr->array_data.static_array);
		}
		else if (arr->type == array_type_dynamic)
		{
			dyn_array_display(arr->array_data.dynamic_array);
		}
		else if (arr->type == array_type_sorted)
		{
			srt_dyn_array_display(arr->array_data.sorted_array);
		}
	}
}

void array_destroy(array_t** arr)
{
	if (arr != NULL && *arr != NULL)
	{
		if ((*arr)->type == array_type_static)
		{
			st_array_destroy(&(*arr)->array_data.static_array);
		}
		else if ((*arr)->type == array_type_dynamic)
		{
			dyn_array_destroy(&(*arr)->array_data.dynamic_array);
		}
		else if ((*arr)->type == array_type_sorted)
		{
			srt_dyn_array_destroy(&(*arr)->array_data.sorted_array);
		}
		memset(*arr, 0, sizeof(array_t));
		free(*arr);
		*arr = NULL;
	}
}

bool array_is_empty(array_t* arr)
{
	if (array_validate(arr))
	{
		if (arr->type == array_type_static)
		{
			return st_array_is_empty(arr->array_data.static_array);
		}
		else if (arr->type == array_type_dynamic)
		{
			return dyn_array_is_empty(arr->array_data.dynamic_array);
		}
		else if (arr->type == array_type_sorted)
		{
			return srt_dyn_array_is_empty(arr->array_data.sorted_array);
		}
	}
	return false;
}

int array_search(array_t* arr, int element)
{
	if (array_validate(arr))
	{
		if (arr->type == array_type_static)
		{
			return st_array_search(arr->array_data.static_array, element);
		}
		else if (arr->type == array_type_dynamic)
		{
			return dyn_array_search(arr->array_data.dynamic_array, element);
		}
		else if (arr->type == array_type_sorted)
		{
			return srt_dyn_array_search(arr->array_data.sorted_array, element);
		}
	}
	return -1;
}

bool array_element_exists(array_t* arr, int element)
{
	return array_search(arr, element) != -1;
}

int array_get_element(array_t* arr, unsigned int position)
{
	if (array_validate(arr) && array_validate_position(arr, position, array_op_get))
	{
		if (arr->type == array_type_static)
		{
			return st_array_get_element(arr->array_data.static_array, position);
		}
		else if (arr->type == array_type_dynamic)
		{
			return dyn_array_get_element(arr->array_data.dynamic_array, position);
		}
		else if (arr->type == array_type_sorted)
		{
			return srt_dyn_array_get_element(arr->array_data.sorted_array, position);
		}
	}
	return -1;
}

array_t* array_set_element(array_t* arr, unsigned int position, int element)
{
	if (array_validate(arr) && array_validate_position(arr, position, array_op_set))
	{
		assert(arr->type != array_type_sorted);
		if (arr->type == array_type_static)
		{
			arr->array_data.static_array = st_array_set_element(arr->array_data.static_array, position, element);
		}
		else if (arr->type == array_type_dynamic)
		{
			arr->array_data.dynamic_array = dyn_array_set_element(arr->array_data.dynamic_array, position, element);
		}
	}
	return arr;
}

bool array_validate(array_t* arr)
{
	assert(arr != NULL);
	assert(arr->type == array_type_static || arr->type == array_type_dynamic || arr->type == array_type_sorted);
	if (arr->type == array_type_static)
	{
		assert(st_array_validate(arr->array_data.static_array));
	}
	else if (arr->type == array_type_dynamic)
	{
		assert(dyn_array_validate(arr->array_data.dynamic_array));
	}
	else if (arr->type == array_type_sorted)
	{
		assert(srt_dyn_array_validate(arr->array_data.sorted_array));
	}
	return true;
}

bool array_validate_position(array_t* arr, unsigned int position, array_op_t op)
{
	assert(arr != NULL);
	assert(arr->type == array_type_static || arr->type == array_type_dynamic || arr->type == array_type_sorted);
	if (arr->type == array_type_static)
	{
		assert(st_array_validate_position(arr->array_data.static_array, position, op));
	}
	else if (arr->type == array_type_dynamic)
	{
		assert(dyn_array_validate_position(arr->array_data.dynamic_array, position, op));
	}
	else if (arr->type == array_type_sorted)
	{
		assert(srt_dyn_array_validate_position(arr->array_data.sorted_array, position, op));
	}
	return true;
}

static array_t* _array_make_generic(array_type_t type, unsigned int capacity, float resize_factor, sorting_mode_t mode)
{
	array_t* arr = (array_t*)calloc(1, sizeof(array_t));
	bool success = true;
	if (arr == NULL)
	{
		printf("calloc: Failed to allocate memory for the array_t data structure: %s\n", strerror(errno));
	}
	else
	{
		if (type == array_type_static)
		{
			arr->array_data.static_array = st_array_new(capacity);
			if (arr->array_data.static_array == NULL)
			{
				printf("st_array_new: Failed to create the static array object!\n");
				success = false;
			}
		}
		else if (type == array_type_dynamic)
		{
			arr->array_data.dynamic_array = dyn_array_new(capacity, resize_factor);
			if (arr->array_data.dynamic_array == NULL)
			{
				printf("dyn_array_new: Failed to create the dynamic array object!\n");
				success = false;
;			}
		}
		else if (type == array_type_sorted)
		{
			arr->array_data.sorted_array = srt_dyn_array_new(capacity, resize_factor, mode);
			if (arr->array_data.sorted_array == NULL)
			{
				printf("srt_dyn_array_new: Failed to create the sorted dynamic array object!\n");
				success = false;
			}
		}
	}
	if (arr != NULL && !success)
	{
		array_destroy(&arr);
	}
	else
	{
		arr->type = type;
	}
	return arr;
}

void test_array(array_type_t type)
{
	if (type == array_type_static)
	{
		_test_static_array();
	}
	else if (type == array_type_dynamic)
	{
		_test_dynamic_array();
	}
	else if (type == array_type_sorted)
	{
		_test_sorted_array();
	}
}

static char* _array_type_to_string(array_type_t type)
{
	if (type == array_type_static)
	{
		return "static";
	}
	else if (type == array_type_dynamic)
	{
		return "dynamic";
	}
	else if (type == array_type_sorted)
	{
		return "sorted";
	}
	return "invalid";
}

void array_display_info(array_t* arr)
{
	printf("Array type: \"%s\"\n", _array_type_to_string(arr->type));
	if (arr->type == array_type_static)
	{
		st_array_display_info(arr->array_data.static_array);
	}
	else if (arr->type == array_type_dynamic)
	{
		dyn_array_display_info(arr->array_data.dynamic_array);
	}
	else if (arr->type == array_type_sorted)
	{
		srt_dyn_array_display_info(arr->array_data.sorted_array);
	}
}

static void _test_static_array()
{
	unsigned int initial_capacity = 20;
	unsigned int iteration_count = 5;
	array_t* arr = array_make_static(initial_capacity);
	assert(array_capacity(arr) == initial_capacity);
	assert(array_size(arr) == 0);
	assert(array_is_empty(arr));
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		array_insert(arr, i, 3 * i);
		assert(array_capacity(arr) == initial_capacity);
		assert(array_size(arr) == i + 1);
		assert(array_get_element(arr, i) == 3 * i);
		assert(!array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		array_insert_first(arr, 3 * i + 1);
		assert(array_capacity(arr) == initial_capacity);
		assert(array_size(arr) == i + 1 + iteration_count);
		assert(array_get_element(arr, 0) == 3 * i + 1);
		assert(!array_is_empty(arr));
	}
	for (unsigned int i = 0; i < 5; i++)
	{
		array_insert_last(arr, 3 * i + 2);
		assert(array_capacity(arr) == initial_capacity);
		assert(array_size(arr) == i + 1 + 2 * iteration_count);
		assert(array_get_element(arr, array_size(arr) - 1) == 3 * i + 2);
		assert(!array_is_empty(arr));
	}
	for (unsigned int i = 0; i < 3 * iteration_count; i++)
	{
		if (i % 3 == 0)
		{
			assert(array_search(arr, i) == (i / 3) + iteration_count);
		}
		else if (i % 3 == 1)
		{
			assert(array_search(arr, i) == iteration_count - ((i - 1) / 3) - 1);
		}
		else if (i % 3 == 2)
		{
			assert(array_search(arr, i) == ((i - 2) / 3) + 2 * iteration_count);
		}
		assert(array_element_exists(arr, i));
	}
	unsigned int size = array_size(arr);
	assert(array_search(arr, 3 * iteration_count + 2) == -1);
	assert(!array_element_exists(arr, 3 * iteration_count + 2));
	array_set_element(arr, iteration_count, 100);
	assert(array_get_element(arr, iteration_count) == 100);
	assert(array_search(arr, 100) == iteration_count);
	assert(array_element_exists(arr, 100));
	assert(size == array_size(arr));
	for (int i = iteration_count - 1; i >= 0; i--)
	{
		assert(array_remove_last(arr) == 3 * i + 2);
		assert(array_size(arr) == size - iteration_count + i);
		assert(array_capacity(arr) == initial_capacity);
	}
	size = array_size(arr);
	for (int i = 0; i < iteration_count; i++)
	{
		assert(array_remove_first(arr) == 3 * (iteration_count - i - 1) + 1);
		assert(array_size(arr) == size - i - 1);
		assert(array_capacity(arr) == initial_capacity);
	}
	size = array_size(arr);
	for (int i = 0; i < iteration_count; i++)
	{
		if (i == iteration_count - 1)
		{
			assert(array_remove(arr, iteration_count - i - 1) == 100);
		}
		else
		{
			assert(array_remove(arr, iteration_count - i - 1) == 3 * (iteration_count - i - 1));
		}
		assert(array_size(arr) == size - i - 1);
		assert(array_capacity(arr) == initial_capacity);
	}
	assert(array_is_empty(arr));
	array_destroy(&arr);
	assert(arr == NULL);
}

static void _test_dynamic_array()
{
	unsigned int initial_capacity = 20;
	unsigned int iteration_count = 10;
	unsigned int expected_capacity = initial_capacity;
	float resize_factor = 1.5f;
	array_t* arr = array_make_dynamic(initial_capacity, resize_factor);
	assert(array_capacity(arr) == initial_capacity);
	assert(array_size(arr) == 0);
	assert(array_is_empty(arr));
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (array_size(arr) == array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		array_insert(arr, i, 3 * i);
		assert(array_capacity(arr) == expected_capacity);
		assert(array_size(arr) == i + 1);
		assert(array_get_element(arr, i) == 3 * i);
		assert(!array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (array_size(arr) == array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		array_insert_first(arr, 3 * i + 1);
		assert(array_capacity(arr) == expected_capacity);
		assert(array_size(arr) == i + 1 + iteration_count);
		assert(array_get_element(arr, 0) == 3 * i + 1);
		assert(!array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (array_size(arr) == array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		array_insert_last(arr, 3 * i + 2);
		assert(array_capacity(arr) == expected_capacity);
		assert(array_size(arr) == i + 1 + 2 * iteration_count);
		assert(array_get_element(arr, array_size(arr) - 1) == 3 * i + 2);
		assert(!array_is_empty(arr));
	}
	for (unsigned int i = 0; i < 3 * iteration_count; i++)
	{
		if (i % 3 == 0)
		{
			assert(array_search(arr, i) == (i / 3) + iteration_count);
		}
		else if (i % 3 == 1)
		{
			assert(array_search(arr, i) == iteration_count - ((i - 1) / 3) - 1);
		}
		else if (i % 3 == 2)
		{
			assert(array_search(arr, i) == ((i - 2) / 3) + 2 * iteration_count);
		}
		assert(array_element_exists(arr, i));
	}
	unsigned int size = array_size(arr);
	assert(array_search(arr, 3 * iteration_count + 2) == -1);
	assert(!array_element_exists(arr, 3 * iteration_count + 2));
	array_set_element(arr, iteration_count, 100);
	assert(array_get_element(arr, iteration_count) == 100);
	assert(array_search(arr, 100) == iteration_count);
	assert(array_element_exists(arr, 100));
	assert(size == array_size(arr));
	for (int i = iteration_count - 1; i >= 0; i--)
	{
		assert(array_remove_last(arr) == 3 * i + 2);
		assert(array_size(arr) == size - iteration_count + i);
		assert(array_capacity(arr) == expected_capacity);
	}
	size = array_size(arr);
	for (int i = 0; i < iteration_count; i++)
	{
		assert(array_remove_first(arr) == 3 * (iteration_count - i - 1) + 1);
		assert(array_size(arr) == size - i - 1);
		assert(array_capacity(arr) == expected_capacity);
	}
	size = array_size(arr);
	for (int i = 0; i < iteration_count; i++)
	{
		if (i == iteration_count - 1)
		{
			assert(array_remove(arr, iteration_count - i - 1) == 100);
		}
		else
		{
			assert(array_remove(arr, iteration_count - i - 1) == 3 * (iteration_count - i - 1));
		}
		assert(array_size(arr) == size - i - 1);
		assert(array_capacity(arr) == expected_capacity);
	}
	assert(array_is_empty(arr));
	array_destroy(&arr);
	assert(arr == NULL);
}

static void _test_sorted_array()
{
	unsigned int initial_capacity = 20;
	unsigned int iteration_count = 10;
	unsigned int expected_capacity = initial_capacity;
	float resize_factor = 1.5f;
	array_t* arr = array_make_sorted(initial_capacity, resize_factor, sorting_mode_asc);
	assert(array_capacity(arr) == initial_capacity);
	assert(array_size(arr) == 0);
	assert(array_is_empty(arr));
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (array_size(arr) == array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		array_insert_ordered(arr, 3 * i);
		assert(array_capacity(arr) == expected_capacity);
		assert(array_size(arr) == i + 1);
		assert(array_get_element(arr, i) == 3 * i);
		assert(!array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (array_size(arr) == array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		array_insert_ordered(arr, 3 * i + 1);
		assert(array_capacity(arr) == expected_capacity);
		assert(array_size(arr) == i + 1 + iteration_count);
		assert(array_get_element(arr, 2 * i + 1) == 3 * i + 1);
		assert(!array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (array_size(arr) == array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		array_insert_ordered(arr, 3 * i + 2);
		assert(array_capacity(arr) == expected_capacity);
		assert(array_size(arr) == i + 1 + 2 * iteration_count);
		assert(array_get_element(arr, 3 * i + 2) == 3 * i + 2);
		assert(!array_is_empty(arr));
	}
	for (unsigned int i = 0; i < 3 * iteration_count; i++)
	{
		assert(array_search(arr, i) == i);
		assert(array_element_exists(arr, i));
	}
	unsigned int size = array_size(arr);
	assert(array_search(arr, 3 * iteration_count + 2) == -1);
	assert(!array_element_exists(arr, 3 * iteration_count + 2));
	assert(size == array_size(arr));
	int max_value = 3 * iteration_count - 1;
	for (int i = iteration_count - 1; i >= 0; i--)
	{
		assert(array_remove_last(arr) == max_value--);
		assert(array_size(arr) == size - iteration_count + i);
		assert(array_capacity(arr) == expected_capacity);
	}
	size = array_size(arr);
	int min_value = 0;
	for (int i = 0; i < iteration_count; i++)
	{
		assert(array_remove_first(arr) == min_value++);
		assert(array_size(arr) == size - i - 1);
		assert(array_capacity(arr) == expected_capacity);
	}
	size = array_size(arr);
	max_value = 2 * iteration_count - 1;
	for (int i = 0; i < iteration_count; i++)
	{
		assert(array_remove(arr, iteration_count - i - 1) == max_value--);
		assert(array_size(arr) == size - i - 1);
		assert(array_capacity(arr) == expected_capacity);
	}
	assert(array_is_empty(arr));
	array_destroy(&arr);
	assert(arr == NULL);
}

bool array_is_full(array_t* arr)
{
	if (array_validate(arr))
	{
		if (arr->type == array_type_static)
		{
			return st_array_is_full(arr->array_data.static_array);
		}
	}
	return false;
}