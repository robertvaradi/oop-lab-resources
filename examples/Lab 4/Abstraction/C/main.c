#include "array.h"
#include <stdio.h>
#include <assert.h>


int main()
{
    test_array(array_type_static);
    test_array(array_type_dynamic);
    test_array(array_type_sorted);

    array_t* arr = array_make_static(20);
    for (unsigned int i = 0; i < 10; i++)
	{
		array_insert(arr, i, 3 * i);
	}
	for (unsigned int i = 0; i < 10; i++)
	{
		array_insert_first(arr, 3 * i + 1);
	}
	for (unsigned int i = 0; i < 10; i++)
	{
		array_insert_last(arr, 3 * i + 2);
	}
	array_display(arr);
    array_destroy(&arr);

    arr = array_make_dynamic(20, 1.5f);
    for (unsigned int i = 0; i < 10; i++)
	{
		array_insert(arr, i, 3 * i);
	}
	for (unsigned int i = 0; i < 10; i++)
	{
		array_insert_first(arr, 3 * i + 1);
	}
	for (unsigned int i = 0; i < 10; i++)
	{
		array_insert_last(arr, 3 * i + 2);
	}
	array_display(arr);
    array_destroy(&arr);

	arr = array_make_sorted(20, 1.5f, sorting_mode_asc);
	for (unsigned int i = 0; i < 10; i++)
	{
		array_insert_ordered(arr, 3 * i);
	}
	for (unsigned int i = 0; i < 10; i++)
	{
		array_insert_ordered(arr, 3 * i + 1);
	}
	for (unsigned int i = 0; i < 10; i++)
	{
		array_insert_ordered(arr, 3 * i + 2);
	}
	array_display(arr);
	return 0;
}