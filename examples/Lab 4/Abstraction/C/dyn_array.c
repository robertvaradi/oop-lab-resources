#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "dyn_array.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

dyn_array_t* dyn_array_new(unsigned int capacity, float resize_factor)
{
	dyn_array_t* arr = (dyn_array_t*)calloc(1, sizeof(dyn_array_t));
	if (arr == NULL)
	{
		printf("calloc: Failed to allocate memory for the dyn_array_t data structure: %s\n", strerror(errno));
	}
	else
	{
		arr->wrapped = st_array_new(capacity);
		if (arr->wrapped == NULL)
		{
			printf("st_array_new: Failed to create the wrapped array object!\n");
			dyn_array_destroy(&arr);
		}
		else
		{
			arr->resize_factor = resize_factor;
		}
	}
	return arr;
}

unsigned int dyn_array_size(dyn_array_t* arr)
{
	if (dyn_array_validate(arr))
	{
		return st_array_size(arr->wrapped);
	}
	return (unsigned int)-1;
}

unsigned int dyn_array_capacity(dyn_array_t* arr)
{
	if (dyn_array_validate(arr))
	{
		return st_array_capacity(arr->wrapped);
	}
	return (unsigned int)-1;
}

int dyn_array_get_element(dyn_array_t* arr, unsigned int position)
{
	if (dyn_array_validate(arr) && dyn_array_validate_position(arr, position, array_op_get))
	{
		return st_array_get_element(arr->wrapped, position);
	}
	return -1;
}

dyn_array_t* dyn_array_set_element(dyn_array_t* arr, unsigned int position, int element)
{
	if (dyn_array_validate(arr) && dyn_array_validate_position(arr, position, array_op_set))
	{
		arr->wrapped = st_array_set_element(arr->wrapped, position, element);
	}
	return arr;
}

dyn_array_t* dyn_array_insert(dyn_array_t* arr, unsigned int position, int element)
{
	if (dyn_array_validate(arr) && dyn_array_validate_position(arr, position, array_op_insert))
	{
		unsigned int size = st_array_size(arr->wrapped);
		unsigned int capacity = st_array_capacity(arr->wrapped);
		if (size == capacity)
		{
			unsigned int new_capacity = (unsigned int)(capacity * arr->resize_factor);
			int* tmp = (int*)realloc(arr->wrapped->elements, new_capacity * sizeof(int));
			if (tmp == NULL)
			{
				printf("realloc: Failed to re-allocate memory for the buffer of elements: %s!\n", strerror(errno));
			}
			else
			{
				arr->wrapped->elements = tmp;
				arr->wrapped->capacity = new_capacity;
			}
		}
		arr->wrapped = st_array_insert(arr->wrapped, position, element);
	}
	return arr;
}

dyn_array_t* dyn_array_insert_first(dyn_array_t* arr, int element)
{
	return dyn_array_insert(arr, 0, element);
}

dyn_array_t* dyn_array_insert_last(dyn_array_t* arr, int element)
{
	return dyn_array_insert(arr, dyn_array_size(arr), element);
}

int dyn_array_remove(dyn_array_t* arr, unsigned int position)
{
	int ret = -1;
	if (dyn_array_validate(arr) && dyn_array_validate_position(arr, position, array_op_remove))
	{
		ret = st_array_remove(arr->wrapped, position);
	}
	return ret;
}

int dyn_array_remove_first(dyn_array_t* arr)
{
	return dyn_array_remove(arr, 0);
}

int dyn_array_remove_last(dyn_array_t* arr)
{
	return dyn_array_remove(arr, dyn_array_size(arr) - 1);
}

bool dyn_array_is_empty(dyn_array_t* arr)
{
	return dyn_array_size(arr) == 0;
}

void dyn_array_destroy(dyn_array_t** arr)
{
	if (arr != NULL && *arr != NULL)
	{
		st_array_destroy(&(*arr)->wrapped);
		memset(*arr, 0, sizeof(dyn_array_t));
		free(*arr);
		*arr = NULL;
	}
}

void dyn_array_display(dyn_array_t* arr)
{
	if (dyn_array_validate(arr))
	{
		st_array_display(arr->wrapped);
	}
}

bool dyn_array_validate(dyn_array_t* arr)
{
	assert(arr != NULL);
	assert(st_array_validate(arr->wrapped));
	unsigned int capacity = st_array_capacity(arr->wrapped);
	unsigned int new_capacity = (int)(capacity * arr->resize_factor);
	assert(new_capacity > capacity);
	return true;
}

bool dyn_array_validate_position(dyn_array_t* arr, unsigned int position, array_op_t op)
{
	assert(st_array_validate_position(arr->wrapped, position, op));
	return true;
}

int dyn_array_search(dyn_array_t* arr, int value)
{
	if (dyn_array_validate(arr))
	{
		return st_array_search(arr->wrapped, value);
	}
	return -1;
}

bool dyn_array_element_exists(dyn_array_t* arr, int value)
{
	return dyn_array_search(arr, value) != -1;
}

float dyn_array_resize_factor(dyn_array_t* arr)
{
	if (dyn_array_validate(arr))
	{
		return arr->resize_factor;
	}
	return 0.0f;
}

void test_dyn_array()
{
	unsigned int initial_capacity = 20;
	unsigned int iteration_count = 10;
	unsigned int expected_capacity = initial_capacity;
	float resize_factor = 1.5f;
	dyn_array_t* arr = dyn_array_new(initial_capacity, resize_factor);
	assert(dyn_array_capacity(arr) == initial_capacity);
	assert(dyn_array_size(arr) == 0);
	assert(dyn_array_is_empty(arr));
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (dyn_array_size(arr) == dyn_array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		dyn_array_insert(arr, i, 3 * i);
		assert(dyn_array_capacity(arr) == expected_capacity);
		assert(dyn_array_size(arr) == i + 1);
		assert(dyn_array_get_element(arr, i) == 3 * i);
		assert(!dyn_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (dyn_array_size(arr) == dyn_array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		dyn_array_insert_first(arr, 3 * i + 1);
		assert(dyn_array_capacity(arr) == expected_capacity);
		assert(dyn_array_size(arr) == i + 1 + iteration_count);
		assert(dyn_array_get_element(arr, 0) == 3 * i + 1);
		assert(!dyn_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (dyn_array_size(arr) == dyn_array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		dyn_array_insert_last(arr, 3 * i + 2);
		assert(dyn_array_capacity(arr) == expected_capacity);
		assert(dyn_array_size(arr) == i + 1 + 2 * iteration_count);
		assert(dyn_array_get_element(arr, dyn_array_size(arr) - 1) == 3 * i + 2);
		assert(!dyn_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < 3 * iteration_count - 1; i++)
	{
		if (i % 3 == 0)
		{
			assert(dyn_array_search(arr, i) == (i / 3) + iteration_count);
		}
		else if (i % 3 == 1)
		{
			assert(dyn_array_search(arr, i) == iteration_count - ((i - 1) / 3) - 1);
		}
		else if (i % 3 == 2)
		{
			assert(dyn_array_search(arr, i) == ((i - 2) / 3) + 2 * iteration_count);
		}
		assert(dyn_array_element_exists(arr, i));
	}
	unsigned int size = dyn_array_size(arr);
	assert(dyn_array_search(arr, 3 * iteration_count + 2) == -1);
	assert(!dyn_array_element_exists(arr, 3 * iteration_count + 2));
	dyn_array_set_element(arr, iteration_count, 100);
	assert(dyn_array_get_element(arr, iteration_count) == 100);
	assert(dyn_array_search(arr, 100) == iteration_count);
	assert(dyn_array_element_exists(arr, 100));
	assert(size == dyn_array_size(arr));
	for (int i = iteration_count - 1; i >= 0; i--)
	{
		assert(dyn_array_remove_last(arr) == 3 * i + 2);
		assert(dyn_array_size(arr) == size - iteration_count + i);
		assert(dyn_array_capacity(arr) == expected_capacity);
	}
	size = dyn_array_size(arr);
	for (int i = 0; i < iteration_count; i++)
	{
		assert(dyn_array_remove_first(arr) == 3 * (iteration_count - i - 1) + 1);
		assert(dyn_array_size(arr) == size - i - 1);
		assert(dyn_array_capacity(arr) == expected_capacity);
	}
	size = dyn_array_size(arr);
	for (int i = 0; i < iteration_count; i++)
	{
		if (i == iteration_count - 1)
		{
			assert(dyn_array_remove(arr, iteration_count - i - 1) == 100);
		}
		else
		{
			assert(dyn_array_remove(arr, iteration_count - i - 1) == 3 * (iteration_count - i - 1));
		}
		assert(dyn_array_size(arr) == size - i - 1);
		assert(dyn_array_capacity(arr) == expected_capacity);
	}
	assert(dyn_array_is_empty(arr));
	dyn_array_destroy(&arr);
	assert(arr == NULL);
}

void dyn_array_display_info(dyn_array_t* arr)
{
	printf("Capacity = %d\nSize = %d\nResize factor = %.6f\n", dyn_array_capacity(arr), dyn_array_size(arr), dyn_array_resize_factor(arr));
	dyn_array_display(arr);
}

bool dyn_array_is_full(dyn_array_t* arr)
{
	return false;
}
