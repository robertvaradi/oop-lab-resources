#pragma once

#include <stdbool.h>
#include <stdlib.h>

typedef struct
{
	unsigned int capacity;
	unsigned int size;
	int* elements;
} st_array_t;

typedef enum
{
	array_op_get,
	array_op_set,
	array_op_insert,
	array_op_remove,
	array_op_invalid
} array_op_t;

st_array_t* st_array_new(unsigned int capacity);
unsigned int st_array_size(st_array_t* arr);
unsigned int st_array_capacity(st_array_t* arr);
bool st_array_is_empty(st_array_t* arr);
bool st_array_is_full(st_array_t* arr);
int st_array_get_element(st_array_t* arr, unsigned int position);
st_array_t* st_array_set_element(st_array_t* arr, unsigned int position, int element);
st_array_t* st_array_insert(st_array_t* arr, unsigned int position, int element);
st_array_t* st_array_insert_first(st_array_t* arr, int element);
st_array_t* st_array_insert_last(st_array_t* arr, int element);
int st_array_remove(st_array_t* arr, unsigned int position);
int st_array_remove_first(st_array_t* arr);
int st_array_remove_last(st_array_t* arr);
void st_array_destroy(st_array_t** arr);
void st_array_display(st_array_t* arr);
bool st_array_validate(st_array_t* arr);
bool st_array_validate_position(st_array_t* arr, unsigned int position, array_op_t op);
int st_array_search(st_array_t* arr, int value);
bool st_array_element_exists(st_array_t* arr, int value);
void test_st_array();
void st_array_display_info(st_array_t* arr);