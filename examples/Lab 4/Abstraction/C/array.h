#pragma once

#include "srt_dyn_array.h"

typedef enum
{
	array_type_static,
	array_type_dynamic,
	array_type_sorted,
	array_type_invalid
} array_type_t;

typedef struct
{
	array_type_t type;
	union
	{
		st_array_t* static_array;
		dyn_array_t* dynamic_array;
		srt_dyn_array_t* sorted_array;
	} array_data;
} array_t;

array_t* array_make_static(unsigned int capacity);
array_t* array_make_dynamic(unsigned int capacity, float resize_factor);
array_t* array_make_sorted(unsigned int capacity, float resize_factor, sorting_mode_t mode);
unsigned int array_size(array_t* arr);
unsigned int array_capacity(array_t* arr);
array_type_t array_type(array_t* arr);
float array_resize_factor(array_t* arr);
sorting_mode_t array_sorting_mode(array_t* arr);
bool array_is_empty(array_t* arr);
bool array_is_full(array_t* arr);
array_t* array_insert(array_t* arr, unsigned int position, int element);
array_t* array_insert_first(array_t* arr, int element);
array_t* array_insert_last(array_t* arr, int element);
array_t* array_insert_ordered(array_t* arr, int element);
int array_remove(array_t* arr, unsigned int position);
int array_remove_first(array_t* arr);
int array_remove_last(array_t* arr);
void array_display(array_t* arr);
void array_destroy(array_t** arr);
int array_search(array_t* arr, int element);
bool array_element_exists(array_t* arr, int element);
int array_get_element(array_t* arr, unsigned int position);
array_t* array_set_element(array_t* arr, unsigned int position, int element);
bool array_validate(array_t* arr);
bool array_validate_position(array_t* arr, unsigned int position, array_op_t op);
void test_array(array_type_t type);
void array_display_info(array_t* arr);