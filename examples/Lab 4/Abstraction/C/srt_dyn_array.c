#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "srt_dyn_array.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

static unsigned int _srt_dyn_array_size_no_validation(srt_dyn_array_t* arr);

srt_dyn_array_t* srt_dyn_array_new(unsigned int capacity, float resize_factor, sorting_mode_t mode)
{
	srt_dyn_array_t* arr = (srt_dyn_array_t*)calloc(1, sizeof(srt_dyn_array_t));
	if (arr == NULL)
	{
		printf("calloc: Failed to allocate memory for the srt_dyn_array_t data structure: %s!\n", strerror(errno));
	}
	else
	{
		arr->wrapped = dyn_array_new(capacity, resize_factor);
		if (arr->wrapped == NULL)
		{
			printf("dyn_array_new: Failed to create the wrapped array object!\n");
			srt_dyn_array_destroy(&arr);
		}
		else
		{
			arr->mode = mode;
		}
	}
	return arr;
}

unsigned int srt_dyn_array_size(srt_dyn_array_t* arr)
{
	if (srt_dyn_array_validate(arr))
	{
		return _srt_dyn_array_size_no_validation(arr);
	}
	return (unsigned int)-1;
}

unsigned int srt_dyn_array_capacity(srt_dyn_array_t* arr)
{
	if (srt_dyn_array_validate(arr))
	{
		return dyn_array_capacity(arr->wrapped);
	}
	return (unsigned int)-1;
}

float srt_dyn_array_resize_factor(srt_dyn_array_t* arr)
{
	if (srt_dyn_array_validate(arr))
	{
		return dyn_array_resize_factor(arr->wrapped);
	}
	return 0.0f;
}

sorting_mode_t srt_dyn_array_sorting_mode(srt_dyn_array_t* arr)
{
	if (srt_dyn_array_validate(arr))
	{
		return arr->mode;
	}
	return sorting_mode_invalid;
}

int srt_dyn_array_get_element(srt_dyn_array_t* arr, unsigned int position)
{
	if (srt_dyn_array_validate(arr) && srt_dyn_array_validate_position(arr, position, array_op_get))
	{
		return dyn_array_get_element(arr->wrapped, position);
	}
	return -1;
}

srt_dyn_array_t* srt_dyn_array_insert_ordered(srt_dyn_array_t* arr, int element)
{
	if (srt_dyn_array_validate(arr))
	{
		int* elements = arr->wrapped->wrapped->elements;
		unsigned int size = srt_dyn_array_size(arr);
		unsigned int position;
		if (size == 0)
		{
			position = 0;
		}
		else if ((element < elements[0] && arr->mode == sorting_mode_asc) 
			|| (element > elements[0] && arr->mode == sorting_mode_desc))
		{
			position = 0;
		}
		else if ((element > elements[size - 1] && arr->mode == sorting_mode_asc) 
			|| (element < elements[size - 1] && arr->mode == sorting_mode_desc))
		{
			position = size;
		}
		else
		{
			unsigned int low = 0;
			unsigned int high = size - 1;
			unsigned int middle = (low + high) / 2;
			bool found = false;
			while (low <= high && !found)
			{
				if ((elements[middle] > element && arr->mode == sorting_mode_asc)
					|| (elements[middle] < element && arr->mode == sorting_mode_desc))
				{
					high = middle - 1;
					
				}
				else if ((elements[middle] < element && arr->mode == sorting_mode_asc) 
					|| (elements[middle] > element && arr->mode == sorting_mode_desc))
				{
					low = middle + 1;
				}
				else if (elements[middle] == element)
				{
					position = middle;
					found = true;
				}
				middle = (low + high) / 2;
			}
			if (!found)
			{
				if ((elements[middle] > element && arr->mode == sorting_mode_asc) 
					|| (elements[middle] < element && arr->mode == sorting_mode_desc))
				{
					position = middle - 1;
				}
				else if ((elements[middle] < element && arr->mode == sorting_mode_asc) 
					|| (elements[middle] > element && arr->mode == sorting_mode_desc))
				{
					position = middle + 1;
				}
			}
		}
		arr->wrapped = dyn_array_insert(arr->wrapped, position, element);
	}
	return arr;
}

int srt_dyn_array_remove(srt_dyn_array_t* arr, unsigned int position)
{
	int result;
	result = -1;
	if (srt_dyn_array_validate(arr) && srt_dyn_array_validate_position(arr, position, array_op_remove))
	{
		result = dyn_array_remove(arr->wrapped, position);
	}
	return result;
}

int srt_dyn_array_remove_first(srt_dyn_array_t* arr)
{
	return srt_dyn_array_remove(arr, 0);
}

int srt_dyn_array_remove_last(srt_dyn_array_t* arr)
{
	return srt_dyn_array_remove(arr, srt_dyn_array_size(arr) - 1);
}

bool srt_dyn_array_is_empty(srt_dyn_array_t* arr)
{
	return srt_dyn_array_size(arr) == 0;
}

void srt_dyn_array_destroy(srt_dyn_array_t** arr)
{
	if (arr != NULL && *arr != NULL)
	{
		dyn_array_destroy(&(*arr)->wrapped);
		memset(*arr, 0, sizeof(srt_dyn_array_t));
		free(*arr);
		*arr = NULL;
	}
}

void srt_dyn_array_display(srt_dyn_array_t* arr)
{
	if (srt_dyn_array_validate(arr))
	{
		dyn_array_display(arr->wrapped);
	}
}

static unsigned int _srt_dyn_array_size_no_validation(srt_dyn_array_t* arr)
{
	return dyn_array_size(arr->wrapped);
}

bool srt_dyn_array_validate(srt_dyn_array_t* arr)
{
	bool valid;
	assert(arr != NULL);
	assert(dyn_array_validate(arr->wrapped));
	assert(arr->mode == sorting_mode_asc || arr->mode == sorting_mode_desc);
	valid = true;
	int* elements = arr->wrapped->wrapped->elements;
	unsigned int size = _srt_dyn_array_size_no_validation(arr);
	if (size != 0)
	{
		for (unsigned int i = 0; i < size - 1 && valid; i++)
		{
			if ((arr->mode == sorting_mode_asc && elements[i] > elements[i + 1])
				|| (arr->mode == sorting_mode_desc && elements[i] < elements[i + 1]))
			{
				valid = false;
			}
		}
	}
	assert(valid);
	return true;
}
bool srt_dyn_array_validate_position(srt_dyn_array_t* arr, unsigned int position, array_op_t op)
{
	assert(dyn_array_validate_position(arr->wrapped, position, op));
	return true;
}

int srt_dyn_array_search(srt_dyn_array_t* arr, int value)
{
	if (srt_dyn_array_validate(arr))
	{
		int* elements = arr->wrapped->wrapped->elements;
		unsigned int size = srt_dyn_array_size(arr);
		unsigned int low = 0;
		unsigned int high = size - 1;
		unsigned int middle = (low + high) / 2;
		while (low <= high)
		{
			if (elements[middle] == value)
			{
				return middle;
			}
			else if ((elements[middle] < value && arr->mode == sorting_mode_asc) 
				|| (elements[middle] > value && arr->mode == sorting_mode_desc))
			{
				low = middle + 1;
			}
			else if ((elements[middle] > value && arr->mode == sorting_mode_asc) 
				|| (elements[middle] < value && arr->mode == sorting_mode_desc))
			{
				high = middle - 1;
			}
			middle = (low + high) / 2;
		}
	}
	return -1;
}

bool srt_dyn_array_element_exists(srt_dyn_array_t* arr, int value)
{
	return srt_dyn_array_search(arr, value) != -1;
}

bool srt_dyn_array_is_full(srt_dyn_array_t* arr)
{
	return false;
}

void srt_dyn_array_display_info(srt_dyn_array_t* arr)
{
	printf("Capacity = %d\nSize = %d\nResize factor = %.6f\nSorting mode = \"%s\"\n", srt_dyn_array_capacity(arr), srt_dyn_array_size(arr), srt_dyn_array_resize_factor(arr), (arr->mode == sorting_mode_asc ? "ascending" : (arr->mode == sorting_mode_desc ? "descending" : "invalid")));
	srt_dyn_array_display(arr);
}

void test_srt_dyn_array()
{
	unsigned int initial_capacity = 20;
	unsigned int iteration_count = 10;
	unsigned int expected_capacity = initial_capacity;
	float resize_factor = 1.5f;
	srt_dyn_array_t* arr = srt_dyn_array_new(initial_capacity, resize_factor, sorting_mode_asc);
	assert(srt_dyn_array_capacity(arr) == initial_capacity);
	assert(srt_dyn_array_size(arr) == 0);
	assert(srt_dyn_array_is_empty(arr));
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (srt_dyn_array_size(arr) == srt_dyn_array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		srt_dyn_array_insert_ordered(arr, 3 * i);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
		assert(srt_dyn_array_size(arr) == i + 1);
		assert(srt_dyn_array_get_element(arr, i) == 3 * i);
		assert(!srt_dyn_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (srt_dyn_array_size(arr) == srt_dyn_array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		srt_dyn_array_insert_ordered(arr, 3 * i + 1);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
		assert(srt_dyn_array_size(arr) == i + 1 + iteration_count);
		assert(srt_dyn_array_get_element(arr, 2 * i + 1) == 3 * i + 1);
		assert(!srt_dyn_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (srt_dyn_array_size(arr) == srt_dyn_array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		srt_dyn_array_insert_ordered(arr, 3 * i + 2);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
		assert(srt_dyn_array_size(arr) == i + 1 + 2 * iteration_count);
		assert(srt_dyn_array_get_element(arr, 3 * i + 2) == 3 * i + 2);
		assert(!srt_dyn_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < 3 * iteration_count; i++)
	{
		assert(srt_dyn_array_search(arr, i) == i);
		assert(srt_dyn_array_element_exists(arr, i));
	}
	unsigned int size = srt_dyn_array_size(arr);
	assert(srt_dyn_array_search(arr, 3 * iteration_count + 2) == -1);
	assert(!srt_dyn_array_element_exists(arr, 3 * iteration_count + 2));
	assert(size == srt_dyn_array_size(arr));
	int max_value = 3 * iteration_count - 1;
	for (int i = iteration_count - 1; i >= 0; i--)
	{
		assert(srt_dyn_array_remove_last(arr) == max_value--);
		assert(srt_dyn_array_size(arr) == size - iteration_count + i);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
	}
	size = srt_dyn_array_size(arr);
	int min_value = 0;
	for (int i = 0; i < iteration_count; i++)
	{
		assert(srt_dyn_array_remove_first(arr) == min_value++);
		assert(srt_dyn_array_size(arr) == size - i - 1);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
	}
	size = srt_dyn_array_size(arr);
	max_value = 2 * iteration_count - 1;
	for (int i = 0; i < iteration_count; i++)
	{
		assert(srt_dyn_array_remove(arr, iteration_count - i - 1) == max_value--);
		assert(srt_dyn_array_size(arr) == size - i - 1);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
	}
	assert(srt_dyn_array_is_empty(arr));
	srt_dyn_array_destroy(&arr);
	assert(arr == NULL);
}