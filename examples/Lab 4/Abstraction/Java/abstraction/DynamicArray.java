package main.oop.principles.abstraction;

import main.oop.principles.abstraction.interfaces.SupportsResize;

public class DynamicArray extends ArrayWithInsert implements SupportsResize {
	private float resizeFactor;

	public DynamicArray(int capacity, float resizeFactor) {
		super(capacity);
		this.resizeFactor = resizeFactor;
	}
	
	@Override
	public void resize(int newCapacity) {
		if (isFull()) {
			getBase().setCapacity(newCapacity);
		}
	}

	@Override
	public float getResizeFactor() {
		return resizeFactor;
	}

	@Override
	protected boolean canInsert() {
		return true;
	}
	
	@Override
	public int newCapacity() {
		return (int) (getCapacity() * resizeFactor);
	}

	@Override
	public void insert(int position, int value) {
		if (validate()) {
			if (isFull()) {
				resize(newCapacity());
			}
			super.insert(position, value);
		}	
	}
}
