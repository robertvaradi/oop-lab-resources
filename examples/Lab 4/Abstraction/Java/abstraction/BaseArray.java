package main.oop.principles.abstraction;

import main.oop.principles.abstraction.interfaces.Displayable;
import main.oop.principles.abstraction.interfaces.ReadableSequence;
import main.oop.principles.abstraction.interfaces.SupportsArrayFlags;
import main.oop.principles.abstraction.interfaces.SupportsCapacity;
import main.oop.principles.abstraction.interfaces.SupportsSimpleInsert;
import main.oop.principles.abstraction.interfaces.SupportsSimpleRemove;
import main.oop.principles.abstraction.interfaces.SupportsSize;
import main.oop.principles.abstraction.interfaces.SupportsValidate;
import main.oop.principles.abstraction.interfaces.WritableSequence;

public class BaseArray implements SupportsCapacity, SupportsSize, SupportsValidate, WritableSequence, ReadableSequence,
		SupportsArrayFlags, SupportsSimpleInsert, SupportsSimpleRemove, Displayable {
	private int size;

	private int capacity;

	private int[] elements;

	public BaseArray(int capacity) {
		this.capacity = capacity;
		this.elements = new int[capacity];
		this.size = 0;
	}

	@Override
	public int getCapacity() {
		return capacity;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public void setCapacity(int capacity) {
		int[] elements = new int[capacity];
		for (int i = 0; i < this.capacity; i++) {
			elements[i] = this.elements[i];
		}
		this.capacity = capacity;
	}

	@Override
	public boolean validatePosition(int position, ArrayOp op) {
		return (ArrayOp.INSERT.equals(op) && position >= 0 && position <= size)
				|| (!ArrayOp.INSERT.equals(op) && position >= 0 && position < size);
	}

	@Override
	public int get(int position) {
		return elements[position];
	}

	@Override
	public void set(int position, int value) {
		elements[position] = value;
	}

	@Override
	public boolean validate() {
		return capacity > 0 && size < capacity && elements != null;
	}

	@Override
	public void insert(int position, int value) {
		for (int i = size - 1; i >= position; i--) {
			elements[i + 1] = elements[i];
		}
		elements[position] = value;
		size++;
	}

	@Override
	public int remove(int position) {
		int ret = elements[position];
		for (int i = position; i < size - 1; i++) {
			elements[i] = elements[i + 1];
		}
		size--;
		return ret;
	}

	@Override
	public boolean isFull() {
		return size == capacity;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void display() {
		for (int i = 0; i < size; i++) {
			System.out.format("%d%s", elements[i], (i == size - 1) ? "\n" : ", ");
		}
	}
}
