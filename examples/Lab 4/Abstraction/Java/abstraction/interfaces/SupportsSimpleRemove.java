package main.oop.principles.abstraction.interfaces;

public interface SupportsSimpleRemove {
	public int remove(int position);
}
