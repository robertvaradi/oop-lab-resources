package main.oop.principles.abstraction.interfaces;

public interface SupportsResize {
	public void resize(int newCapacity);
	public float getResizeFactor();
	public int newCapacity();
}
