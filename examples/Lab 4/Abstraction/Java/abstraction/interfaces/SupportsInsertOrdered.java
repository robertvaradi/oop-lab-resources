package main.oop.principles.abstraction.interfaces;

import main.oop.principles.abstraction.SortingMode;

public interface SupportsInsertOrdered {
	public void insert(int value);
	public SortingMode getSortingMode();
}
