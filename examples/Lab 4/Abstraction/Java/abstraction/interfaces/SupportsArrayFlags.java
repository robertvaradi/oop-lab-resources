package main.oop.principles.abstraction.interfaces;

public interface SupportsArrayFlags {
	public boolean isEmpty();
	public boolean isFull();
}
