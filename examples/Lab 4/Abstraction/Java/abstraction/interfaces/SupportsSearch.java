package main.oop.principles.abstraction.interfaces;

public interface SupportsSearch {
	public int search(int value);
	public boolean exists(int value);
}
