package main.oop.principles.abstraction.interfaces;

public interface ReadableSequence {
	public int get(int position);
}
