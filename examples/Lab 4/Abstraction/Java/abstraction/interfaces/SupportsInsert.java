package main.oop.principles.abstraction.interfaces;

public interface SupportsInsert extends SupportsSimpleInsert {
	public void insertFirst(int value);
	public void insertLast(int value);
}
