package main.oop.principles.abstraction.interfaces;

import main.oop.principles.abstraction.ArrayOp;

public interface SupportsValidate {
	public boolean validate();
	public boolean validatePosition(int position, ArrayOp op);
}
