package main.oop.principles.abstraction.interfaces;

public interface SupportsCapacity {
	public int getCapacity();
	public void setCapacity(int capacity);
}
