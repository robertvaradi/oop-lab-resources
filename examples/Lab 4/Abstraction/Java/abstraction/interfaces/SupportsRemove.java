package main.oop.principles.abstraction.interfaces;

public interface SupportsRemove extends SupportsSimpleRemove {
	public int removeFirst();
	public int removeLast();
}
