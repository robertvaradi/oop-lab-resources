package main.oop.principles.abstraction.interfaces;

public interface SupportsSize {
	public int getSize();
}
