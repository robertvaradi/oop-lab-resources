package main.oop.principles.abstraction.interfaces;

public interface SupportsSimpleInsert {
	public void insert(int position, int value);
}
