package main.oop.principles.abstraction.interfaces;

public interface Displayable {
	public void display();
}
