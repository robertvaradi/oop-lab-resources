package main.oop.principles.abstraction.interfaces;

public interface WritableSequence {
	public void set(int position, int value);
}
