package main.oop.principles.abstraction;

import main.oop.principles.abstraction.interfaces.SupportsInsert;
import main.oop.principles.abstraction.interfaces.WritableSequence;

public abstract class ArrayWithInsert extends Array implements SupportsInsert, WritableSequence {

	public ArrayWithInsert(int capacity) {
		super(capacity);
	}

	@Override
	public void insertFirst(int value) {
		insert(0, value);
	}

	@Override
	public void insertLast(int value) {
		insert(getSize() - 1, value);
	}

	@Override
	public void insert(int position, int value) {
		if (validate() && validatePosition(position, ArrayOp.INSERT) && canInsert()) {
			getBase().insert(position, value);
		}
	}
	
	protected abstract boolean canInsert();

	@Override
	public void set(int position, int value) {
		if (validate() && validatePosition(position, ArrayOp.SET)) {
			getBase().set(position, value);
		}
	}
}
