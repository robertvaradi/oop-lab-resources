package main.oop.principles.abstraction;

import main.oop.principles.abstraction.interfaces.Displayable;
import main.oop.principles.abstraction.interfaces.ReadableSequence;
import main.oop.principles.abstraction.interfaces.SupportsArrayFlags;
import main.oop.principles.abstraction.interfaces.SupportsCapacity;
import main.oop.principles.abstraction.interfaces.SupportsRemove;
import main.oop.principles.abstraction.interfaces.SupportsSearch;
import main.oop.principles.abstraction.interfaces.SupportsSize;
import main.oop.principles.abstraction.interfaces.SupportsValidate;

public abstract class Array implements SupportsRemove, SupportsCapacity, SupportsSize, SupportsSearch,
		SupportsValidate, SupportsArrayFlags, ReadableSequence, Displayable {

	private BaseArray base;

	public Array(int capacity) {
		base = new BaseArray(capacity);
	}
	
	@Override
	public int remove(int position) {
		if (validate() && validatePosition(position, ArrayOp.REMOVE)) {
			return remove(position);
		}
		return -1;
	}

	@Override
	public int get(int position) {
		if (validate() && validatePosition(position, ArrayOp.GET)) {
			return base.get(position);
		}
		return -1;
	}

	@Override
	public boolean isEmpty() {
		return base.isEmpty();
	}

	@Override
	public boolean isFull() {
		return base.isFull();
	}

	@Override
	public boolean validate() {
		return base.validate();
	}

	@Override
	public boolean validatePosition(int position, ArrayOp op) {
		return base.validatePosition(position, op);
	}

	@Override
	public int search(int value) {
		if (validate()) {
			int size = base.getSize();
			for (int i = 0; i < size; i++) {
				if (base.get(i) == value) {
					return i;
				}
			}
		}
		return -1;
	}

	@Override
	public boolean exists(int value) {
		return search(value) != -1;
	}

	@Override
	public int getSize() {
		if (validate()) {
			return base.getSize();
		}
		return -1;
	}

	@Override
	public int getCapacity() {
		if (validate()) {
			return base.getCapacity();
		}
		return -1;
	}

	@Override
	public void setCapacity(int capacity) {
		base.setCapacity(capacity);
	}

	@Override
	public int removeFirst() {
		return remove(0);
	}

	@Override
	public int removeLast() {
		return remove(base.getSize() - 1);
	}
	
	protected BaseArray getBase() {
		return base;
	}
	
	public void display() {
		base.display();
	}
}
