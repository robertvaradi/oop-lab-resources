package main.oop.principles.abstraction;

import main.oop.principles.abstraction.interfaces.SupportsCapacity;
import main.oop.principles.abstraction.interfaces.SupportsInsertOrdered;
import main.oop.principles.abstraction.interfaces.SupportsResize;
import main.oop.principles.abstraction.interfaces.SupportsSearch;
import main.oop.principles.abstraction.interfaces.SupportsSize;

public class SortedDynamicArray extends Array
		implements SupportsResize, SupportsSearch, SupportsInsertOrdered, SupportsSize, SupportsCapacity {
	private float resizeFactor;

	private SortingMode sortingMode;

	public SortedDynamicArray(int capacity, float resizeFactor, SortingMode sortingMode) {
		super(capacity);
		this.resizeFactor = resizeFactor;
		this.sortingMode = sortingMode;
	}

	private int find(int value) {
		int low = 0;
		int high = getSize() - 1;
		int middle = (low + high) / 2;
		while (low <= high) {
			int middleElement = get(middle);
			if ((middleElement < value && SortingMode.ASC.equals(sortingMode))
					|| (middleElement > value && SortingMode.DESC.equals(sortingMode))) {
				low = middle + 1;
			} else if ((middleElement > value && SortingMode.ASC.equals(sortingMode))
					|| (middleElement < value && SortingMode.DESC.equals(sortingMode))) {
				high = middle - 1;
			} else {
				return middle;
			}
			middle = (low + high) / 2;
		}
		return middle;
	}

	private int getSortedInsertPosition(int value) {
		int size = getSize();
		if (size == 0) {
			return 0;
		} else {
			int first = get(0);
			int last = get(size - 1);
			if ((value < first && SortingMode.ASC.equals(sortingMode))
					|| (value > first && SortingMode.DESC.equals(sortingMode))) {
				return 0;
			} else if ((value > last && SortingMode.DESC.equals(sortingMode))
					|| (value < last && SortingMode.DESC.equals(sortingMode))) {
				return size - 1;
			} else {
				int position = find(value);
				int element = get(position);
				if ((element < value && SortingMode.ASC.equals(sortingMode))
						|| (element > value && SortingMode.DESC.equals(sortingMode))) {
					return position + 1;
				} else if ((element > value && SortingMode.ASC.equals(sortingMode))
						|| (element < value && SortingMode.DESC.equals(sortingMode))) {
					return position > 0 ? position - 1 : 0;
				} else {
					return position;
				}
			}
		}
	}

	@Override
	public void insert(int value) {
		if (validate()) {
			if (isFull()) {
				resize(newCapacity());
			}
			getBase().insert(getSortedInsertPosition(value), value);
		}
	}

	@Override
	public void resize(int newCapacity) {
		if (isFull()) {
			getBase().setCapacity(newCapacity);
		}
	}

	@Override
	public float getResizeFactor() {
		if (validate()) {
			return resizeFactor;
		}
		return 0.0f;
	}

	@Override
	public SortingMode getSortingMode() {
		if (validate()) {
			return sortingMode;
		}
		return null;
	}

	@Override
	public int newCapacity() {
		return (int) (getCapacity() * resizeFactor);
	}

	@Override
	public int search(int value) {
		if (validate()) {
			int position = find(value);
			int element = get(position);
			if (element == value) {
				return position;
			}
		}
		return -1;
	}

	private boolean isSorted() {
		int size = getBase().getSize();
		if (size < 2) {
			return true;
		}
		int first = getBase().get(0);
		int second = getBase().get(1);
		for (int i = 0; i < size - 2; i++) {
			if ((first < second && SortingMode.DESC.equals(sortingMode))
					|| (first > second && SortingMode.ASC.equals(sortingMode))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean validate() {
		return super.validate() && isSorted();
	}
}
