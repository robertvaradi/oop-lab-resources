package main.oop.principles.abstraction;

public enum SortingMode {
	ASC,
	DESC
}
