package main.oop.principles.abstraction;

public enum ArrayOp {
	SET,
	GET,
	INSERT,
	REMOVE
}
