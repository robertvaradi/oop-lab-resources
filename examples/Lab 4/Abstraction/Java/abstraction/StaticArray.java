package main.oop.principles.abstraction;

public class StaticArray extends ArrayWithInsert {

	public StaticArray(int capacity) {
		super(capacity);
	}

	@Override
	protected boolean canInsert() {
		return !isFull();
	}
}
