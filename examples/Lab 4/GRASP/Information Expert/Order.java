package lab4.grasp.informationExpert;

public class Order {
	private OrderItem[] orderItems;
	
	private Integer actualCount;
	
	public Order(int itemCount) {
		orderItems = new OrderItem[itemCount];
		actualCount = 0;
	}
	
	public void add(Product product, Integer quantity) {
		orderItems[actualCount++] = new OrderItem(product, quantity);
	}
	
	public Double getPrice() {
		Double price = 0.0;
		for (int i = 0; i < actualCount; i++) {
			price += orderItems[i].getPrice();
		}
		return price;
	}
}
