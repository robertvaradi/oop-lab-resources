package lab4.grasp.informationExpert;

public class OrderItem {
	private Product product;
	private Integer quantity;
	
	public OrderItem(Product product, Integer quantity) {
		this.product = product;
		this.quantity = quantity;
	}
	
	public Double getPrice() {
		return quantity * product.getPrice();
	}
}
