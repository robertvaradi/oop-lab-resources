package lab4.grasp.informationExpert;

public class Program {
	public static void main(String[] args) {
		Order order = new Order(10);
		order.add(new Product("A", 6.0), 10);
		order.add(new Product("B", 10.0), 15);
		order.add(new Product("C", 6.7), 27);
		System.out.println(order.getPrice());
	}
}
