package lab4.grasp.controller;

public interface Connection {
	public void connect();
	public void disconnect();
	public void execute(String queryString);
}
