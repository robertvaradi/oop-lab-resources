package lab4.grasp.controller;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	
	private static final String COMMAND_INSERT = "insert";
	
	private static final String COMMAND_UPDATE = "update";
	
	private static final String COMMAND_DELETE = "delete";
	
	private static final String COMMAND_LIST = "list";
	
	private static final String COMMAND_EXIT = "exit";
	
	private static void insert(Repository repository, String params[]) {
		User user = new User();
		for (int i = 0; i < params.length; i++) {
			if (params[i].contains("=")) {
				String[] toks = params[i].split("=");
				String key = toks[0];
				String value = String.join("=", Arrays.copyOfRange(toks, 1, toks.length));
				if ("username".equals(key)) {
					user.setUsername(value);
				} else if ("password".equals(key)) {
					user.setPassword(value);
				} else if ("phoneNumber".equals(key)) {
					user.setPhoneNumber(value);
				} else if ("fullName".equals(key)) {
					user.setFullName(value);
				} else if ("email".equals(key)) {
					user.setEmail(value);
				}
			}
		}
		repository.save(user);
	}
	
	private static void update(Repository repository, String[] params) {
		User user = new User();
		for (int i = 1; i < params.length; i++) {
			String param = params[i];
			if (param.contains("=")) {
				String[] toks = param.split("=");
				String key = toks[0];
				String value = String.join("=", Arrays.copyOfRange(params, 1, params.length));
				if ("username".equals(key)) {
					user.setUsername(value);
				} else if ("password".equals(key)) {
					user.setPassword(value);
				} else if ("phoneNumber".equals(key)) {
					user.setPhoneNumber(value);
				} else if ("fullName".equals(key)) {
					user.setFullName(value);
				} else if ("email".equals(key)) {
					user.setEmail(value);
				} else if ("id".equals(value)) {
					user.setId(Long.parseLong(value));
				}
			}
		}
		repository.update(user);
	}
	
	private static void delete(Repository repository, String[] params) {
		User user = (User) repository.findById(Long.parseLong(params[0]));
		repository.delete(user);
	}
	
	private static void list(Repository repository, String[] params) {
		Entity[] entities = repository.findAll();
		System.out.format("|id    |      username|       password|       fullName|  phoneNumber|          email|\n");
		for (Entity entity : entities) {
			User user = (User) entity;
			System.out.format("|%6d|%14s|%15s|%15s|%13s|%15s|\n", user.getId(), user.getUsername(), user.getPassword(), user.getFullName(), user.getPhoneNumber(), user.getEmail());
		}
	}
	
	public static void main(String[] args) {
		Repository repository = new UserRepository(new MySqlConnection());
		Scanner scanner = new Scanner(System.in);
		String commandString = null;
		do {
			System.out.print(">> ");
			commandString = scanner.nextLine();
			String[] tokens = commandString.split(" ");
			if (tokens.length >= 1) {
				String[] params = new String[tokens.length - 1];
				for (int i = 0; i < tokens.length - 1; i++) {
					params[i] = tokens[i + 1];
				}
				String commandName = tokens[0];
				if (COMMAND_INSERT.equals(commandName)) {
					insert(repository, params);
				} else if (COMMAND_UPDATE.equals(commandName)) {
					update(repository, params);
				} else if (COMMAND_DELETE.equals(commandName)) {
					delete(repository, params);
				} else if (COMMAND_LIST.equals(commandName)) {
					list(repository, params);
				} else if (!COMMAND_EXIT.equals(commandName)) {
					System.err.println("Invalid command \"" + commandString + "\"!");
				}
			}
		} while (!COMMAND_EXIT.equals(commandString));
		scanner.close();
	}
}
