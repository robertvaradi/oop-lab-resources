package lab4.grasp.controller;

public interface Entity {
	public Long getId();
	public void setId(Long id);
}
