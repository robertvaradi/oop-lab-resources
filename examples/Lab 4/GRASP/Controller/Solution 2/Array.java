package lab4.grasp.controller;

public class Array {
	private Integer size;
	private Integer capacity;
	private Double resizeFactor;
	private Entity[] entities;
	
	public Array(Integer capacity, Double resizeFactor) {
		this.capacity = capacity;
		this.entities = new Entity[capacity];
		this.size = 0;
		this.resizeFactor = resizeFactor;
	}
	
	public void insert(Entity entity) {
		if (size == capacity) {
			Integer newCapacity = (int) (capacity * resizeFactor);
			Entity[] newEntities = new Entity[newCapacity];
			for (int i = 0; i < capacity; i++) {
				newEntities[i] = entities[i];
			}
			capacity = newCapacity;
			entities = newEntities;
		}
		entities[size++] = entity;
	}
	
	private int findPositionById(Long id) {
		for (int i = 0; i < size; i++) {
			if (entities[i].getId().equals(id)) {
				return i;
			}
		}
		return -1;
	}
	
	public void remove(Entity toRemove) {
		int position = findPositionById(toRemove.getId());
		for (int i = position; i < size - 1; i++) {
			entities[i] = entities[i + 1];
		}
		size--;
	}
	
	public Entity findById(Long id) {
		int position = findPositionById(id);
		return position != -1 ? entities[position] : null;
	}
	
	public void update(Entity toUpdate) {
		int position = findPositionById(toUpdate.getId());
		if (position != -1) {
			entities[position] = toUpdate;
		}
	}
	
	public Entity[] entities() {
		Entity[] toReturn = new Entity[size];
		for (int i = 0; i < size; i++) {
			toReturn[i] = entities[i];
		}
		return toReturn;
	}
}
