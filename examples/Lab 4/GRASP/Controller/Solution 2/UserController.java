package lab4.grasp.controller;

import java.util.Arrays;

public class UserController {
	private static final String COMMAND_INSERT = "insert";
	
	private static final String COMMAND_UPDATE = "update";
	
	private static final String COMMAND_DELETE = "delete";
	
	private static final String COMMAND_LIST = "list";
	
	private static final String COMMAND_EXIT = "exit";
	
	private Repository repository;
	
	public UserController(Repository repository) {
		this.repository = repository;
	}
	
	private void setProperty(User user, String key, String value) {
		if ("username".equals(key)) {
			user.setUsername(value);
		} else if ("password".equals(key)) {
			user.setPassword(value);
		} else if ("fullName".equals(key)) {
			user.setFullName(value);
		} else if ("phoneNumber".equals(key)) {
			user.setPhoneNumber(value);
		} else if ("email".equals(key)) {
			user.setEmail(value);
		} else if ("id".equals(key)) {
			user.setId(Long.parseLong(value));
		}
	}
	
	
	private void handleInsert(String[] params) {
		User user = new User();
		for (String param : params) {
			if (param.contains("=")) {
				String[] tokens = param.split("=");
				String key = tokens[0];
				String value = String.join("=", Arrays.copyOfRange(tokens, 1, tokens.length));
				setProperty(user, key, value);
			}
		}
		repository.save(user);
	}
	
	private void handleUpdate(String[] params) {
		User userToUpdate = (User) repository.findById(Long.parseLong(params[0]));
		for (String param : params) {
			if (param.contains("=")) {
				String[] tokens = param.split("=");
				String key = tokens[0];
				String value = String.join("=", Arrays.copyOfRange(tokens, 1, tokens.length));
				setProperty(userToUpdate, key, value);
			}
		}
		repository.update(userToUpdate);
	}
	
	private void handleDelete(String[] params) {
		User userToDelete = (User) repository.findById(Long.parseLong(params[0]));
		repository.delete(userToDelete);
	}
	
	private void lineDelimiter() {
		System.out.println("-".repeat(105));
	}
	
	private void displayHeader() {
		lineDelimiter();
		System.out.format("|%3s|%20s|%20s|%20s|%15s|%20s|\n", "id", "username", "password", "fullName", "phoneNumber", "email");
	}
	
	private void display(User user) {
		System.out.format("|%3d|%20s|%20s|%20s|%15s|%20s|\n", user.getId(), user.getUsername(), user.getPassword(), user.getFullName(), user.getPhoneNumber(), user.getEmail());
	}
	
	
	private void handleList(String[] params) {
		Entity[] entities = repository.findAll();
		displayHeader();
		for (Entity entity : entities) {
			lineDelimiter();
			display((User) entity);
		}
		lineDelimiter();
	}
	
	public boolean handle(String commandString) {
		String[] tokens = commandString.split(" ");
		System.out.println("Handling command \"" + commandString + "\"...");
		if (tokens.length >= 1) {
			String commandName = tokens[0];
			String[] params = Arrays.copyOfRange(tokens, 1, tokens.length);
			if (COMMAND_INSERT.equals(commandName)) {
				handleInsert(params);
			} else if (COMMAND_UPDATE.equals(commandName)) {
				handleUpdate(params);
			} else if (COMMAND_DELETE.equals(commandName)) {
				handleDelete(params);
			} else if (COMMAND_LIST.equals(commandName)) {
				handleList(params);
			} else if (COMMAND_EXIT.equals(commandName)) {
				return false;
			} else {
				System.err.println("Invalid command \"" + commandString + "\"!");
			}
		}
		return true;
	}
}
