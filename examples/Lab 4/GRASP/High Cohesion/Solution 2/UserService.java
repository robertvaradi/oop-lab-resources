package lab4.grasp.highCohesion;

public class UserService {

	private Repository userRepository;
	private UserValidator validator;
	
	public UserService(UserRepository repository) {
		this.userRepository = repository;
	}
	
	public void create(User user) {
		if (validator.validateUser(user, Operation.CREATE)) {
			userRepository.save(user);
		}
	}
	
	public void update(User user) {
		if (validator.validateUser(user, Operation.UPDATE)) {
			userRepository.update(user);
		}
	}
	
	public void delete(User user) {
		if (validator.validateUser(user, Operation.DELETE)) {
			userRepository.delete(user);
		}
	}
	
	public User findUser(Long id) {
		return (User) userRepository.findById(id);
	}
	
	public User[] findAll() {
		Entity[] all = userRepository.findAll();
		User[] allUsers = new User[all.length];
		for (int i = 0; i < all.length; i++) {
			allUsers[i] = (User) all[i];
		}
		return allUsers;
	}
	
	public User findUser(String username) {
		return ((UserRepository) userRepository).findByUsername(username);
	}
}
