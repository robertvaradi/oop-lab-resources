package lab4.grasp.highCohesion;

import java.util.Arrays;

public class UserController {
	private static final String COMMAND_INSERT = "insert";

	private static final String COMMAND_UPDATE = "update";

	private static final String COMMAND_DELETE = "delete";

	private static final String COMMAND_LIST = "list";

	private static final String COMMAND_EXIT = "exit";

	private static final String COMMAND_SEND = "send";

	private UserService service;
	private EmailSender emailSender;

	public UserController(UserService service) {
		this.service = service;
		emailSender = new EmailSender();
	}

	private void setProperty(User user, String key, String value) {
		if ("username".equals(key)) {
			user.setUsername(value);
		} else if ("password".equals(key)) {
			user.setPassword(value);
		} else if ("fullName".equals(key)) {
			user.setFullName(value);
		} else if ("phoneNumber".equals(key)) {
			user.setPhoneNumber(value);
		} else if ("email".equals(key)) {
			user.setEmail(value);
		} else if ("id".equals(key)) {
			user.setId(Long.parseLong(value));
		}
	}

	private void handleInsert(String[] params) {
		User user = new User();
		for (String param : params) {
			if (param.contains("=")) {
				String[] tokens = param.split("=");
				String key = tokens[0];
				String value = String.join("=", Arrays.copyOfRange(tokens, 1, tokens.length));
				setProperty(user, key, value);
			}
		}
		service.create(user);
	}

	private void handleUpdate(String[] params) {
		User userToUpdate = (User) service.findUser(Long.parseLong(params[0]));
		for (String param : params) {
			if (param.contains("=")) {
				String[] tokens = param.split("=");
				String key = tokens[0];
				String value = String.join("=", Arrays.copyOfRange(tokens, 1, tokens.length));
				setProperty(userToUpdate, key, value);
			}
		}
		service.update(userToUpdate);
	}

	private void handleDelete(String[] params) {
		User userToDelete = (User) service.findUser(Long.parseLong(params[0]));
		service.delete(userToDelete);
	}

	private void lineDelimiter() {
		System.out.println("-".repeat(105));
	}

	private void displayHeader() {
		lineDelimiter();
		System.out.format("|%3s|%20s|%20s|%20s|%15s|%20s|\n", "id", "username", "password", "fullName", "phoneNumber",
				"email");
	}

	private void display(User user) {
		System.out.format("|%3d|%20s|%20s|%20s|%15s|%20s|\n", user.getId(), user.getUsername(), user.getPassword(),
				user.getFullName(), user.getPhoneNumber(), user.getEmail());
	}

	private void handleList(String[] params) {
		Entity[] entities = service.findAll();
		displayHeader();
		for (Entity entity : entities) {
			lineDelimiter();
			display((User) entity);
		}
		lineDelimiter();
	}

	private void handleSend(String[] params) {
		String senderUsername = null;
		String receiverUsername = null;
		String subject = null;
		String body = null;
		for (String param : params) {
			if (param.contains("=")) {
				String[] tokens = param.split("=");
				String key = tokens[0];
				String value = String.join("=", Arrays.copyOfRange(tokens, 1, tokens.length));
				if ("sender".equals(key)) {
					senderUsername = value;
				} else if ("receiver".equals(key)) {
					receiverUsername = value;
				} else if ("subject".equals(key)) {
					subject = value;
				} else if ("body".equals(key)) {
					body = value;
				}
			}
		}
		User sender = service.findUser(senderUsername);
		User receiver = service.findUser(receiverUsername);
		emailSender.sendEmail(subject, body, sender, receiver);
	}

	public boolean handle(String commandString) {
		String[] tokens = commandString.split(" ");
		System.out.println("Handling command \"" + commandString + "\"...");
		if (tokens.length >= 1) {
			String commandName = tokens[0];
			String[] params = Arrays.copyOfRange(tokens, 1, tokens.length);
			if (COMMAND_INSERT.equals(commandName)) {
				handleInsert(params);
			} else if (COMMAND_UPDATE.equals(commandName)) {
				handleUpdate(params);
			} else if (COMMAND_DELETE.equals(commandName)) {
				handleDelete(params);
			} else if (COMMAND_LIST.equals(commandName)) {
				handleList(params);
			} else if (COMMAND_SEND.equals(commandName)) {
				handleSend(params);
			} else if (COMMAND_EXIT.equals(commandName)) {
				return false;
			} else {
				System.err.println("Invalid command \"" + commandString + "\"!");
			}
		}
		return true;
	}
}
