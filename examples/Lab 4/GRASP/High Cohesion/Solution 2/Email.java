package lab4.grasp.highCohesion;

public record Email(String subject, String body, User sender, User receiver) {

}
