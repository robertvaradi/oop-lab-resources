package lab4.grasp.highCohesion;

public class UserValidator {
	@SuppressWarnings("unused")
	private UserService service;
	
	public UserValidator(UserService service) {
		this.service = service;
	}
	
	public boolean validateUser(User user, Operation operation) {
		// User validation code, which could use the service instance as well
		return true;
	}
}
