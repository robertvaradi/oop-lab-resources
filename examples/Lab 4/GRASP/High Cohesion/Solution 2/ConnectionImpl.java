package lab4.grasp.highCohesion;

public class ConnectionImpl implements Connection {
	private String dbEngine;
	
	public ConnectionImpl(String dbEngine) {
		this.dbEngine = dbEngine;
	}

	private void prompt(String message) {
		System.out.println(message);
	}
	
	public void connect() {
		prompt(String.format("Connecting to the %s Database ...", dbEngine));
	}
	
	public void disconnect() {
		prompt(String.format("Disconnecting from the %s Database ...", dbEngine));
	}
	
	public void execute(String queryString) {
		prompt(String.format("Executing query \"%s\" on the %s Databsase ...", queryString, dbEngine));
	}
}
