package lab4.grasp.highCohesion;

public class EmailSender {
	
	private void send(Email email) {
		// Do send the actual email
	}
	
	public void sendEmail(String subject, String body, User sender, User receiver) {
		Email email = new Email(subject, body, sender, receiver);
		send(email);
	}
}
