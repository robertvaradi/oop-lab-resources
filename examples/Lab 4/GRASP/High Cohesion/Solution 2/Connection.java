package lab4.grasp.highCohesion;

public interface Connection {
	public void connect();
	public void disconnect();
	public void execute(String queryString);
}
