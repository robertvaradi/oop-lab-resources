package lab4.grasp.highCohesion;

public interface Entity {
	public Long getId();
	public void setId(Long id);
}
