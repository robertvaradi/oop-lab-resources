package lab4.grasp.highCohesion;

public enum Operation {
	CREATE,
	READ,
	UPDATE,
	DELETE
}
