package lab4.grasp.highCohesion;

public class UserService {
	private static enum Operation {
		CREATE,
		READ,
		UPDATE,
		DELETE
	}
	
	private Repository userRepository;
	
	public UserService(UserRepository repository) {
		this.userRepository = repository;
	}
	
	public void create(User user) {
		if (validateUser(user, Operation.CREATE)) {
			userRepository.save(user);
		}
	}
	
	public void update(User user) {
		if (validateUser(user, Operation.UPDATE)) {
			userRepository.update(user);
		}
	}
	
	public void delete(User user) {
		if (validateUser(user, Operation.DELETE)) {
			userRepository.delete(user);
		}
	}
	
	public User findUser(Long id) {
		return (User) userRepository.findById(id);
	}
	
	public User[] findAll() {
		Entity[] all = userRepository.findAll();
		User[] allUsers = new User[all.length];
		for (int i = 0; i < all.length; i++) {
			allUsers[i] = (User) all[i];
		}
		return allUsers;
	}
	
	public boolean validateUser(User user, Operation operation) {
		// User validation code
		return true;
	}
	
	private void send(Email email) {
		// sending the email
	}
	
	public void sendEmail(User sender, User receiver, String subject, String body) {
		Email email = new Email(subject, body, sender, receiver);
		send(email);
	}
}
