package lab4.grasp.highCohesion;

import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		UserController controller = new UserController(new UserService(new UserRepository(new MySqlConnection())));
		Scanner scanner = new Scanner(System.in);
		String commandString = null;
		do {
			System.out.print(">> ");
			commandString = scanner.nextLine();
		} while (controller.handle(commandString));
		scanner.close();
	}
}
