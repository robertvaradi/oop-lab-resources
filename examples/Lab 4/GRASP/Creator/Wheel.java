package lab4.grasp.creator;

public class Wheel implements CarComponent {
	private Tyre tyre;
	
	private Tyre createTyre() {
		return new Tyre();
	}
	
	public Wheel() {
		this.tyre = createTyre();
	}
	
	public Tyre getTyre() {
		return tyre;
	}
}
