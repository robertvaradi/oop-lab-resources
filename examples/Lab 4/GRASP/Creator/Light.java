package lab4.grasp.creator;

public class Light implements CarComponent {
	public static enum LightPosition {
		FRONT_LEFT,
		FRONT_RIGHT,
		BACK_LEFT,
		BACK_RIGHT
	}
	
	public static enum LightColor {
		WHITE,
		RED
	}
	
	private LightPosition position;
	
	private LightColor color;
	
	public Light(LightPosition position) {
		this.position = position;
		if (LightPosition.FRONT_LEFT.equals(position) || LightPosition.FRONT_RIGHT.equals(position)) {
			this.color = LightColor.WHITE;
		} else if (LightPosition.BACK_LEFT.equals(position) || LightPosition.BACK_RIGHT.equals(position)) {
			this.color = LightColor.RED;
		}
	}

	public LightPosition getPosition() {
		return position;
	}

	public LightColor getColor() {
		return color;
	}
}
