package lab4.grasp.creator;

public class Door implements CarComponent {
	public static enum DoorPosition {
		FRONT_LEFT,
		FRONT_RIGHT,
		BACK_LEFT,
		BACK_RIGHT
	}
	
	private DoorPosition position;
	
	public Door(DoorPosition position) {
		this.position = position;
	}
	
	public DoorPosition getDoorPosition() {
		return position;
	}
}
