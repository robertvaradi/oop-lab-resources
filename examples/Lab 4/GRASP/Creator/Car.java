package lab4.grasp.creator;

import lab4.grasp.creator.Door.DoorPosition;

public class Car {
	private CarComponent[] components;
	
	private Wheel createWheel() {
		return new Wheel();
	}
	
	private Engine createEngine() {
		return new Engine();
	}
	
	private Door createDoor(Door.DoorPosition position) {
		return new Door(position);
	}
	
	private Light createLight(Light.LightPosition position) {
		return new Light(position);
	}
	
	public Car() {
		components = new CarComponent[] {
				createEngine(),
				createWheel(),
				createWheel(),
				createWheel(),
				createWheel(),
				createDoor(DoorPosition.FRONT_LEFT),
				createDoor(DoorPosition.FRONT_RIGHT),
				createDoor(DoorPosition.BACK_LEFT),
				createDoor(DoorPosition.BACK_RIGHT),
				createLight(Light.LightPosition.FRONT_LEFT),
				createLight(Light.LightPosition.FRONT_RIGHT),
				createLight(Light.LightPosition.BACK_LEFT),
				createLight(Light.LightPosition.BACK_RIGHT)
		};
	}
	
	public CarComponent[] getComponents() {
		return components;
	}
}
