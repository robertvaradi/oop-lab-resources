package main.oop.principles.encapsulation;

public class StaticArray {
	private int capacity;

	private int size;

	private int[] elements;

	public static enum ArrayOp {
		GET, SET, INSERT, REMOVE
	}

	public StaticArray(int capacity) {
		this.capacity = capacity;
		this.size = 0;
		this.elements = new int[capacity];
	}

	public int getCapacity() {
		return capacity;
	}

	public int getSize() {
		return size;
	}

	public boolean validate() {
		return elements != null && capacity > 0 && size <= capacity;
	}

	public boolean validatePosition(int position, ArrayOp op) {
		return position >= 0
				&& ((ArrayOp.INSERT.equals(op) && position <= size) || (!ArrayOp.INSERT.equals(op) && position < size));
	}

	public int getElement(int position) {
		if (validate() && validatePosition(position, ArrayOp.GET)) {
			return elements[position];
		}
		return -1;
	}

	public void setElement(int position, int value) {
		if (validate() && validatePosition(position, ArrayOp.SET)) {
			elements[position] = value;
		}
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public boolean isFull() {
		return size == capacity;
	}

	public void insert(int position, int value) {
		if (validate() && validatePosition(position, ArrayOp.INSERT)) {
			if (!isFull()) {
				for (int i = size; i > position; i--) {
					elements[i] = elements[i - 1];
				}
				elements[position] = value;
				size++;
			}
		}
	}

	public void insertFirst(int value) {
		insert(0, value);
	}

	public void insertLast(int value) {
		insert(size, value);
	}

	public int remove(int position) {
		int ret = -1;
		if (validate() && validatePosition(position, ArrayOp.REMOVE)) {
			if (!isEmpty()) {
				ret = elements[position];
				for (int i = position; i < size - 1; i++) {
					elements[i] = elements[i + 1];
				}
				elements[size - 1] = 0;
				size--;
			}
		}
		return ret;
	}

	public int removeFirst() {
		return remove(0);
	}

	public int removeLast() {
		return remove(size - 1);
	}

	public void display() {
		if (validate()) {
			for (int i = 0; i < size; i++) {
				System.out.format("%d%s", elements[i], (i == size - 1) ? "\n" : ", ");
			}
		}
	}

	public int search(int value) {
		if (validate()) {
			for (int i = 0; i < size; i++) {
				if (elements[i] == value) {
					return i;
				}
			}
		}
		return -1;
	}

	public boolean exists(int value) {
		return search(value) != -1;
	}
}