#include "st_array.h"

int main()
{
    test_st_array();
    st_array_t* arr = st_array_new(20);
    for (unsigned int i = 0; i < 10; i++)
    {
        st_array_insert(arr, i, i + 1);
    }
    // Uncomment these three lines in order to see what happens because of the lack of encapsulation
    // arr->elements[7] = 100;
    // arr->capacity = 50;
    // arr->size = 20;
    st_array_display(arr);
    st_array_destroy(&arr);
}