#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "srt_dyn_array.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

static unsigned int _srt_dyn_array_size_ex(srt_dyn_array_t* arr, bool validate);

static int _srt_dyn_array_find(srt_dyn_array_t* arr, int value);

static int _srt_dyn_array_get_sorted_insert_position(srt_dyn_array_t* arr, int value);

static bool _srt_dyn_array_is_sorted(srt_dyn_array_t* arr);

static int _srt_dyn_array_get_element_ex(srt_dyn_array_t* arr, int position, bool validate);

srt_dyn_array_t* srt_dyn_array_new(unsigned int capacity, float resize_factor, sorting_mode_t mode)
{
	srt_dyn_array_t* arr = (srt_dyn_array_t*)calloc(1, sizeof(srt_dyn_array_t));
	if (arr == NULL)
	{
		printf("calloc: Failed to allocate memory for the srt_dyn_array_t data structure: %s!\n", strerror(errno));
	}
	else
	{
		arr->wrapped = dyn_array_new(capacity, resize_factor);
		if (arr->wrapped == NULL)
		{
			printf("dyn_array_new: Failed to create the wrapped array object!\n");
			srt_dyn_array_destroy(&arr);
		}
		else
		{
			arr->mode = mode;
		}
	}
	return arr;
}

unsigned int srt_dyn_array_size(srt_dyn_array_t* arr)
{
	return _srt_dyn_array_size_ex(arr, true);
}

unsigned int srt_dyn_array_capacity(srt_dyn_array_t* arr)
{
	if (srt_dyn_array_validate(arr))
	{
		return dyn_array_capacity(arr->wrapped);
	}
	return (unsigned int)-1;
}

float srt_dyn_array_resize_factor(srt_dyn_array_t* arr)
{
	if (srt_dyn_array_validate(arr))
	{
		return dyn_array_resize_factor(arr->wrapped);
	}
	return 0.0f;
}

sorting_mode_t srt_dyn_array_sorting_mode(srt_dyn_array_t* arr)
{
	if (srt_dyn_array_validate(arr))
	{
		return arr->mode;
	}
	return sorting_mode_invalid;
}

int srt_dyn_array_get_element(srt_dyn_array_t* arr, unsigned int position)
{
	return _srt_dyn_array_get_element_ex(arr, position, true);
}

srt_dyn_array_t* srt_dyn_array_insert_ordered(srt_dyn_array_t* arr, int element)
{
	if (srt_dyn_array_validate(arr))
	{
		int position = _srt_dyn_array_get_sorted_insert_position(arr, element);
		dyn_array_insert(arr->wrapped, position, element);
	}
	return arr;
}

int srt_dyn_array_remove(srt_dyn_array_t* arr, unsigned int position)
{
	int result;
	result = -1;
	if (srt_dyn_array_validate(arr) && srt_dyn_array_validate_position(arr, position, array_op_remove))
	{
		result = dyn_array_remove(arr->wrapped, position);
	}
	return result;
}

int srt_dyn_array_remove_first(srt_dyn_array_t* arr)
{
	return srt_dyn_array_remove(arr, 0);
}

int srt_dyn_array_remove_last(srt_dyn_array_t* arr)
{
	return srt_dyn_array_remove(arr, srt_dyn_array_size(arr) - 1);
}

bool srt_dyn_array_is_empty(srt_dyn_array_t* arr)
{
	return srt_dyn_array_size(arr) == 0;
}

void srt_dyn_array_destroy(srt_dyn_array_t** arr)
{
	if (arr != NULL && *arr != NULL)
	{
		dyn_array_destroy(&(*arr)->wrapped);
		memset(*arr, 0, sizeof(srt_dyn_array_t));
		free(*arr);
		*arr = NULL;
	}
}

void srt_dyn_array_display(srt_dyn_array_t* arr)
{
	if (srt_dyn_array_validate(arr))
	{
		dyn_array_display(arr->wrapped);
	}
}

static unsigned int _srt_dyn_array_size_ex(srt_dyn_array_t* arr, bool validate)
{
	if (!validate || srt_dyn_array_validate(arr))
	{
		return dyn_array_size(arr->wrapped);
	}
	return (unsigned int)-1;
}

bool srt_dyn_array_validate(srt_dyn_array_t* arr)
{
	assert(dyn_array_validate(arr->wrapped));
	assert(_srt_dyn_array_is_sorted(arr));
	return true;
}

bool srt_dyn_array_validate_position(srt_dyn_array_t* arr, unsigned int position, array_op_t op)
{
	assert(dyn_array_validate_position(arr->wrapped, position, op));
	return true;
}

int srt_dyn_array_search(srt_dyn_array_t* arr, int value)
{
	if (srt_dyn_array_validate(arr))
	{
		int initial_position = _srt_dyn_array_find(arr, value);
		int element = srt_dyn_array_get_element(arr, initial_position);
		return element == value ? initial_position : -1;
	}
	return -1;
}

static int _srt_dyn_array_get_sorted_insert_position(srt_dyn_array_t* arr, int value)
{
	int size = srt_dyn_array_size(arr);
	int first = srt_dyn_array_get_element(arr, 0);
	int last = srt_dyn_array_get_element(arr, size > 0 ? size - 1 : 0);
	if (size == 0 || (value < first && arr->mode == sorting_mode_asc) 
		|| (value > first && arr->mode == sorting_mode_desc))
	{
		return 0;
	}
	else if ((value > last && arr->mode == sorting_mode_asc) 
		|| (value < last && arr->mode == sorting_mode_desc))
	{
		return size;
	}
	else
	{
		int initial_position = _srt_dyn_array_find(arr, value);
		int element = srt_dyn_array_get_element(arr, initial_position);
		if (element == value)
		{
			return initial_position + 1;
		}
		else if ((element < value && arr->mode == sorting_mode_asc) 
			|| (element > value && arr->mode == sorting_mode_desc))
		{
			return initial_position + 1;
		}
		else if ((element > value && arr->mode == sorting_mode_asc) 
			|| (element < value && arr->mode == sorting_mode_desc))
		{
			return initial_position > 0 ? initial_position - 1 : 0;
		}
	}
	return -1;
}

bool srt_dyn_array_element_exists(srt_dyn_array_t* arr, int value)
{
	return srt_dyn_array_search(arr, value) != -1;
}

bool srt_dyn_array_is_full(srt_dyn_array_t* arr)
{
	return false;
}

void srt_dyn_array_display_info(srt_dyn_array_t* arr)
{
	printf("Capacity = %d\nSize = %d\nResize factor = %.6f\nSorting mode = \"%s\"\n", srt_dyn_array_capacity(arr), srt_dyn_array_size(arr), srt_dyn_array_resize_factor(arr), (arr->mode == sorting_mode_asc ? "ascending" : (arr->mode == sorting_mode_desc ? "descending" : "invalid")));
	srt_dyn_array_display(arr);
}

static int _srt_dyn_array_find(srt_dyn_array_t* arr, int value)
{
	int low = 0;
	int high = srt_dyn_array_size(arr) - 1;
	int middle = (low + high) / 2;
	while (low <= high)
	{
		int middle_element = srt_dyn_array_get_element(arr, middle);
		if ((middle_element < value && arr->mode == sorting_mode_asc) 
			|| (middle_element > value && arr->mode == sorting_mode_desc))
		{
			low = middle + 1;
		}
		else if ((middle_element > value && arr->mode == sorting_mode_asc) 
			|| (middle_element < value && arr->mode == sorting_mode_desc))
		{
			high = middle - 1;
		}
		else
		{
			return middle;
		}
		middle = (low + high) / 2;
	}
	return middle;
}

static int _srt_dyn_array_get_element_ex(srt_dyn_array_t* arr, int position, bool validate)
{
	if (!validate || (srt_dyn_array_validate(arr) && srt_dyn_array_validate_position(arr, position, array_op_get)))
	{
		return dyn_array_get_element(arr->wrapped, position);
	}
	return -1;
}

static bool _srt_dyn_array_is_sorted(srt_dyn_array_t* arr)
{
	int size = _srt_dyn_array_size_ex(arr, false);
	if (size < 2)
	{
		return true;
	}
	int first = _srt_dyn_array_get_element_ex(arr, 0, false);
	int second = _srt_dyn_array_get_element_ex(arr, 1, false);
	for (int i = 0; i < size - 1; i++)
	{
		if ((first > second && arr->mode == sorting_mode_asc) 
			|| (first < second && arr->mode == sorting_mode_desc))
		{
			return false;
		}
		first = second;
		if (i < size - 2)
		{
			second = _srt_dyn_array_get_element_ex(arr, i + 2, false);
		}
	}
	return true;
}

void test_srt_dyn_array()
{
	unsigned int initial_capacity = 20;
	unsigned int iteration_count = 10;
	unsigned int expected_capacity = initial_capacity;
	float resize_factor = 1.5f;
	srt_dyn_array_t* arr = srt_dyn_array_new(initial_capacity, resize_factor, sorting_mode_asc);
	assert(srt_dyn_array_capacity(arr) == initial_capacity);
	assert(srt_dyn_array_size(arr) == 0);
	assert(srt_dyn_array_is_empty(arr));
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (srt_dyn_array_size(arr) == srt_dyn_array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		srt_dyn_array_insert_ordered(arr, 3 * i);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
		assert(srt_dyn_array_size(arr) == i + 1);
		assert(srt_dyn_array_get_element(arr, i) == 3 * i);
		assert(!srt_dyn_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (srt_dyn_array_size(arr) == srt_dyn_array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		srt_dyn_array_insert_ordered(arr, 3 * i + 1);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
		assert(srt_dyn_array_size(arr) == i + 1 + iteration_count);
		assert(srt_dyn_array_get_element(arr, 2 * i + 1) == 3 * i + 1);
		assert(!srt_dyn_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		if (srt_dyn_array_size(arr) == srt_dyn_array_capacity(arr))
		{
			expected_capacity = (int)(expected_capacity * resize_factor);
		}
		srt_dyn_array_insert_ordered(arr, 3 * i + 2);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
		assert(srt_dyn_array_size(arr) == i + 1 + 2 * iteration_count);
		assert(srt_dyn_array_get_element(arr, 3 * i + 2) == 3 * i + 2);
		assert(!srt_dyn_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < 3 * iteration_count; i++)
	{
		assert(srt_dyn_array_search(arr, i) == i);
		assert(srt_dyn_array_element_exists(arr, i));
	}
	unsigned int size = srt_dyn_array_size(arr);
	assert(srt_dyn_array_search(arr, 3 * iteration_count + 2) == -1);
	assert(!srt_dyn_array_element_exists(arr, 3 * iteration_count + 2));
	assert(size == srt_dyn_array_size(arr));
	int max_value = 3 * iteration_count - 1;
	for (int i = iteration_count - 1; i >= 0; i--)
	{
		assert(srt_dyn_array_remove_last(arr) == max_value--);
		assert(srt_dyn_array_size(arr) == size - iteration_count + i);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
	}
	size = srt_dyn_array_size(arr);
	int min_value = 0;
	for (int i = 0; i < iteration_count; i++)
	{
		assert(srt_dyn_array_remove_first(arr) == min_value++);
		assert(srt_dyn_array_size(arr) == size - i - 1);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
	}
	size = srt_dyn_array_size(arr);
	max_value = 2 * iteration_count - 1;
	for (int i = 0; i < iteration_count; i++)
	{
		assert(srt_dyn_array_remove(arr, iteration_count - i - 1) == max_value--);
		assert(srt_dyn_array_size(arr) == size - i - 1);
		assert(srt_dyn_array_capacity(arr) == expected_capacity);
	}
	assert(srt_dyn_array_is_empty(arr));
	srt_dyn_array_destroy(&arr);
	assert(arr == NULL);
}