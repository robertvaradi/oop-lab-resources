#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "st_array.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

st_array_t* st_array_new(unsigned int capacity)
{
	assert(capacity != 0);
	st_array_t* arr = (st_array_t*)calloc(1, sizeof(st_array_t));
	if (arr == NULL)
	{
		printf("calloc: Failed to allocate memory for the st_array_t data structure: %s!\n", strerror(errno));
	}
	else
	{
		arr->elements = (int*)calloc(capacity, sizeof(int));
		if (arr->elements == NULL)
		{
			printf("calloc: Failed to allocate memory for the buffer containing the elements of the array: %s!\n", strerror(errno));
			st_array_destroy(&arr);
		}
		else
		{
			arr->capacity = capacity;
			arr->size = 0;
		}
	}
	return arr;
}

unsigned int st_array_size(st_array_t* arr)
{
	if (st_array_validate(arr))
	{
		return arr->size;
	}
	return (unsigned int)-1;
}

unsigned int st_array_capacity(st_array_t* arr)
{
	if (st_array_validate(arr))
	{
		return arr->capacity;
	}
	return (unsigned int)-1;
}

int st_array_get_element(st_array_t* arr, unsigned int position)
{
	if (st_array_validate(arr) && st_array_validate_position(arr, position, array_op_get))
	{
		return arr->elements[position];
	}
	return (unsigned int)-1;
}

st_array_t* st_array_set_element(st_array_t* arr, unsigned int position, int element)
{
	if (st_array_validate(arr) && st_array_validate_position(arr, position, array_op_set))
	{
		arr->elements[position] = element;
	}
	return arr;
}

st_array_t* st_array_insert(st_array_t* arr, unsigned int position, int element)
{
	if (st_array_validate(arr) && st_array_validate_position(arr, position, array_op_insert))
	{
		if (!st_array_is_full(arr))
		{
			for (unsigned int i = arr->size; i > position; i--)
			{
				arr->elements[i] = arr->elements[i - 1];
			}
			arr->elements[position] = element;
			arr->size++;
		}
	}
	return arr;
}

st_array_t* st_array_insert_first(st_array_t* arr, int element)
{
	return st_array_insert(arr, 0, element);
}

st_array_t* st_array_insert_last(st_array_t* arr, int element)
{
	return st_array_insert(arr, st_array_size(arr), element);
}

int st_array_remove(st_array_t* arr, unsigned int position)
{
	int ret;
	ret = -1;
	if (st_array_validate(arr) && st_array_validate_position(arr, position, array_op_remove))
	{
		if (!st_array_is_empty(arr))
		{
			ret = arr->elements[position];
			for (unsigned int i = position; i < arr->size - 1; i++)
			{
				arr->elements[i] = arr->elements[i + 1];
			}
			arr->elements[arr->size - 1] = 0;
			arr->size--;
		}
	}
	return ret;
}
int st_array_remove_first(st_array_t* arr)
{
	return st_array_remove(arr, 0);
}

int st_array_remove_last(st_array_t* arr)
{
	return st_array_remove(arr, st_array_size(arr) - 1);
}

bool st_array_is_empty(st_array_t* arr)
{
	return st_array_size(arr) == 0;
}

void st_array_destroy(st_array_t** arr)
{
	if (arr != NULL && *arr != NULL)
	{
		if ((*arr)->elements != NULL)
		{
			free((*arr)->elements);
		}
		memset(*arr, 0, sizeof(st_array_t));
		*arr = NULL;
	}
}

void st_array_display(st_array_t* arr)
{
	if (st_array_validate(arr))
	{
		for (unsigned int i = 0; i < arr->size; i++)
		{
			printf("%d%s", arr->elements[i], (i == arr->size - 1) ? "\n" : ", ");
		}
	}
}

bool st_array_validate(st_array_t* arr)
{
	assert(arr != NULL);
	assert(arr->elements != NULL);
	assert(arr->capacity != 0);
	assert(arr->size <= arr->capacity);
	return true;
}

bool st_array_validate_position(st_array_t* arr, unsigned int position, array_op_t op)
{
	if (op == array_op_insert || arr->size == 0)
	{
		assert(position <= arr->size);
	}
	else
	{
		assert(position < arr->size);
	}
	return true;
}

int st_array_search(st_array_t* arr, int value)
{
	if (st_array_validate(arr))
	{
		for (unsigned int i = 0; i < arr->size; i++)
		{
			if (arr->elements[i] == value)
			{
				return i;
			}
		}
	}
	return -1;
}

bool st_array_element_exists(st_array_t* arr, int value)
{
	return st_array_search(arr, value) != -1;
}

void test_st_array()
{
	unsigned int initial_capacity = 20;
	unsigned int iteration_count = 5;
	st_array_t* arr = st_array_new(initial_capacity);
	assert(st_array_capacity(arr) == initial_capacity);
	assert(st_array_size(arr) == 0);
	assert(st_array_is_empty(arr));
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		st_array_insert(arr, i, 3 * i);
		assert(st_array_capacity(arr) == initial_capacity);
		assert(st_array_size(arr) == i + 1);
		assert(st_array_get_element(arr, i) == 3 * i);
		assert(!st_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < iteration_count; i++)
	{
		st_array_insert_first(arr, 3 * i + 1);
		assert(st_array_capacity(arr) == initial_capacity);
		assert(st_array_size(arr) == i + 1 + iteration_count);
		assert(st_array_get_element(arr, 0) == 3 * i + 1);
		assert(!st_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < 5; i++)
	{
		st_array_insert_last(arr, 3 * i + 2);
		assert(st_array_capacity(arr) == initial_capacity);
		assert(st_array_size(arr) == i + 1 + 2 * iteration_count);
		assert(st_array_get_element(arr, st_array_size(arr) - 1) == 3 * i + 2);
		assert(!st_array_is_empty(arr));
	}
	for (unsigned int i = 0; i < 3 * iteration_count; i++)
	{
		if (i % 3 == 0)
		{
			assert(st_array_search(arr, i) == (i / 3) + iteration_count);
		}
		else if (i % 3 == 1)
		{
			assert(st_array_search(arr, i) == iteration_count - ((i - 1) / 3) - 1);
		}
		else if (i % 3 == 2)
		{
			assert(st_array_search(arr, i) == ((i - 2) / 3) + 2 * iteration_count);
		}
		assert(st_array_element_exists(arr, i));
	}
	unsigned int size = st_array_size(arr);
	assert(st_array_search(arr, 3 * iteration_count + 2) == -1);
	assert(!st_array_element_exists(arr, 3 * iteration_count + 2));
	st_array_set_element(arr, iteration_count, 100);
	assert(st_array_get_element(arr, iteration_count) == 100);
	assert(st_array_search(arr, 100) == iteration_count);
	assert(st_array_element_exists(arr, 100));
	assert(size == st_array_size(arr));
	for (int i = iteration_count - 1; i >= 0; i--)
	{
		assert(st_array_remove_last(arr) == 3 * i + 2);
		assert(st_array_size(arr) == size - iteration_count + i);
		assert(st_array_capacity(arr) == initial_capacity);
	}
	size = st_array_size(arr);
	for (int i = 0; i < iteration_count; i++)
	{
		assert(st_array_remove_first(arr) == 3 * (iteration_count - i - 1) + 1);
		assert(st_array_size(arr) == size - i - 1);
		assert(st_array_capacity(arr) == initial_capacity);
	}
	size = st_array_size(arr);
	for (int i = 0; i < iteration_count; i++)
	{
		if (i == iteration_count - 1)
		{
			assert(st_array_remove(arr, iteration_count - i - 1) == 100);
		}
		else
		{
			assert(st_array_remove(arr, iteration_count - i - 1) == 3 * (iteration_count - i - 1));
		}
		assert(st_array_size(arr) == size - i - 1);
		assert(st_array_capacity(arr) == initial_capacity);
	}
	assert(st_array_is_empty(arr));
	st_array_destroy(&arr);
	assert(arr == NULL);
}

void st_array_display_info(st_array_t* arr)
{
	printf("Capacity = %d\nSize = %d\n", st_array_capacity(arr), st_array_size(arr));
	st_array_display(arr);
}

bool st_array_is_full(st_array_t* arr)
{
	return st_array_size(arr) == st_array_capacity(arr);
}
