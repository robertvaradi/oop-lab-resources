#pragma once

#include "dyn_array.h"

typedef enum
{
	sorting_mode_asc,
	sorting_mode_desc,
	sorting_mode_invalid
} sorting_mode_t;

typedef struct
{
	dyn_array_t* wrapped;
	sorting_mode_t mode;
} srt_dyn_array_t;

srt_dyn_array_t* srt_dyn_array_new(unsigned int capacity, float resize_factor, sorting_mode_t mode);
unsigned int srt_dyn_array_size(srt_dyn_array_t* arr);
unsigned int srt_dyn_array_capacity(srt_dyn_array_t* arr);
float srt_dyn_array_resize_factor(srt_dyn_array_t* arr);
sorting_mode_t srt_dyn_array_sorting_mode(srt_dyn_array_t* arr);
bool srt_dyn_array_is_empty(srt_dyn_array_t* arr);
bool srt_dyn_array_is_full(srt_dyn_array_t* arr);
int srt_dyn_array_get_element(srt_dyn_array_t* arr, unsigned int position);
srt_dyn_array_t* srt_dyn_array_insert_ordered(srt_dyn_array_t* arr, int element);
int srt_dyn_array_remove(srt_dyn_array_t* arr, unsigned int position);
int srt_dyn_array_remove_first(srt_dyn_array_t* arr);
int srt_dyn_array_remove_last(srt_dyn_array_t* arr);
void srt_dyn_array_destroy(srt_dyn_array_t** arr);
void srt_dyn_array_display(srt_dyn_array_t* arr);
bool srt_dyn_array_validate(srt_dyn_array_t* arr);
bool srt_dyn_array_validate_position(srt_dyn_array_t* arr, unsigned int position, array_op_t op);
int srt_dyn_array_search(srt_dyn_array_t* arr, int value);
bool srt_dyn_array_element_exists(srt_dyn_array_t* arr, int value);
void test_srt_dyn_array();
void srt_dyn_array_display_info(srt_dyn_array_t* arr);