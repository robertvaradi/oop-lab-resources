#include "srt_dyn_array.h"

int main()
{
    test_dyn_array();
    test_srt_dyn_array();
    dyn_array_t* darray = dyn_array_new(10, 1.5f);
    for (unsigned int i = 0; i < 15; i++)
    {
        dyn_array_insert(darray, i, i + 1);
    }
    dyn_array_display(darray);
    dyn_array_destroy(&darray);
    srt_dyn_array_t* sdarray = srt_dyn_array_new(10, 1.5f, sorting_mode_desc);
    for (unsigned int i = 0; i < i < 15; i++)
    {
        srt_dyn_array_insert_ordered(sdarray, i + 1);
    }
    srt_dyn_array_display(sdarray);
    srt_dyn_array_destroy(&sdarray);
    return 0;
}