package main.oop.principles.inheritance;

public class DynamicArray extends StaticArray {
	private float resizeFactor;
	
	public DynamicArray(int capacity, float resizeFactor) {
		super(capacity);
		this.resizeFactor = resizeFactor;
	}
	
	public float getResizeFactor() {
		return resizeFactor;
	}

	@Override
	protected boolean resizeAllowed() {
		return true;
	}

	@Override
	protected int newCapacity(int capacity) {
		return (int) (capacity * resizeFactor);
	}

	@Override
	protected int[] resize(int oldCapacity, int newCapacity, int[] elements) {
		int[] ret = new int[newCapacity];
		for (int i = 0; i < oldCapacity; i++) {
			ret[i] = elements[i];
		}
		return ret;
	}

	@Override
	public boolean validate() {
		return super.validate() && newCapacity(getCapacity()) > getCapacity();
	}
}
