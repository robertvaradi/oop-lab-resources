package main.oop.principles.inheritance;

public class SortedDynamicArray extends DynamicArray {
	private SortingMode sortingMode;

	public static enum SortingMode {
		ASC, DESC
	};

	public SortedDynamicArray(int capacity, float resizeFactor, SortingMode sortingMode) {
		super(capacity, resizeFactor);
		this.sortingMode = sortingMode;
	}

	public SortingMode getSortingMode() {
		return sortingMode;
	}

	@Override
	public void insert(int position, int value) {
		// No implementation as it does not make sense for a sorted array
	}

	@Override
	public void insertFirst(int value) {
		// No implementation as it does not make sense for a sorted array
	}

	@Override
	public void insertLast(int value) {
		// No implementation as it does not make sense for a sorted array
	}

	private int find(int value) {
		int low = 0;
		int high = getSize() - 1;
		int middle = (low + high) / 2;
		while (low <= high) {
			int middleElement = getElement(middle);
			if ((middleElement < value && SortingMode.ASC.equals(sortingMode))
					|| (middleElement > value && SortingMode.DESC.equals(sortingMode))) {
				low = middle + 1;
			} else if ((middleElement > value && SortingMode.ASC.equals(sortingMode))
					|| (middleElement < value && SortingMode.DESC.equals(sortingMode))) {
				high = middle - 1;
			} else {
				return middle;
			}
			middle = (low + high) / 2;
		}
		return middle;
	}

	private int getSortedInsertPosition(int value) {
		int size = getSize();
		int first = getElement(0);
		int last = getElement(getSize() - 1);
		if (size == 0 || (value < first && SortingMode.ASC.equals(sortingMode))
				|| (value > first && SortingMode.DESC.equals(sortingMode))) {
			return 0;
		} else if ((value > last && SortingMode.ASC.equals(sortingMode))
				|| (value < last && SortingMode.DESC.equals(sortingMode))) {
			return size;
		} else {
			int initialPosition = find(value);
			int element = getElement(initialPosition);
			if (element == value) {
				return initialPosition + 1;
			} else if ((element < value && SortingMode.ASC.equals(sortingMode))
					|| (element > value && SortingMode.DESC.equals(sortingMode))) {
				return initialPosition + 1;
			} else if ((element > value && SortingMode.ASC.equals(sortingMode))
					|| (element < value && SortingMode.DESC.equals(sortingMode))) {
				return initialPosition > 0 ? initialPosition - 1 : 0;
			}
		}
		return -1;
	}
	
	private boolean isSorted() {
		int size = getSize();
		int first = getElementEx(0, false);
		int second = getElementEx(1, false);
		for (int i = 0; i < size - 1; i++) {
			if ((first > second && SortingMode.ASC.equals(sortingMode)) || (second > first && SortingMode.DESC.equals(sortingMode))) {
				return false;
			}
			first = second;
			if (i < size - 2) {
				second = getElementEx(i + 2, false);
			}
		}
		return true;
	}
	
	

	@Override
	public boolean validate() {
		return super.validate() && isSorted();
	}

	public void insertOrdered(int value) {
		if (validate()) {
			int position = getSortedInsertPosition(value);
			super.insert(position, value);
		}
	}

	@Override
	public int search(int value) {
		int position = find(value);
		int element = getElement(position);
		return element == value ? position : -1;
	}

	@Override
	public void setElement(int position, int value) {
		// No implementation as it does not make sense for a sorted array
	}
}
