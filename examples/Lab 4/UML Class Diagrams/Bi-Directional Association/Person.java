package lab4.uml;

public class Person {
	static Integer count;

	protected String name;

	private Integer age;

	public Gender gender;
	
	private Address address;
	
	private Person mother;
	
	private Person father;
	
	private Person[] children;

	public Person(String name, Integer age, Gender gender, Address address, Person mother, Person father, Person[] children) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.address = address;
		this.mother = mother;
		this.father = father;
		this.children = children;
		for (Person child : children) {
			if (Gender.MALE.equals(gender)) {
				child.setFather(this);
			} else if (Gender.FEMALE.equals(gender)) {
				child.setMother(mother);
			}
		}
		count++;
	}

	public Integer getAge() {
		return age;
	}

	public static Integer getCount() {
		return count;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public Person[] getChildren() {
		return children;
	}
	
	public Person getMother() {
		return mother;
	}
	
	public void setMother(Person mother) {
		this.mother = mother;
	}
	
	public Person getFather() {
		return father;
	}
	
	public void setFather(Person father) {
		this.father = father;
	}
}
