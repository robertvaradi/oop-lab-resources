package lab4.uml;

public class Man extends Person {

	public Man(String name, Integer age, Address address, Woman mother, Man father,
			Person[] children) {
		super(name, age, Gender.MALE, address, mother, father, children);
	}

}
