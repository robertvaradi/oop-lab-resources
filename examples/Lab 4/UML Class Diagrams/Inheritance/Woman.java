package lab4.uml;

public class Woman extends Person {

	public Woman(String name, Integer age, Address address, Woman mother, Man father,
			Person[] children) {
		super(name, age, Gender.FEMALE, address, mother, father, children);
	}

}
