package lab4.uml;

public class Wheel extends RepairableProduct {

	private Tyre tyre;
	
	public Wheel(Tyre tyre) {
		this.tyre = tyre;
	}
	
	public Tyre getTyre() {
		return tyre;
	}
	
}
