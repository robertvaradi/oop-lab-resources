package lab4.uml;

public abstract class Person {
	static Integer count;

	protected String name;

	private Integer age;

	public Gender gender;

	private Address address;

	private Woman mother;

	private Man father;

	private Person[] children;

	private Head head;

	private Hand[] hands;

	private Leg[] legs;

	public Person(String name, Integer age, Gender gender, Address address, Woman mother, Man father,
			Person[] children) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.address = address;
		this.mother = mother;
		this.father = father;
		this.children = children;
		for (Person child : children) {
			if (Gender.MALE.equals(gender)) {
				child.setFather((Man)this);
			} else if (Gender.FEMALE.equals(gender)) {
				child.setMother((Woman)this);
			}
		}
		count++;
		this.head = new Head();
		this.hands = new Hand[] { new Hand(), new Hand() };
		this.legs = new Leg[] { new Leg(), new Leg() };
	}

	public Integer getAge() {
		return age;
	}

	public static Integer getCount() {
		return count;
	}

	public Address getAddress() {
		return address;
	}

	public Person[] getChildren() {
		return children;
	}

	public Woman getMother() {
		return mother;
	}

	public void setMother(Woman mother) {
		this.mother = mother;
	}

	public Man getFather() {
		return father;
	}

	public void setFather(Man father) {
		this.father = father;
	}

	public Head getHead() {
		return head;
	}

	public Hand[] getHands() {
		return hands;
	}

	public Leg[] getLegs() {
		return legs;
	}
	
	public void buy(Car car) {
		// Buys a car
	}
}
