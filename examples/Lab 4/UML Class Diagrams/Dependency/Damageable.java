package lab4.uml;

public interface Damageable {
	public Double getDamage();
	public void damage(Double damage);
	public Boolean isDamaged();
}
