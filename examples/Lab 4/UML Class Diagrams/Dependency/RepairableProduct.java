package lab4.uml;

public class RepairableProduct extends DamageableProduct implements Repairable {
	
	public static final Double MAX_REPAIRABLE_DAMAGE = 0.7;

	@Override
	public void repair(Double repairRate) {
		if (canRepair()) {
			damage(-repairRate);
		}
	}

	@Override
	public Boolean canRepair() {
		return getDamage() < MAX_REPAIRABLE_DAMAGE;
	}

}
