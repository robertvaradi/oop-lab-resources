package lab4.uml;

public interface Repairable {
	public void repair(Double repairRate);
	public Boolean canRepair();
}
