package lab4.uml;

public record Address(String country, String region, String city, String street, String number) {
	
}
