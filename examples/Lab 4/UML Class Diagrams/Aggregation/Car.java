package lab4.uml;

public class Car {
	public static enum Color {
		RED, BLUE, BLACK, WHITE, YELLOW, GREEN, ORANGE
	}

	private String model;
	private Color color;
	private String make;
	private Wheel[] wheels;
	private Engine engine;
	private Door[] doors;
	private Light[] lights;

	public Car(String model, String make, Color color) {
		this.model = model;
		this.make = make;
		this.color = color;
		this.engine = new Engine(10000, 150, 2.0);
		this.wheels = new Wheel[] {
				new Wheel(new Tyre(model, make, 22, 50), make, model, 20, 50),
				new Wheel(new Tyre(model, make, 22, 50), make, model, 20, 50),
				new Wheel(new Tyre(model, make, 22, 50), make, model, 20, 50),
				new Wheel(new Tyre(model, make, 22, 50), make, model, 20, 50)
		};
		doors = new Door[] {
				new Door(Door.DoorPosition.FRONT_LEFT),
				new Door(Door.DoorPosition.FRONT_RIGHT),
				new Door(Door.DoorPosition.BACK_LEFT),
				new Door(Door.DoorPosition.BACK_RIGHT)
		};
		lights = new Light[] {
			new Light(Light.LightPosition.FRONT_LEFT),
			new Light(Light.LightPosition.FRONT_RIGHT),
			new Light(Light.LightPosition.BACK_LEFT),
			new Light(Light.LightPosition.BACK_RIGHT)
		};
	}

	public String getModel() {
		return model;
	}

	public Color getColor() {
		return color;
	}

	public String getMake() {
		return make;
	}

	public Wheel[] getWheels() {
		return wheels;
	}

	public Engine getEngine() {
		return engine;
	}

	public Door[] getDoors() {
		return doors;
	}

	public Light[] getLights() {
		return lights;
	}
}
