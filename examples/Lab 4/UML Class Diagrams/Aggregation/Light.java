package lab4.uml;

public class Light {
	public static enum LightPosition {
		FRONT_LEFT,
		FRONT_RIGHT,
		BACK_LEFT,
		BACK_RIGHT
	};
	
	public static enum Color {
		WHITE,
		RED
	};
	
	private LightPosition position;
	
	private Color color;
	
	public Light(LightPosition position) {
		this.position = position;
		if (LightPosition.FRONT_LEFT.equals(position) || LightPosition.FRONT_RIGHT.equals(position)) {
			this.color = Color.WHITE;
		} else {
			this.color = Color.RED;
		}
	}

	public LightPosition getPosition() {
		return position;
	}

	public Color getColor() {
		return color;
	}
}
