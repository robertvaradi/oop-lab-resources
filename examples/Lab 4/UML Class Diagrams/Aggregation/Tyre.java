package lab4.uml;

public class Tyre {
	private String model;

	private String make;

	private Integer size;
	
	private Integer width;
	
	public Tyre(String model, String make, Integer size, Integer width) {
		this.model = model;
		this.make = make;
		this.size = size;
		this.width = width;
	}

	public String getModel() {
		return model;
	}

	public String getMake() {
		return make;
	}

	public Integer getSize() {
		return size;
	}

	public Integer getWidth() {
		return width;
	}
}
