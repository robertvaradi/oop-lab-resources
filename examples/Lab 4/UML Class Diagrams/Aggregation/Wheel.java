package lab4.uml;

public class Wheel {

	private Tyre tyre;

	private String make;

	private String model;

	private Integer size;

	private Integer width;

	public Wheel(Tyre tyre, String make, String model, Integer size, Integer width) {
		this.tyre = tyre;
		this.make = make;
		this.model = model;
		this.size = size;
		this.width = width;
	}

	public Tyre getTyre() {
		return tyre;
	}

	public String getMake() {
		return make;
	}

	public String getModel() {
		return model;
	}

	public Integer getSize() {
		return size;
	}

	public Integer getWidth() {
		return width;
	}
}
