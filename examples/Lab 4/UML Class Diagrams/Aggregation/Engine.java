package lab4.uml;

public class Engine {

	private Integer maxRpm;

	private Integer maxPower;

	private Double capacity;

	public Engine(Integer maxRpm, Integer maxPower, Double capacity) {
		this.maxPower = maxPower;
		this.capacity = capacity;
		this.maxRpm = maxRpm;
	}

	public Integer getMaxRpm() {
		return maxRpm;
	}

	public Integer getMaxPower() {
		return maxPower;
	}

	public Double getCapacity() {
		return capacity;
	}

}
