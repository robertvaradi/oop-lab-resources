package realization;

public class Wheel extends RepairableProduct {

	private Tyre tyre;
	
	public Wheel(Tyre tyre) {
		this.tyre = tyre;
	}
	
	public Tyre getTyre() {
		return tyre;
	}
	
}
