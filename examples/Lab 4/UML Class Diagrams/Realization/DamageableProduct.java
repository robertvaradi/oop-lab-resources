package realization;

public class DamageableProduct extends Product implements Damageable {

	public static final Double MIN_DAMAGE = 0.3;
	
	public static final Double MAX_DAMAGE = 1.0;
	
	private Double damage;
	
	public DamageableProduct() {
		this.damage = 0.0;
	}

	@Override
	public Double getDamage() {
		return damage;
	}

	@Override
	public void damage(Double damage) {
		this.damage = Math.min(this.damage + damage, MAX_DAMAGE);
	}

	@Override
	public Boolean isDamaged() {
		return damage > MIN_DAMAGE;
	}

}
