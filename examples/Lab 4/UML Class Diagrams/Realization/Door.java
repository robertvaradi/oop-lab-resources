package realization;

public class Door {
	public static enum DoorPosition {
		FRONT_LEFT,
		FRONT_RIGHT,
		BACK_LEFT,
		BACK_RIGHT
	};
	
	private DoorPosition position;
	
	public Door(DoorPosition position) {
		this.position = position;
	}

	public DoorPosition getPosition() {
		return position;
	}
}
