package realization;

public interface Repairable {
	public void repair(Double repairRate);
	public Boolean canRepair();
}
