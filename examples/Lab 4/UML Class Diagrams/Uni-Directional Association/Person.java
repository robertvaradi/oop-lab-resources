package lab4.uml;

public class Person {
	static Integer count;

	protected String name;

	private Integer age;

	public String gender;
	
	private Address address;

	public Person(String name, Integer age, String gender, Address address) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.address = address;
		count++;
	}

	public Integer getAge() {
		return age;
	}

	public static Integer getCount() {
		return count;
	}
	
	public Address getAddress() {
		return address;
	}
}
