package lab4.uml;

public class Person {
	static Integer count;

	protected String name;

	private Integer age;

	public String gender;

	public Person(String name, Integer age, String gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		count++;
	}

	public Integer getAge() {
		return age;
	}

	public static Integer getCount() {
		return count;
	}
}
