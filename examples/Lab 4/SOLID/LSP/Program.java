package lab4.solid.lsp;

public class Program {
	
	private static boolean equals(Double d1, Double d2) {
		Double epsilon = 0.00001;
		return Math.abs(d1 - d2) < epsilon;
	}
	
	
	public static void main(String[] args) {
		
		Rectangle rectangle = new Rectangle();
		rectangle.setWidth(4.0);
		rectangle.setHeight(5.0);
		if (equals(rectangle.getWidth(), 4.0) && equals(rectangle.getHeight(), 5.0)) {
			System.out.println("Rectangle - OK");
		} else {
			System.out.println("Rectangle - Not OK");
		}
		
		Rectangle square = new Square();
		square.setWidth(4.0);
		square.setHeight(5.0);
		if (equals(square.getWidth(), 4.0) && equals(square.getHeight(), 5.0)) {
			System.out.println("Square - OK");
		} else {
			System.out.println("Square - Not OK");
		}
	}
}
