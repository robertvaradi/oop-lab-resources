package lab4.solid.lsp;

public class Square extends Rectangle {

	@Override
	public void setWidth(Double width) {
		super.setWidth(width);
		super.setHeight(width);
	}

	@Override
	public void setHeight(Double height) {
		super.setHeight(height);
		super.setWidth(height);
	}
	
}
