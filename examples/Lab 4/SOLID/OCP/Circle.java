package lab4.solid.ocp;

public class Circle implements Shape {
	private Double radius;
	
	public Circle(Double radius) {
		this.radius = radius;
	}
	
	public Double getRadius() {
		return radius;
	}
}
