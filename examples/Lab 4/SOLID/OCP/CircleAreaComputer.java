package lab4.solid.ocp;

public class CircleAreaComputer implements AreaComputer {

	@Override
	public Double compute(Shape shape) {
		Circle circle = (Circle) shape;
		return Math.PI * Math.pow(circle.getRadius(), 2);
	}

}
