package lab4.solid.ocp;

public class Triangle implements Shape {
	private Double a;
	private Double b;
	private Double c;
	
	public Triangle(Double a, Double b, Double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public Double getA() {
		return a;
	}
	
	public Double getB() {
		return b;
	}
	
	public Double getC() {
		return c;
	}
}
