package lab4.solid.ocp;

public class ShapeAreaComputer {
	private AreaComputer getComputer(Shape shape) {
		if (shape instanceof Rectangle) {
			return new RectangleAreaComputer();
		} else if (shape instanceof Square) {
			return new SquareAreaComputer();
		} else if (shape instanceof Circle) {
			return new CircleAreaComputer();
		} else if (shape instanceof Triangle) {
			return new TriangleAreaComputer();
		}
		return null;
	}
	
	public Double[] compute(Shape[] shapes) {
		Double[] areas = new Double[shapes.length];
		for (int i = 0; i < shapes.length; i++) {
			areas[i] = getComputer(shapes[i]).compute(shapes[i]);
		}
		return areas;
	}
}
