package lab4.solid.ocp;

public class Square implements Shape {
	private Double length;
	
	public Square(Double length) {
		this.length = length;
	}
	
	public Double getLength() {
		return length;
	}
}
