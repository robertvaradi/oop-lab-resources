package lab4.solid.ocp;

public class SquareAreaComputer implements AreaComputer {

	@Override
	public Double compute(Shape shape) {
		Square square = (Square) shape;
		return Math.pow(square.getLength(), 2);
	}

}
