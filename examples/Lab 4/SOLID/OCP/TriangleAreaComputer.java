package lab4.solid.ocp;

public class TriangleAreaComputer implements AreaComputer {

	@Override
	public Double compute(Shape shape) {
		Triangle triangle = (Triangle) shape;
		Double p = (triangle.getA() + triangle.getB() + triangle.getC()) / 2;
		return Math.sqrt(p * (p - triangle.getA()) * (p - triangle.getB()) * (p - triangle.getC()));
	}

}
