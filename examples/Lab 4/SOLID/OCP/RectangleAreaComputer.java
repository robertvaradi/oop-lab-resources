package lab4.solid.ocp;

public class RectangleAreaComputer implements AreaComputer {

	@Override
	public Double compute(Shape shape) {
		Rectangle rectangle = (Rectangle) shape;
		return rectangle.getHeight() * rectangle.getWidth();
	}

}
