package lab4.solid.ocp;

public interface AreaComputer {
	public Double compute(Shape shape);
}
