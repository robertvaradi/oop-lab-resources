package lab4.solid.isp;

public class Square implements Shape2D {
	private Double length;
	
	public Square(Double length) {
		this.length = length;
	}
	
	@Override
	public Double area() {
		return Math.pow(length, 2);
	}
}
