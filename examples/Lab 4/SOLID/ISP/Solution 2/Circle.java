package lab4.solid.isp;

public class Circle implements Shape2D {
	private Double radius;
	
	public Circle(Double radius) {
		this.radius = radius;
	}
	
	@Override
	public Double area() {
		return Math.PI * Math.pow(radius, 2);
	}
}
