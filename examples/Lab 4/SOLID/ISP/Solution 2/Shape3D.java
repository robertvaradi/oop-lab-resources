package lab4.solid.isp;

public interface Shape3D extends Shape {
	public Double volume();
}
