package lab4.solid.isp;

public class Cube implements Shape3D {
	private Double length;
	
	public Cube(Double length) {
		this.length = length;
	}

	@Override
	public Double volume() {
		return Math.pow(length, 3);
	}
	
}
