package lab4.solid.isp;

public class Square implements Shape {
	private Double length;
	
	public Square(Double length) {
		this.length = length;
	}
	
	@Override
	public Double area() {
		return Math.pow(length, 2);
	}

	@Override
	public Double volume() {
		return null;
	}

}
