package lab4.solid.isp;

public class Circle implements Shape {
	private Double radius;
	
	public Circle(Double radius) {
		this.radius = radius;
	}
	
	@Override
	public Double area() {
		return Math.PI * Math.pow(radius, 2);
	}

	@Override
	public Double volume() {
		return null;
	}

}
