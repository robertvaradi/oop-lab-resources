package lab4.solid.isp;

public class Cube implements Shape {
	private Double length;
	
	public Cube(Double length) {
		this.length = length;
	}
	
	@Override
	public Double area() {
		return null;
	}

	@Override
	public Double volume() {
		return Math.pow(length, 3);
	}
	
}
