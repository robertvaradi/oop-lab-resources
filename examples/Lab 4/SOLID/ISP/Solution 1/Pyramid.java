package lab4.solid.isp;

public class Pyramid implements Shape {
	private Double baseHeight;
	
	private Double baseWidth;
	
	private Double height;
	
	public Pyramid(Double baseWidth, Double baseHeight, Double height) {
		this.baseWidth = baseWidth;
		this.baseHeight = baseHeight;
		this.height = height;
	}
	
	@Override
	public Double area() {
		return null;
	}

	@Override
	public Double volume() {
		return (baseWidth * baseHeight * height) / 3.0;
	}
	
}
