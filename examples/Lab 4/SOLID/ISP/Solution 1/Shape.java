package lab4.solid.isp;

public interface Shape {
	public Double area();
	public Double volume();
}
