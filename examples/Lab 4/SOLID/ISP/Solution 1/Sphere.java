package lab4.solid.isp;

public class Sphere implements Shape {
	private Double radius;
	
	public Sphere(Double radius) {
		this.radius = radius;
	}
	
	@Override
	public Double area() {
		return null;
	}

	@Override
	public Double volume() {
		return 4.0 * Math.PI * Math.pow(radius, 3) / 3.0;
	}
	
}
