package lab4.solid.isp;

public class Program {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[] { new Rectangle(4.0, 6.0), new Square(5.0), new Circle(5.0),
				new Triangle(3.0, 4.0, 5.0), new Pyramid(4.0, 6.0, 3.0), new Sphere(4.0), new Cube(4.0) };
		for (Shape shape : shapes) {
			boolean is3D = false;
			Double value = shape.area();
			if (value == null) {
				is3D = true;
				value = shape.volume();
			}
			if (is3D) {
				System.out.println("The volume of the " + shape.getClass().getSimpleName() + " is " + value);
			} else {
				System.out.println("The area of the " + shape.getClass().getSimpleName() + " is " + value);
			}
		}
	}
}
