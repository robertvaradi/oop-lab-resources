package lab4.solid.dip;

public abstract class RepositoryImpl implements Repository {
	private MySqlConnection connection;
	
	private Array entityArray;
	
	private Long nextId;
	
	private String tableName;

	public RepositoryImpl(String tableName) {
		connection = new MySqlConnection();
		nextId = 0L;
		entityArray = new Array(20, 1.5);
		this.tableName = tableName;
	}

	protected abstract String makeInsertStatement(Entity object);

	protected abstract String makeUpdateStatement(Entity object);

	@Override
	public void save(Entity object) {
		connection.connect();
		object.setId(++nextId);
		connection.execute(makeInsertStatement(object));
		entityArray.insert(object);
		connection.disconnect();
	}

	@Override
	public Entity findById(Long id) {
		connection.connect();
		connection.execute("select * from " + tableName + " where id = " + id);
		Entity found = entityArray.findById(id);
		connection.disconnect();
		return found;
	}

	@Override
	public void update(Entity toUpdate) {
		connection.connect();
		connection.execute(makeUpdateStatement(toUpdate));
		entityArray.update(toUpdate);
		connection.disconnect();
	}

	@Override
	public void delete(Entity toRemove) {
		connection.connect();
		connection.execute("delete from " + tableName + " where id = " + toRemove.getId());
		entityArray.remove(toRemove);
		connection.disconnect();
	}

	@Override
	public Entity[] findAll() {
		connection.connect();
		connection.execute("select * from " + tableName);
		Entity[] entities = entityArray.entities();
		connection.disconnect();
		return entities;
	}
	
	public String getTableName() {
		return tableName;
	}

}
