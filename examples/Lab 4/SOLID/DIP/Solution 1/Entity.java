package lab4.solid.dip;

public interface Entity {
	public Long getId();
	public void setId(Long id);
}
