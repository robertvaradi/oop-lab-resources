package lab4.solid.dip;

public class MySqlConnection {
	private void prompt(String message) {
		System.out.println(message);
	}
	
	public void connect() {
		prompt("Connecting to the MySQL Server Database ...");
	}
	
	public void disconnect() {
		prompt("Diconnecting from teh MySQL Server Database ...");
	}
	
	public void execute(String queryString) {
		prompt("Executing query \"" + queryString + "\"...");
	}
}
