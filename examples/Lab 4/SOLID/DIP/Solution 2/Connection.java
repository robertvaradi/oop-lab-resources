package lab4.solid.dip;

public interface Connection {
	public void connect();
	public void disconnect();
	public void execute(String queryString);
}
