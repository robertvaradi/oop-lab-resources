package lab4.solid.dip;

public class Program {
	private static void insert(Repository repository, String username, String password, String fullName, String phoneNumber, String email) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setFullName(fullName);
		user.setPhoneNumber(phoneNumber);
		user.setEmail(email);
		repository.save(user);
	}
	
	public static void main(String[] args) {
		Repository repo = new UserRepository(new MssqlConnection());
		insert(repo, "jdoe", "1234", "John Doe", "097574536", "jdoe@gmail.com");
		insert(repo, "jsmith", "abcd", "Jane Smith", "0986755654", "jsmith@yahoo.com");
		for (Entity entity : repo.findAll()) {
			User user = (User) entity;
			System.out.format("User[%d, %s, %s, %s, %s, %s]\n", user.getId(), user.getUsername(), user.getPassword(), user.getFullName(), user.getPhoneNumber(), user.getEmail());
		}
	}
}
