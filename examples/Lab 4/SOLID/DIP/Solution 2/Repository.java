package lab4.solid.dip;

public interface Repository {
	public void save(Entity object);
	public Entity findById(Long id);
	public void update(Entity toUpdate);
	public void delete(Entity toRemove);
	public Entity[] findAll();
}
