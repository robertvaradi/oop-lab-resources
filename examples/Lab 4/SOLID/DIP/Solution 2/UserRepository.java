package lab4.solid.dip;

public class UserRepository extends RepositoryImpl {

	public UserRepository(Connection connection) {
		super(connection, "ApplicationUser");
	}

	@Override
	protected String makeInsertStatement(Entity object) {
		User user = (User) object;
		String formatString = "insert into %s (id, username, password, fullName, phoneNumber, email) values (%d, '%s', '%s', '%s', '%s', '%s')";
		return String.format(formatString, getTableName(), user.getId(), user.getUsername(), user.getPassword(), user.getFullName(), user.getPhoneNumber(), user.getEmail());
	}

	@Override
	protected String makeUpdateStatement(Entity object) {
		User user = (User) object;
		String formatString = "update %s set username = '%s', password = '%s', fullName = '%s', phoneNumber = '%s', email = '%s' where id = %d";
		return String.format(formatString, getTableName(), user.getUsername(), user.getPassword(), user.getFullName(), user.getPhoneNumber(), user.getEmail(), user.getId());
	}
}
