package lab4.solid.srp;

public class AreaDisplayer {
	private ShapeAreaComputer computer;
	
	public AreaDisplayer(ShapeAreaComputer computer) {
		this.computer = computer;
	}
	
	public void displayAreas(Shape[] shapes) {
		Double[] areas = computer.compute(shapes);
		for (Double area : areas) {
			System.out.println(area);
		}
	}
}
