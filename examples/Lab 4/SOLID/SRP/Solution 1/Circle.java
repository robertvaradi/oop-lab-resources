package lab4.solid.srp;

public class Circle implements Shape {
	private Double radius;
	
	public Circle(Double radius) {
		this.radius = radius;
	}
	
	public Double getRadius() {
		return radius;
	}
}
