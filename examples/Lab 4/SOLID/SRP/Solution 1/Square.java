package lab4.solid.srp;

public class Square implements Shape {
	private Double length;
	
	public Square(Double length) {
		this.length = length;
	}
	
	public Double getLength() {
		return length;
	}
}
