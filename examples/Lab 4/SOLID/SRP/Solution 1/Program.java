package lab4.solid.srp;

public class Program {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[] {new Rectangle(2.0, 3.0), new Square(4.0), new Circle(5.0), new Triangle(3.0, 4.0, 5.0)};
		ShapeAreaComputer sac = new ShapeAreaComputer();
		sac.computeAndDisplay(shapes);
	}
}
