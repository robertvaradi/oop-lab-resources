package lab4.solid.srp;

public class ShapeAreaComputer {
	public void computeAndDisplay(Shape[] shapes) {
		Double[] areas = new Double[shapes.length];
		for (int i = 0; i < shapes.length; i++) {
			Double area = null;
			if (shapes[i] instanceof Rectangle) {
				Rectangle rectangle = (Rectangle) shapes[i];
				area = rectangle.getHeight() * rectangle.getWidth();
			} else if (shapes[i] instanceof Square) {
				Square square = (Square) shapes[i];
				area = Math.pow(square.getLength(), 2);
			} else if (shapes[i] instanceof Circle) {
				Circle circle = (Circle) shapes[i];
				area = Math.PI * Math.pow(circle.getRadius(), 2);
			} else if (shapes[i] instanceof Triangle) {
				Triangle triangle = (Triangle) shapes[i];
				Double p = (triangle.getA() + triangle.getB() + triangle.getC()) / 2.0;
				area = Math.sqrt(p * (p - triangle.getA()) * (p - triangle.getB()) * (p - triangle.getC()));
			}
			areas[i] = area;
		}
		for (Double area : areas) {
			System.out.println(area);
		}
	}
}
