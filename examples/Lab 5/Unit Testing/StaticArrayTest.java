package lab5.unitTests;

import org.junit.jupiter.api.Test;

class StaticArrayTest {

	@Test
	public void testGetCapacity() {
		StaticArray array = new StaticArray(20);
		assert array.getCapacity() == 20;
	}
	
	@Test
	public void tesGetSize() {
		StaticArray array = new StaticArray(20);
		assert array.getSize() == 0;
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		assert array.getSize() == 10;
	}
	
	@Test
	public void testGetElement() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		for (int i = 0; i < 10; i++) {
			assert array.getElement(i) == i;
		}
	}

	@Test
	public void testSetElement() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		array.setElement(5, 100);
		for (int i = 0; i < 10; i++) {
			if (i != 5) {
				assert array.getElement(i) == i;
			} else {
				assert array.getElement(i) == 100;
			}
		}
	}

	@Test
	public void testIsEmpty() {
		StaticArray array = new StaticArray(10);
		assert array.isEmpty();
		array.insertLast(0);
		assert !array.isEmpty();
		array.removeFirst();
		assert array.isEmpty();
	}
	
	@Test
	public void tesIsFull() {
		StaticArray array = new StaticArray(10);
		for (int i = 0; i < 10; i++) {
			assert !array.isFull();
			array.insertLast(i);
		}
		assert array.isFull();
	}
	
	@Test
	public void testInsert() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		array.insert(5, 100);
		assert array.getSize() == 11;
		assert array.getElement(5) == 100;
	}

	@Test
	public void testInsertFirst() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		array.insertFirst(100);
		assert array.getSize() == 11;
		assert array.getElement(0) == 100;
	}

	@Test
	public void testInsertLast() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		array.insertLast(100);
		assert array.getSize() == 11;
		assert array.getElement(array.getSize() - 1) == 100;
	}

	@Test
	public void testRemove() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		int value = array.remove(5);
		assert array.getSize() == 9;
		for (int i = 0; i < array.getSize(); i++) {
			assert array.getElement(i) != value;
		}
	}

	@Test
	public void testRemoveFirst() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		int value = array.removeFirst();
		assert array.getSize() == 9;
		for (int i = 0; i < array.getSize(); i++) {
			assert array.getElement(i) != value;
		}
	}

	@Test
	public void testRemoveLast() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		int value = array.removeLast();
		assert array.getSize() == 9;
		for (int i = 0; i < array.getSize(); i++) {
			assert array.getElement(i) != value;
		}
	}
	
	@Test
	public void testSearch() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		for (int i = 0; i < 10; i++) {
			assert array.search(i) == i;
		}
		int value = array.remove(5);
		assert array.search(value) == -1;
		assert array.search(100) == -1;
	}

	@Test
	public void testExists() {
		StaticArray array = new StaticArray(20);
		for (int i = 0; i < 10; i++) {
			array.insertLast(i);
		}
		for (int i = 0; i < 10; i++) {
			assert array.exists(i);
		}
		int value = array.remove(5);
		assert !array.exists(value);
		assert !array.exists(100);
	}
}
