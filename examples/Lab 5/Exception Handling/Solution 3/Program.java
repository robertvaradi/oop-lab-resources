package lab5.exceptions;

import java.util.Scanner;

public class Program {
	private static Integer toInteger(String token) {
		try {
			return Integer.parseInt(token.trim());
		} catch (NumberFormatException nfe) {
			return null;
		}
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		try {
			String commandString = null;
			do {
				System.out.print(">> ");
				commandString = scanner.nextLine();
				if (!"exit".equals(commandString)) {
					String[] tokens = null;
					Character operator = null;
					if (commandString.indexOf('+') != -1) {
						tokens = commandString.split("\\+");
						operator = '+';
					} else if (commandString.indexOf('-') != -1) {
						tokens = commandString.split("-");
						operator = '-';
					} else if (commandString.indexOf('*') != -1) {
						tokens = commandString.split("\\*");
						operator = '*';
					} else if (commandString.indexOf('/') != -1) {
						tokens = commandString.split("/");
						operator = '/';
					} else if (commandString.indexOf('%') != -1) {
						tokens = commandString.split("%");
						operator = '%';
					}
					if (tokens == null) {
						System.err.println("No tokenizable input!");
					} else if (tokens.length != 2) {
						System.err.println("There should be exactly two operators!");
					} else {
						Integer operand1 = toInteger(tokens[0]);
						Integer operand2 = toInteger(tokens[1]);
						if (operand1 != null && operand2 != null) {
							Integer result = switch (operator) {
								case '+' -> operand1 + operand2;
								case '-' -> operand1 - operand2;
								case '*' -> operand1 * operand2;
								case '/' -> operand2 != 0 ? operand1 / operand2 : null;
								case '%' -> operand2 != 0 ? operand1 % operand2 : null;
								default -> null;
							};
							if (result == null) {
								System.err.println("Division by zero detected");
							} else {
								System.out.println(result);
							}
						} else {
							if (operand1 == null) {
								System.err.println("Invalid number " + tokens[0]);
							} else {
								System.err.println("Invalid number " + tokens[1]);
							}
						}
					}
				}
			} while (!"exit".equals(commandString));
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid number format: \'" + nfe.getMessage() + "\'");
		} catch (Exception exc) {
			exc.printStackTrace();
		} finally {
			scanner.close();
		}
		System.out.println("The program finished execution!");
	}
}
