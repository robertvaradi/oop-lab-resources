package lab5.exceptions;

import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String commandString = null;
		do {
			System.out.print(">> ");
			commandString = scanner.nextLine();
			if (!"exit".equals(commandString)) {
				String[] tokens = null;
				Character operator = null;
				if (commandString.indexOf('+') != -1) {
					tokens = commandString.split("\\+");
					operator = '+';
				} else if (commandString.indexOf('-') != -1) {
					tokens = commandString.split("-");
					operator = '-';
				} else if (commandString.indexOf('*') != -1) {
					tokens = commandString.split("\\*");
					operator = '*';
				} else if (commandString.indexOf('/') != -1) {
					tokens = commandString.split("/");
					operator = '/';
				} else if (commandString.indexOf('%') != -1) {
					tokens = commandString.split("%");
					operator = '%';
				}
				Integer result = switch (operator) {
					case '+' -> Integer.parseInt(tokens[0]) + Integer.parseInt(tokens[1]);
					case '-' -> Integer.parseInt(tokens[0]) - Integer.parseInt(tokens[1]);
					case '*' -> Integer.parseInt(tokens[0]) * Integer.parseInt(tokens[1]);
					case '/' -> Integer.parseInt(tokens[0]) / Integer.parseInt(tokens[1]);
					case '%' -> Integer.parseInt(tokens[0]) % Integer.parseInt(tokens[1]);
					default -> null;
				};
				System.out.println(result);
			}
		} while (!"exit".equals(commandString));
		scanner.close();
	}
}
