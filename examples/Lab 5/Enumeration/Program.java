package lab5.enumeration;

public class Program {
	private static void newLine() {
		System.out.println("-".repeat(64));
	}

	private static void display(PersonArrayEnum enumeration) {
		newLine();
		System.out.format("|%20s|%20s|%20s|\n", "name", "gender", "age");
		while (enumeration.hasMoreElements()) {
			Person person = enumeration.nextElement();
			newLine();
			System.out.format("|%20s|%20s|%20d|\n", person.getName(), person.getGender(), person.getAge());
		}
		newLine();
		System.out.println();
	}

	public static void main(String[] args) {
		Person[] persons = new Person[] {
				new Person("Kassey Hassall", "M", 33),
				new Person("Becky Fealty", "F", 21),
				new Person("Sharla Luxon", "F", 25),
				new Person("Elka Critchell", "F", 58),
				new Person("Beck Haines", "M", 42),
				new Person("Mavra Inde", "F", 16),
				new Person("Alisun Menichino", "F", 55),
				new Person("Kizzee Straker", "F", 25),
				new Person("Giacobo Mansion", "M", 21),
				new Person("Derward Harold", "M", 31)
		};
		PersonArrayEnum personEnum = new PersonArrayEnum(persons);
		display(personEnum);
		personEnum.sort();
		personEnum.reset();
		display(personEnum);
	}
}
