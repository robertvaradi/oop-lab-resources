package lab5.enumeration;

import java.util.Arrays;
import java.util.Enumeration;

public class PersonArrayEnum implements Enumeration<Person> {

	private Person[] persons;
	
	private Integer currentIndex;
	
	private Integer size;
	
	public PersonArrayEnum(Person[] persons) {
		this.persons = persons;
		this.size = persons.length;
		this.currentIndex = 0;
	}
	
	@Override
	public boolean hasMoreElements() {
		return currentIndex < size;
	}

	@Override
	public Person nextElement() {
		Person elem = persons[currentIndex];
		currentIndex++;
		return elem;
	}
	
	public void sort() {
		Arrays.sort(persons);
	}
	
	public void reset() {
		currentIndex = 0;
	}

}
