package lab5.exceptions;

public class Program {
	public static void main(String[] args) {
		StaticArray array = null;
		try {
			array = new StaticArray(-20);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		array = new StaticArray(10);
		try {
			array.insert(-1, 10);
		} catch (ArrayIndexOutOfBoundsException | StaticArrayIsFullException e) {
			e.printStackTrace();
		}
		try {
			array.insert(5, 10);
		} catch (ArrayIndexOutOfBoundsException | StaticArrayIsFullException e) {
			e.printStackTrace();
		}
		try {
			for (int i = 0; i < 11; i++) {
				array.insert(array.getSize(), i);
			}
		} catch (ArrayIndexOutOfBoundsException | StaticArrayIsFullException e) {
			e.printStackTrace();
		}
		array.display();
	}
}
