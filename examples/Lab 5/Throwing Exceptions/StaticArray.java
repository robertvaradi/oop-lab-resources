package lab5.exceptions;

public class StaticArray {
	private Integer[] elements;
	
	private Integer size;
	
	public StaticArray(Integer capacity) {
		if (capacity <= 0) {
			throw new IllegalArgumentException("The capacity of the array must be a strictly positive integer number!");
		}
		this.elements = new Integer[capacity];
		this.size = 0;
	}
	
	public Integer getCapacity() {
		return elements.length;
	}
	
	public Integer getSize() {
		return size;
	}
	
	public void insert(Integer value, Integer position) throws StaticArrayIsFullException {
		if (position < 0) {
			throw new ArrayIndexOutOfBoundsException("The insertion position must be a positive number!");
		} else if (position > size) {
			throw new ArrayIndexOutOfBoundsException(String.format("The insertion position (%d) should not be greater than %d", position, size));
		} else if (size == getCapacity()) {
			throw new StaticArrayIsFullException(getCapacity());
		}
		for (int i = size - 1; i >= position; i--) {
			elements[i + 1] = elements[i];
		}
		elements[position] = value;
		size++;
	}
	
	public void display() {
		for (int i = 0; i < size; i++) {
			System.out.format("%d%s", elements[i], (i == size - 1) ? "\n" : ", ");
		}
	}
	
}
