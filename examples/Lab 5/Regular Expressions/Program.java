package lab5.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String arrayString = scanner.nextLine();
		Pattern pattern = Pattern.compile("\\[\\]|\\[[-?\\d+,[ ]*]*-?\\d+\\]");
		Matcher matcher = pattern.matcher(arrayString);
		if (matcher.matches()) {
			Pattern numberPattern = Pattern.compile("-?\\d+");
			Matcher numberMatcher = numberPattern.matcher(arrayString);
			int capacity = 0;
			while (numberMatcher.find()) {
				capacity++;
			}
			numberMatcher = numberPattern.matcher(arrayString);
			StaticArray array = new StaticArray(capacity);
			while (numberMatcher.find()) {
				try {
					array.insert(Integer.parseInt(numberMatcher.group()), array.getSize());
				} catch (NumberFormatException | StaticArrayIsFullException e) {
					e.printStackTrace();
				}
			}
			array.display();
		} else {
			System.err.format("%s is not a valid array representation", arrayString);
		}
		scanner.close();
	}
}
