package lab5.regex;

public class StaticArrayIsFullException extends Exception {

	private static final long serialVersionUID = 4082341982910551183L;

	public StaticArrayIsFullException(Integer capacity) {
		super(String.format("Static array already reached its maximum capacity %d which cannot be exceeded!", capacity));
	}
	
}
