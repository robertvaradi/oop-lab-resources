package lab5.comparable;

import java.util.Arrays;

public class Program {
	private static void display(Person[] persons) {
		System.out.println("-".repeat(64));
		System.out.format("|%20s|%20s|%20s|\n", "name", "gender", "age");
		for (Person person : persons) {
			System.out.println("-".repeat(64));
			System.out.format("|%20s|%20s|%20d|\n", person.getName(), person.getGender(), person.getAge());
		}
		System.out.println("-".repeat(64));
		System.out.println();
	}
	
	public static void main(String[] args) {
		Person[] persons = new Person[] {
				new Person("Kassey Hassall","M",33),
				new Person("Becky Fealty","F",21),
				new Person("Sharla Luxon","F",25),
				new Person("Elka Critchell","F",58),
				new Person("Beck Haines","M",42),
				new Person("Mavra Inde","F",16),
				new Person("Alisun Menichino","F",55),
				new Person("Kizzee Straker","F",25),
				new Person("Giacobo Mansion","M",21),
				new Person("Derward Harold","M",31)
		};
		display(persons);
		Arrays.sort(persons);
		display(persons);
	}
}
