package lab5.object;

public class Person {
	private String name;
	private String gender;
	private Integer age;
	
	public Person(String name, String gender, Integer age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}

	@Override
	public int hashCode() {
		return String.join("_", name, gender, age.toString()).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (obj == this) {
			return true;
		} else if (!getClass().equals(obj.getClass())) {
			return false;
		} else {
			Person other = (Person) obj;
			return name.equals(other.name) && gender.equals(other.gender) && age.equals(other.age);
		}
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new Person(name, gender, age);
	}

	@Override
	public String toString() {
		
		return String.format("Person-%s[name = %s, gender = %s, age = %d]", super.toString(), name, gender, age);
	}

	@Override
	protected void finalize() throws Throwable {
		System.out.format("Object %s is about to be destroyed by the garbage collector", toString());
	}
}
