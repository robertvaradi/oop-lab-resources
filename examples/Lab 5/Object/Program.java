package lab5.object;

public class Program {
	private static void checkEqualsOperator(Person person1, Person person2) {
		if (person1 == person2) {
			System.out.format("%s == %s\n", person1, person2);
		} else {
			System.out.format("%s != %s\n", person1, person2);
		}
	}
	
	private static void checkEqualsMethod(Person person1, Person person2) {
		if (person1 == null && person2 == null || (person1 != null && person1.equals(person2))) {
			System.out.format("%s == %s\n", person1, person2);
		} else {
			System.out.format("%s != %s\n", person1, person2);
		}
	}
	
	public static void main(String[] args) throws CloneNotSupportedException {
		Person john = new Person("John Doe", "M", 25);
		Person jane = new Person("Jane Doe", "F", 21);
		checkEqualsOperator(john, john);
		checkEqualsMethod(john, john);
		System.out.println(john);
		System.out.println(jane);
		checkEqualsOperator(john, jane);
		checkEqualsMethod(john, jane);
		Person johnsClone = (Person) john.clone();
		checkEqualsOperator(john, johnsClone);
		checkEqualsMethod(john, johnsClone);
		System.out.println("hashCode(john) = " + john.hashCode());
		System.out.println("hashCode(jane) = " + jane.hashCode());
		System.out.println("hashCode(johnsClone) = " + johnsClone.hashCode());
	}
}
